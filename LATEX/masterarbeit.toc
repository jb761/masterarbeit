\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Motivation}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Gliederung der Arbeit}{1}{subsection.1.1}%
\contentsline {section}{\numberline {2}Bisherige Ansätze zur Bestimmung von Pflanzenarten}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Machine Learning}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Deep Learning}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Allgemeine hierarchische Ansätze}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}CNN - Convolutional Neural Network}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Layertypen}{5}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Convolutional Layer}{5}{subsubsection.3.1.1}%
\contentsline {paragraph}{Aktivierungsfunktionen}{6}{section*.3}%
\contentsline {subsubsection}{\numberline {3.1.2}Pooling Layer}{6}{subsubsection.3.1.2}%
\contentsline {paragraph}{MAX-Pooling}{6}{section*.5}%
\contentsline {paragraph}{AVG-Pooling}{6}{section*.6}%
\contentsline {subsubsection}{\numberline {3.1.3}Flatten-Layer}{8}{subsubsection.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.4}Fully Connected Layer (FC-Layer / Dense-Layer / Output-Layer)}{8}{subsubsection.3.1.4}%
\contentsline {subsection}{\numberline {3.2}Backpropagation}{8}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Fully Connected Layer (FC-Layer)}{9}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Pooling Layer}{9}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Convolutional Layer}{9}{subsubsection.3.2.3}%
\contentsline {paragraph}{Ausgehender Fehler}{9}{section*.8}%
\contentsline {paragraph}{Filter-Gewichte}{10}{section*.9}%
\contentsline {subsubsection}{\numberline {3.2.4}Lernstrategien}{10}{subsubsection.3.2.4}%
\contentsline {paragraph}{SGD - Stochastic Gradient Descent}{10}{section*.11}%
\contentsline {paragraph}{Batch/Mini-Batch Gradient Descent}{10}{section*.12}%
\contentsline {paragraph}{Momentum}{11}{section*.13}%
\contentsline {subsection}{\numberline {3.3}Bekannte Architekturen}{13}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}AlexNet}{13}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}VGG-Net}{14}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}GoogLeNet}{15}{subsubsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.4}ResNet}{16}{subsubsection.3.3.4}%
\contentsline {paragraph}{ResNet-152}{16}{section*.19}%
\contentsline {section}{\numberline {4}Pl@ntNet-300K Datensatz}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}Versuch I - Referenz Versuch}{19}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Framework deeplearning4j}{19}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Vorbereitung und Datensätze Gattung, Art}{19}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Erhobene Metriken}{21}{subsubsection.4.1.3}%
\contentsline {paragraph}{Top-K-Accuracy}{21}{section*.23}%
\contentsline {paragraph}{Makro-AVG-Top-1-Accuracy}{21}{section*.24}%
\contentsline {subsubsection}{\numberline {4.1.4}Auswertung D11}{21}{subsubsection.4.1.4}%
\contentsline {subsubsection}{\numberline {4.1.5}Auswertung D12}{23}{subsubsection.4.1.5}%
\contentsline {section}{\numberline {5}Verwendung gemeinsamer Klassen}{25}{section.5}%
\contentsline {subsection}{\numberline {5.1}Klassen-Hierarchie}{26}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Versuch II - Gemeinsame Klassen}{26}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Verwendete Metriken}{27}{subsubsection.5.2.1}%
\contentsline {paragraph}{Ebenen-Top-1-Accuracy}{30}{section*.35}%
\contentsline {paragraph}{Ebenen-Macro-AVG-Top-1-Accuracy}{30}{section*.36}%
\contentsline {paragraph}{Ebenen-Aktivierung}{30}{section*.37}%
\contentsline {subsubsection}{\numberline {5.2.2}Auswertung gemeinsamer Klassen}{30}{subsubsection.5.2.2}%
\contentsline {subsection}{\numberline {5.3}Conditional Feedforeward}{34}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Distance Feedforward}{36}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Selective Feedforward}{37}{subsubsection.5.3.2}%
\contentsline {section}{\numberline {6}Branch-CNN}{38}{section.6}%
\contentsline {subsection}{\numberline {6.1}Versuch III - Branched-AlexNet}{39}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}Auswertung}{40}{subsubsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.2}Selective-Conditional Feedforward}{42}{subsubsection.6.1.2}%
\contentsline {subsubsection}{\numberline {6.1.3}Getauschte Ausgänge}{43}{subsubsection.6.1.3}%
\contentsline {subsection}{\numberline {6.2}Versuch IV - B-CNN und Branched-VGG-16}{44}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}Auswertung}{45}{subsubsection.6.2.1}%
\contentsline {paragraph}{Selective-Conditional Feedforward 224$\times $224}{46}{section*.60}%
\contentsline {subsection}{\numberline {6.3}Versuch V - Branched-ResNet50}{47}{subsection.6.3}%
\contentsline {paragraph}{Selective-Conditional Feedforward 224$\times $224}{48}{section*.65}%
\contentsline {section}{\numberline {7}Versuche VI - Verbundene Netze / Fusion-Netze}{50}{section.7}%
\contentsline {paragraph}{Late-Fusion-Net (LFNet)}{50}{section*.68}%
\contentsline {paragraph}{Early-Fusion-Net (EFNet)}{50}{section*.69}%
\contentsline {subsection}{\numberline {7.1}Auswertung}{51}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Gegenprobe}{54}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Selective-Conditional-Feedforward}{54}{subsection.7.3}%
\contentsline {section}{\numberline {8}Optimierung}{55}{section.8}%
\contentsline {subsection}{\numberline {8.1}Anwendung auf AlexNet (OptAlexNet)}{55}{subsection.8.1}%
\contentsline {paragraph}{Custom Loss}{56}{section*.77}%
\contentsline {paragraph}{Transfer Learning}{56}{section*.78}%
\contentsline {paragraph}{Data Augmentation}{56}{section*.79}%
\contentsline {subsection}{\numberline {8.2}Anwendung auf LFNet (OptLFNet)}{58}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Selective-Conditional-Feedforward}{58}{subsection.8.3}%
\contentsline {section}{\numberline {9}Diskussion}{60}{section.9}%
\contentsline {section}{\numberline {10}Fazit}{62}{section.10}%
\contentsline {section}{Literaturverzeichnis}{64}{section*.88}%
\contentsline {section}{\numberline {A}Plantnet-300K-Datensatz}{67}{appendix.A}%
\contentsline {section}{\numberline {B}Trainingsgenauigkeiten}{69}{appendix.B}%
\contentsline {section}{\numberline {C}Übersicht Ebenen-Aktivierung}{76}{appendix.C}%
