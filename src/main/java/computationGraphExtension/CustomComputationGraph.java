package computationGraphExtension;

import org.apache.commons.io.IOUtils;
import org.deeplearning4j.nn.api.FwdPassType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.common.base.Preconditions;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.api.memory.MemoryWorkspace;
import org.nd4j.linalg.api.memory.abstracts.DummyWorkspace;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.AsyncMultiDataSetIterator;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.workspace.WorkspaceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class CustomComputationGraph extends ComputationGraph {

	public static Logger LOGGER = LoggerFactory.getLogger(CustomComputationGraph.class);

	public CustomComputationGraph(ComputationGraphConfiguration configuration) {
		super(configuration);
	}

	/**
	 * Customly changed
	 * See {@link ComputationGraph#doEvaluation(MultiDataSetIterator, IEvaluation[])}
	 */
	public <T extends IEvaluation> List<T> doCustomEvaluation(MultiDataSetIterator iterator, T... evaluations) {
		Map<Integer, List<T>> map = new TreeMap<>();
		for(int i=0; i<evaluations.length; i++) {
//			map.computeIfAbsent(i%3, key -> new ArrayList()) // TODO warn i changed something
			map.computeIfAbsent(i%6, key -> new ArrayList()) // TODO warn i changed something
					.add(evaluations[i]);
		}
		Map<Integer,List<T>> outputs = doCustomEvaluationHelper(iterator, map);
		List<T> result = new ArrayList<>();
		for(List<T> evalList : outputs.values()) {
			for(T eval : evalList) {
				result.add(eval);
			}
		}
		return result;
	}

	private <T extends IEvaluation> Map<Integer,List<T>> doCustomEvaluationHelper(MultiDataSetIterator iterator, Map<Integer, List<T>> evaluations){
		WorkspaceUtils.assertNoWorkspacesOpen("Expected no external workspaces open at start of evaluation (doEvaluationHelper)");

		if (iterator.resetSupported() && !iterator.hasNext())
			iterator.reset();

		MultiDataSetIterator iter = iterator.asyncSupported() ? new AsyncMultiDataSetIterator(iterator, 2, true) : iterator;

		WorkspaceMode cMode = configuration.getTrainingWorkspaceMode();
		configuration.setTrainingWorkspaceMode(configuration.getInferenceWorkspaceMode());

		MemoryWorkspace outputWs;
		if(getConfiguration().getInferenceWorkspaceMode() == WorkspaceMode.ENABLED){
			outputWs = Nd4j.getWorkspaceManager().getWorkspaceForCurrentThread(WS_ALL_LAYERS_ACT_CONFIG, WS_OUTPUT_MEM);
		} else {
			outputWs = new DummyWorkspace();
		}

		while (iter.hasNext()) {
			MultiDataSet next = iter.next();

			if (next.getFeatures() == null || next.getLabels() == null) {
				continue;
			}

			//Assuming single output here
			INDArray[] features = next.getFeatures();
			INDArray[] featuresMasks = next.getFeaturesMaskArrays();
			INDArray[] labels = next.getLabels();
			INDArray[] labelMasks = next.getLabelsMaskArrays();
			List<Serializable> meta = next.getExampleMetaData();

			try (MemoryWorkspace ws = outputWs.notifyScopeEntered()) {
				INDArray[] out = outputOfLayersDetached(false, FwdPassType.STANDARD, getOutputLayerIndices(), features, featuresMasks, labelMasks, true, false, ws);

				for (Integer i : evaluations.keySet()) {
					Preconditions.checkState(i >= 0 && i <labels.length, "Invalid output index: evaluation/output indices must be between 0" +
							" and numOutputs-1 (%s), got index %s", 6, (int)i);
					List<T> evalsThisOutput = evaluations.get(i);
					if (evalsThisOutput == null)
						continue;

					Preconditions.checkState(i >= 0 && i < getNumOutputArrays(), "Invalid output index: indices for outputs " +
							"must be between 0 and %s inclusive - found index %s", 6, (int) i);
					INDArray currOut = out[i];
					INDArray currLabel = labels[i];

					try (MemoryWorkspace wsO = Nd4j.getWorkspaceManager().scopeOutOfWorkspaces()) {
						for (IEvaluation evaluation : evalsThisOutput)
							evaluation.eval(currLabel, currOut, next.getLabelsMaskArray(i), meta);
					}
				}
			}
		}

		//Clear inputs, masks etc. Important to avoid leaking invalidated/out of scope arrays between iterations
		clearLayersStates();

		if (iterator.asyncSupported())
			((AsyncMultiDataSetIterator) iter).shutdown();

		configuration.setTrainingWorkspaceMode(cMode);

		return evaluations;
	}

	public static CustomComputationGraph customLoad(File core) throws Exception {
		FileInputStream is = new FileInputStream(core);

		Map<String, byte[]> files = loadZipData(is);

		boolean gotConfig = false;
		boolean gotCoefficients = false;
		boolean gotUpdaterState = false;
		boolean gotPreProcessor = false;

		String json = "";
		INDArray params = null;
		INDArray updaterState = null;
		DataSetPreProcessor preProcessor = null;

		byte[] config = files.get("configuration.json");
		if (config != null) {
			//restoring configuration

			InputStream stream = new ByteArrayInputStream(config);
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String line = "";
			StringBuilder js = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				js.append(line).append("\n");
			}
			json = js.toString();

			reader.close();
			stream.close();
			gotConfig = true;
		}

		byte[] coefficients = files.get("coefficients.bin");
		if (coefficients != null) {
			if(coefficients.length > 0) {
				InputStream stream = new ByteArrayInputStream(coefficients);
				DataInputStream dis = new DataInputStream(stream);
				params = Nd4j.read(dis);

				dis.close();
				gotCoefficients = true;
			} else {
				byte[] noParamsMarker = files.get("noParams.marker");
				gotCoefficients = (noParamsMarker != null);
			}
		}

		byte[] updaterStateEntry = files.get("updaterState.bin");
		if (updaterStateEntry != null) {
			InputStream stream = new ByteArrayInputStream(updaterStateEntry);
			DataInputStream dis = new DataInputStream(stream);
			updaterState = Nd4j.read(dis);
				dis.close();

			gotUpdaterState = true;
		}

		byte[] prep = files.get("preprocessor.bin");
		if (prep != null) {
			InputStream stream = new ByteArrayInputStream(prep);
			ObjectInputStream ois = new ObjectInputStream(stream);

			try {
				preProcessor = (DataSetPreProcessor) ois.readObject();
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}

			gotPreProcessor = true;
		}

		if (gotConfig && gotCoefficients) {
			ComputationGraphConfiguration confFromJson;
			try{
				confFromJson = CustomComputationGraphConfiguration.customFromJson(json);
				if(confFromJson.getNetworkInputs() == null && (confFromJson.getVertices() == null || confFromJson.getVertices().size() == 0)){
					//May be deserialized correctly, but mostly with null fields
					throw new RuntimeException("Invalid JSON - not a ComputationGraphConfiguration");
				}
			} catch (Exception e){
				if(e.getMessage() != null && e.getMessage().contains("registerLegacyCustomClassesForJSON")){
					throw e;
				}
				try{
					MultiLayerConfiguration.fromJson(json);
				} catch (Exception e2){
					//Invalid, and not a compgraph
					throw new RuntimeException("Error deserializing JSON ComputationGraphConfiguration. Saved model JSON is" +
							" not a valid ComputationGraphConfiguration", e);
				}
				throw new RuntimeException("Error deserializing JSON ComputationGraphConfiguration. Saved model appears to be " +
						"a MultiLayerNetwork - use ModelSerializer.restoreMultiLayerNetwork instead");
			}

			//Handle legacy config - no network DataType in config, in beta3 or earlier
			if(params != null)
				confFromJson.setDataType(params.dataType());

			CustomComputationGraph cg = new CustomComputationGraph(confFromJson);
			cg.init(params, false);


			if (gotUpdaterState && updaterState != null) {
				cg.getUpdater().setStateViewArray(updaterState);
			}
			return cg;
		} else
			throw new IllegalStateException("Model wasnt found within file: gotConfig: [" + gotConfig
					+ "], gotCoefficients: [" + gotCoefficients + "], gotUpdater: [" + gotUpdaterState + "]");
	}

	private static Map<String, byte[]> loadZipData(InputStream is) throws IOException {
		Map<String, byte[]> result = new HashMap<>();
		try (final ZipInputStream zis = new ZipInputStream(is)) {
			while (true) {
				final ZipEntry zipEntry = zis.getNextEntry();
				if (zipEntry == null)
					break;
				if(zipEntry.isDirectory() || zipEntry.getSize() > Integer.MAX_VALUE)
					throw new IllegalArgumentException();

				final int size = (int) (zipEntry.getSize());
				final byte[] data;
				if (size >= 0) { // known size
					data = IOUtils.readFully(zis, size);
				}
				else { // unknown size
					final ByteArrayOutputStream bout = new ByteArrayOutputStream();
					IOUtils.copy(zis, bout);
					data = bout.toByteArray();
				}
				result.put(zipEntry.getName(), data);
			}
		}
		return result;
	}
}
