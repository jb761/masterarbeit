package computationGraphExtension;

import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.distribution.Distribution;
import org.deeplearning4j.nn.conf.graph.GraphVertex;
import org.deeplearning4j.nn.conf.graph.LayerVertex;
import org.deeplearning4j.nn.conf.layers.BaseLayer;
import org.deeplearning4j.nn.conf.layers.Layer;
import org.deeplearning4j.nn.conf.serde.JsonMappers;
import org.deeplearning4j.nn.weights.IWeightInit;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.activations.IActivation;
import org.nd4j.shade.jackson.databind.JsonNode;
import org.nd4j.shade.jackson.databind.ObjectMapper;
import org.nd4j.shade.jackson.databind.exc.InvalidTypeIdException;

import java.io.IOException;
import java.util.Map;

public class CustomComputationGraphConfiguration extends ComputationGraphConfiguration {

	public static ComputationGraphConfiguration customFromJson(String json) {
		//As per MultiLayerConfiguration.fromJson()
		ObjectMapper mapper = NeuralNetConfiguration.mapper();
		ComputationGraphConfiguration conf;
		try {
			conf = mapper.readValue(json, ComputationGraphConfiguration.class);
		} catch (InvalidTypeIdException e){
			if(e.getMessage().contains("@class")){
				try{
					//JSON may be legacy (1.0.0-alpha or earlier), attempt to load it using old format
					return JsonMappers.getLegacyMapper().readValue(json, ComputationGraphConfiguration.class);
				} catch (InvalidTypeIdException e2){
					//Check for legacy custom layers: "Could not resolve type id 'CustomLayer' as a subtype of [simple type, class org.deeplearning4j.nn.conf.layers.Layer]: known type ids = [Bidirectional, CenterLossOutputLayer, CnnLossLayer, ..."
					//1.0.0-beta5: dropping support for custom layers defined in pre-1.0.0-beta format. Built-in layers from these formats still work
					String msg = e2.getMessage();
					if(msg != null && msg.contains("Could not resolve type id")){
						throw new RuntimeException("Error deserializing ComputationGraphConfiguration - configuration may have a custom " +
								"layer, vertex or preprocessor, in pre version 1.0.0-beta JSON format.\nModels in legacy format with custom" +
								" layers should be loaded in 1.0.0-beta to 1.0.0-beta4 and saved again, before loading in the current version of DL4J", e);
					}
					throw new RuntimeException(e2);
				} catch (IOException e2){
					throw new RuntimeException(e2);
				}
			}
			throw new RuntimeException(e);
		} catch (Exception e) {
			//Check if this exception came from legacy deserializer...
			String msg = e.getMessage();
			if(msg != null && msg.contains("legacy")){
				throw new RuntimeException("Error deserializing ComputationGraphConfiguration - configuration may have a custom " +
						"layer, vertex or preprocessor, in pre version 1.0.0-alpha JSON format. These layers can be " +
						"deserialized by first registering them with NeuralNetConfiguration.registerLegacyCustomClassesForJSON(Class...)", e);
			}
			throw new RuntimeException(e);
		}

		//To maintain backward compatibility after activation function refactoring (configs generated with v0.7.1 or earlier)
		// Previously: enumeration used for activation functions. Now: use classes
		int layerCount = 0;
		Map<String, GraphVertex> vertexMap = conf.getVertices();
		JsonNode vertices = null;
		for (Map.Entry<String, GraphVertex> entry : vertexMap.entrySet()) {

			if (!(entry.getValue() instanceof LayerVertex)) {
				continue;
			}

			LayerVertex lv = (LayerVertex) entry.getValue();
			if (lv.getLayerConf() != null && lv.getLayerConf().getLayer() != null) {
				Layer layer = lv.getLayerConf().getLayer();

				if (layer instanceof BaseLayer && ((BaseLayer) layer).getActivationFn() == null) {
					String layerName = layer.getLayerName();

					try {
						if (vertices == null) {
							JsonNode jsonNode = mapper.readTree(json);
							vertices = jsonNode.get("vertices");
						}

						JsonNode vertexNode = vertices.get(layerName);
						JsonNode layerVertexNode = vertexNode.get("LayerVertex");
						if (layerVertexNode == null || !layerVertexNode.has("layerConf")
								|| !layerVertexNode.get("layerConf").has("layer")) {
							continue;
						}
						JsonNode layerWrapperNode = layerVertexNode.get("layerConf").get("layer");

						if (layerWrapperNode == null || layerWrapperNode.size() != 1) {
							continue;
						}

						JsonNode layerNode = layerWrapperNode.elements().next();
						JsonNode activationFunction = layerNode.get("activationFunction"); //Should only have 1 element: "dense", "output", etc

						if (activationFunction != null) {
							IActivation ia = Activation.fromString(activationFunction.asText()).getActivationFunction();
							((BaseLayer) layer).setActivationFn(ia);
						}

					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				handleLegacyWeightInitFromJson(json, layer, mapper, vertices);
			}
		}

		return conf;
	}

	private static void handleLegacyWeightInitFromJson(String json, Layer layer, ObjectMapper mapper, JsonNode vertices) {
		if (layer instanceof BaseLayer && ((BaseLayer) layer).getWeightInitFn() == null) {
			String layerName = layer.getLayerName();

			try {
				if (vertices == null) {
					JsonNode jsonNode = mapper.readTree(json);
					vertices = jsonNode.get("vertices");
				}

				JsonNode vertexNode = vertices.get(layerName);
				JsonNode layerVertexNode = vertexNode.get("LayerVertex");
				if (layerVertexNode == null || !layerVertexNode.has("layerConf")
						|| !layerVertexNode.get("layerConf").has("layer")) {
					return;
				}
				JsonNode layerWrapperNode = layerVertexNode.get("layerConf").get("layer");

				if (layerWrapperNode == null || layerWrapperNode.size() != 1) {
					return;
				}

				JsonNode layerNode = layerWrapperNode.elements().next();
				JsonNode weightInit = layerNode.get("weightInit"); //Should only have 1 element: "dense", "output", etc
				JsonNode distribution = layerNode.get("dist");

				Distribution dist = null;
				if(distribution != null) {
					dist = mapper.treeToValue(distribution, Distribution.class);
				}

				if (weightInit != null) {
					final IWeightInit wi = WeightInit.valueOf(weightInit.asText()).getWeightInitFunction(dist);
					((BaseLayer) layer).setWeightInitFn(wi);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
