package computationGraphExtension;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.lossfunctions.impl.LossNegativeLogLikelihood;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.TreeMap;

public class CustomLoss extends LossNegativeLogLikelihood {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomLoss.class);

	public static Map<Integer, CustomLoss> staticCustomLossMap = new TreeMap<>();

	private Integer id;

	public CustomLoss() {
		super();
		CustomLoss loss = this;
		new Thread() {
			public void run() {
				try {
					Thread.sleep(100);
					LOGGER.warn("unsupported Constructor call: " + loss.weights.length());
					switch ((int)loss.weights.length()) {
						case 16:
							staticCustomLossMap.put(6, loss);
							break;
						case 29:
							staticCustomLossMap.put(5, loss);
							break;
						case 59:
							staticCustomLossMap.put(4, loss);
							break;
						case 113:
							staticCustomLossMap.put(3, loss);
							break;
						case 224:
							staticCustomLossMap.put(2, loss);
							break;
						case 1019:
							staticCustomLossMap.put(1, loss);
							break;
					}
				} catch(Exception e) {
					System.out.println(e);
				}
			}
		}.start();
	}

	public CustomLoss(Integer id, INDArray weighs) {
		super(weighs);
		staticCustomLossMap.computeIfAbsent(id, key -> this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		staticCustomLossMap.computeIfAbsent(id, key -> this);
	}
}
