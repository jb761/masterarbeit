package computationGraphExtension;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.impl.LossNegativeLogLikelihood;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.TreeMap;

public class CustomLoss_Opposite extends LossNegativeLogLikelihood {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomLoss_Opposite.class);

	public static Map<Integer, CustomLoss_Opposite> staticCustomLossMap = new TreeMap<>();

	private Integer id;

	public CustomLoss_Opposite() {
		super();
		CustomLoss_Opposite loss = this;
		new Thread() {
			public void run() {
				try {
					Thread.sleep(200);
					int ID = (int)loss.weights.getFloat(0);
					float currentValue = loss.weights.getFloat(1);
					LOGGER.warn("unsupported Constructor call: "
							+ loss.weights.length() + " --> "
							+ "ID: " + ID + " / currValue:" + currentValue);
					loss.setWeights(Nd4j.zeros(1, (int)loss.weights.length()).addi(currentValue));
					staticCustomLossMap.put(ID, loss);
				} catch(Exception e) {
					System.out.println(e);
				}
			}
		}.start();
	}

	public CustomLoss_Opposite(Integer id, INDArray weighs) {
		super(weighs);
		staticCustomLossMap.computeIfAbsent(id, key -> this);
	}

	public static void persistAll() {
		LOGGER.info("PERSIST");
		for(Map.Entry<Integer, CustomLoss_Opposite> entry : staticCustomLossMap.entrySet()) {
			float[] weights = entry.getValue().weights.toFloatVector();
			weights[0] = entry.getKey();
			entry.getValue().weights = Nd4j.create(weights);
		}
		staticCustomLossMap.clear();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		staticCustomLossMap.computeIfAbsent(id, key -> this);
	}
}
