package datasetIterator;

import head.Main;
import netType.D76;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.DAD76;
import util.ImageData;
import util.Stopwatch;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DAD76_PlantNetImageProvider implements MultiDataSetIterator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetImageProvider.class);

	private final boolean TRAIN_ENABLED;
	private final boolean TEST_ENABLED;
	private final boolean VAL_ENABLED;

	private MultiDataSet dataSet;
	private int samplesShown;
	private int batchSize;
	private int maxSamples;

	private int trainingRotation;
	private int testRotation;
	private int validationRotation;

	private Map<DataSetType, Future<float[][][]>> dataAndClassesLoader;

	public DAD76_PlantNetImageProvider(int train, int test, int val) {
		TRAIN_ENABLED = train == 1;
		TEST_ENABLED = test == 1;
		VAL_ENABLED = val == 1;

		trainingRotation = 0;
		testRotation = 0;
		validationRotation = 0;

		dataAndClassesLoader = new HashMap<>();
		if(TRAIN_ENABLED) {
			Collections.shuffle(DAD76.trainingImageList, new Random(Main.SEED));
//			dataAndClassesLoader.put(DataSetType.TRAIN, shadowLoadNextPartition(DataSetType.TRAIN));
		}
		if(TEST_ENABLED) {
			Collections.shuffle(DAD76.testImageList, new Random(Main.SEED));
//			dataAndClassesLoader.put(DataSetType.TEST, shadowLoadNextPartition(DataSetType.TEST));
		}
		if(VAL_ENABLED) {
			Collections.shuffle(DAD76.validationImageList, new Random(Main.SEED));
//			dataAndClassesLoader.put(DataSetType.VALIDATION, shadowLoadNextPartition(DataSetType.VALIDATION));
		}
		loadNextRotation(DataSetType.TRAIN);
	}

	public void loadNextRotation(DataSetType dataSetType) {
		try {
			Nd4j.getMemoryManager().invokeGc();
			if(!dataAndClassesLoader.containsKey(dataSetType)) {
				dataAndClassesLoader.put(dataSetType, shadowLoadNextPartition(dataSetType));
			}
			float[][][] dataAndClasses = dataAndClassesLoader.get(dataSetType).get();
			float[][] imageDataMatrix = dataAndClasses[0];
			float[][] classesCombinedMatrix = dataAndClasses[1];
			dataAndClassesLoader.replace(dataSetType, shadowLoadNextPartition(dataSetType));

			INDArray imageData = Nd4j.create(imageDataMatrix);
			Nd4j.getMemoryManager().invokeGc();

			int imageCount = classesCombinedMatrix.length;

			float[][] E6matrix = new float[imageCount][];
			float[][] E5matrix = new float[imageCount][];
			float[][] E4matrix = new float[imageCount][];
			float[][] E3matrix = new float[imageCount][];
			float[][] E2matrix = new float[imageCount][];
			float[][] E1matrix = new float[imageCount][];

			for(int i=0; i<imageCount; i++) {
				float[] labelsCombined = classesCombinedMatrix[i];
				E6matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E6[0], D76.E6[1]);
				E5matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E5[0], D76.E5[1]);
				E4matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E4[0], D76.E4[1]);
				E3matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E3[0], D76.E3[1]);
				E2matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E2[0], D76.E2[1]);
				E1matrix[i] = Arrays.copyOfRange(labelsCombined, D76.E1[0], D76.E1[1]);
			}

			dataSet = new MultiDataSet();
			dataSet.setFeatures(new INDArray[]{imageData});
			dataSet.setLabels(new INDArray[] {
					Nd4j.create(E6matrix),
					Nd4j.create(E5matrix),
					Nd4j.create(E4matrix),
					Nd4j.create(E3matrix),
					Nd4j.create(E2matrix),
					Nd4j.create(E1matrix),
			});

			switch (dataSetType) {
				case TRAIN: {
					if(TRAIN_ENABLED) {
						trainingRotation = (trainingRotation + 1) % D76.TRAINING_ROTATION_COUNT;
						batchSize = D76.TRAINING_BATCH_SIZE;
					}
					break;
				}
				case TEST: {
					if(TEST_ENABLED) {
						testRotation = (testRotation + 1) % D76.TEST_ROTATION_COUNT;
						batchSize = Math.min(imageCount, D76.TEST_BATCH_SIZE);
					}
					break;
				}
				case VALIDATION: {
					if(VAL_ENABLED) {
						validationRotation = (validationRotation + 1) % D76.VALIDATION_ROTATION_COUNT;
						batchSize = Math.min(imageCount, D76.VALIDATION_BATCH_SIZE);
					}
					break;
				}
			}
			samplesShown = 0;
			maxSamples = imageCount;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public void loadNextRotation(DataSetType dataSetType) {
//		boolean testOrTrain = DataSetType.TEST == dataSetType;
//		try {
//			Nd4j.getMemoryManager().invokeGc();
//			INDArray imageData = Nd4j.create(activeLoadNextRAW(testOrTrain?
//					String.format("%s\\raw_%d.raw", D36.RAW_DATA_PATH_TEST, testRotation) :
//					String.format("%s\\raw_%d.raw", D36.RAW_DATA_PATH_TRAINING, trainingRotation)));
//			Nd4j.getMemoryManager().invokeGc();
//
//			float[][] classesCombined = FileUtils.readFormFile(String.format("%s\\classes_%d.raw",
//					(testOrTrain? D36.RAW_CLASSES_PATH_TEST : D36.RAW_CLASSES_PATH_TRAINING),
//					(testOrTrain? testRotation : trainingRotation)));
//
//			int imageCount = classesCombined.length;
//
//			float[][] E6matrix = new float[imageCount][];
//			float[][] E5matrix = new float[imageCount][];
//			float[][] E4matrix = new float[imageCount][];
//			float[][] E3matrix = new float[imageCount][];
//			float[][] E2matrix = new float[imageCount][];
//			float[][] E1matrix = new float[imageCount][];
//
//			for(int i=0; i<imageCount; i++) {
//				float[] labelsCombined = classesCombined[i];
//				E6matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E6[0], D36.E6[1]);
//				E5matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E5[0], D36.E5[1]);
//				E4matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E4[0], D36.E4[1]);
//				E3matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E3[0], D36.E3[1]);
//				E2matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E2[0], D36.E2[1]);
//				E1matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E1[0], D36.E1[1]);
//			}
//
//			dataSet = new MultiDataSet();
//			dataSet.setFeatures(new INDArray[]{imageData});
//			dataSet.setLabels(new INDArray[] {
//					Nd4j.create(E6matrix),
//					Nd4j.create(E5matrix),
//					Nd4j.create(E4matrix),
//					Nd4j.create(E3matrix),
//					Nd4j.create(E2matrix),
//					Nd4j.create(E1matrix),
//			});
//
//			if(testOrTrain) {
//				testRotation = (testRotation + 1) % D36.TEST_ROTATION_COUNT;
//			}else {
//				trainingRotation = (trainingRotation + 1) % D36.TRAINING_ROTATION_COUNT;
//			}
//			samplesShown = 0;
//			batchSize = (testOrTrain? D36.TEST_BATCH_SIZE : D36.TRAINING_BATCH_SIZE);
//			maxSamples = (testOrTrain? D36.TEST_ROTATION_SIZE : D36.TRAINING_ROTATION_SIZE);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private float[][] activeLoadNextRAW(String path) throws Exception {
//		File rotationFile = new File(path);
//		FileInputStream fis = new FileInputStream(rotationFile);
//		ObjectInputStream ois = new ObjectInputStream(fis);
//		Object rotationRAW = ois.readObject();
//		return (float[][])rotationRAW;
////
////		float[][] images = (float[][])rotationRAW;
////		for(int x=0; x<images.length; x++) {
////			for(int y=0; y<images[x].length; y++) {
////				images[x][y] = (1.0f + images[x][y]) / 2.0f;
////			}
////		}
////		return images;
//	}

	@Override
	public MultiDataSet next(int num) {
		LOGGER.error("next(" + num + ")");
		throw new UnsupportedOperationException("next(num)");
	}

	@Override
	public boolean resetSupported() {
		return true;
	}

	@Override
	public boolean asyncSupported() {
		return false;
	}

	@Override
	public void reset() {
		samplesShown = 0;
	}

	@Override
	public void setPreProcessor(MultiDataSetPreProcessor preProcessor) {
		// do nothing.
	}

	@Override
	public MultiDataSetPreProcessor getPreProcessor() {
		return null;
	}

	@Override
	public boolean hasNext() {
		return samplesShown < maxSamples;
	}

	@Override
	public MultiDataSet next() {
		INDArray[] features = new INDArray[1];
		features[0] = dataSet.getFeatures(0)
				.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize), NDArrayIndex.all());

		INDArray[] labels = new INDArray[6];
		for(int i=0; i<6; i++) {
			labels[i] = dataSet.getLabels(i)
					.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize));
		}

		MultiDataSet next = new MultiDataSet();
		next.setFeatures(features);
		next.setLabels(labels);

		samplesShown += batchSize;
		return next;
	}

	private Future<float[][][]> shadowLoadNextPartition(DataSetType type) {
		return Executors.newSingleThreadExecutor().submit(() -> {
			Stopwatch sw = new Stopwatch().start();
			switch (type) {
				case TRAIN: {
					List<ImageData> images = DAD76.trainingImageList.subList(
							trainingRotation * D76.TRAINING_ROTATION_SIZE,
							Math.min((1+trainingRotation) * D76.TRAINING_ROTATION_SIZE, D76.TRAINING_TOTAL));
					float[][][] result = new float[2][images.size()][];
					Future<float[]>[] futures = new Future[images.size()];
					for(int i=0; i<images.size(); i++) {
						futures[i] = images.get(i).getDataVector(true);
					}
					for(int i=0; i<images.size(); i++) {
						result[0][i] = futures[i].get();
						result[1][i] = images.get(i).getClassVector();
					}
//				LOGGER.info(String.format("TRAINING-Rotation %d loaded within %dms", trainingRotation, sw.get()));
					return result;
				}
				case TEST: {
					List<ImageData> images = DAD76.testImageList.subList(
							testRotation * D76.TEST_BATCH_SIZE,
							Math.min((1+testRotation) * D76.TEST_BATCH_SIZE, D76.TEST_TOTAL));
					float[][][] result = new float[2][images.size()][];
					Future<float[]>[] futures = new Future[images.size()];
					for(int i=0; i<images.size(); i++) {
						futures[i] = images.get(i).getDataVector(false);
					}
					for(int i=0; i<images.size(); i++) {
						result[0][i] = futures[i].get();
						result[1][i] = images.get(i).getClassVector();
					}
//				LOGGER.info(String.format("TEST-Rotation %d loaded within %dms", testRotation, sw.get()));
					return result;
				}
				case VALIDATION: {
					List<ImageData> images = DAD76.validationImageList.subList(
							validationRotation * D76.VALIDATION_BATCH_SIZE,
							Math.min((1+validationRotation) * D76.VALIDATION_BATCH_SIZE, D76.VALIDATION_TOTAL));
					float[][][] result = new float[2][images.size()][];
					Future<float[]>[] futures = new Future[images.size()];
					for(int i=0; i<images.size(); i++) {
						futures[i] = images.get(i).getDataVector(false);
					}
					for(int i=0; i<images.size(); i++) {
						result[0][i] = futures[i].get();
						result[1][i] = images.get(i).getClassVector();
					}
				LOGGER.info(String.format("VALIDATION-Rotation %d loaded within %dms", validationRotation, sw.get()));
					return result;
				}
				default:
					throw new UnsupportedOperationException("This should not happen");
			}
		});
	}
}
