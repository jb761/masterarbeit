package datasetIterator;

import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.SamplingDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

import javax.imageio.ImageIO;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MNISTImageProvider implements DataSetIterator {

	private static final Logger LOGGER = LoggerFactory.getLogger(MNISTImageProvider.class);

	private static final String ROOT_PATH = "E:\\MNIST";

	private SamplingDataSetIterator imageProvider;

	public MNISTImageProvider(DataSetType typ, int batchSize, int imageCount, long seed) throws IOException {
		Stopwatch stopwatch = new Stopwatch().start();
		LOGGER.info(String.format("Start loading %s Dataset (batchSize:%d seed:%d)", typ, batchSize, seed));


		int X = 28;
		int Y = 28;
		float[][] images = new float[imageCount][X*Y];
		float[][] labels = new float[imageCount][10]; // TODO
		List<String> labelNames = new ArrayList<>();
		List<Integer> indexList = new ArrayList<>();

		int index = 0;
		File imagesTrainingFile = new File(ROOT_PATH + (typ == DataSetType.TEST ? "\\testing" : "\\training"));
		for(File classDir : imagesTrainingFile.listFiles()) {
			int label = Integer.valueOf(classDir.getName());
			for(File imgFile : classDir.listFiles()) {
				Raster img = ImageIO.read(imgFile).getData();
				int imgIndex = 0;
				int[] p = new int[3];
				for (int x = 0; x<X; x++) {
					for (int y = 0; y<Y; y++) {
						img.getPixel(x, y, p);
						float v = ((p[0] / 255.0f) * 2.0f - 1.0f)
								+ ((p[1] / 255.0f) * 2.0f - 1.0f)
								+ ((p[2] / 255.0f) * 2.0f - 1.0f);
						images[index][imgIndex++] = v / 3.0f;
					}
				}
				labels[index][label] = 1;
				labelNames.add(classDir.getName());
				indexList.add(index++);
			}
		}

		Collections.shuffle(indexList, new Random(seed));
		float[][] imagesShuffled = new float[imageCount][];
		float[][] labelsShuffled = new float[imageCount][];
		int j = 0;
		for(Integer i : indexList) {
			imagesShuffled[j] = images[i];
			labelsShuffled[j] = labels[i];
			j++;
		}

		INDArray imageArray = Nd4j.create(imagesShuffled);
		INDArray labelArray = Nd4j.create(labelsShuffled);

		DataSet dataSet = new DataSet(imageArray, labelArray, null, null);
		dataSet.setLabelNames(labelNames);

		imageProvider = new SamplingDataSetIterator(dataSet, batchSize, imageCount);

		LOGGER.info(String.format("Done loading %d images within %d milliSec.", imageCount, stopwatch.get()));
	}

	@Override
	public DataSet next(int num) {
		return imageProvider.next();
	}

	@Override
	public int inputColumns() {
		return imageProvider.inputColumns();
	}

	@Override
	public int totalOutcomes() {
		return imageProvider.totalOutcomes();
	}

	@Override
	public boolean resetSupported() {
		return imageProvider.resetSupported();
	}

	@Override
	public boolean asyncSupported() {
		return imageProvider.asyncSupported();
	}

	@Override
	public void reset() {
		imageProvider.reset();
	}

	@Override
	public int batch() {
		return imageProvider.batch();
	}

	@Override
	public void setPreProcessor(DataSetPreProcessor preProcessor) {
		imageProvider.setPreProcessor(preProcessor);
	}

	@Override
	public DataSetPreProcessor getPreProcessor() {
		return imageProvider.getPreProcessor();
	}

	@Override
	public List<String> getLabels() {
		return imageProvider.getLabels();
	}

	@Override
	public boolean hasNext() {
		return imageProvider.hasNext();
	}

	@Override
	public DataSet next() {
		return imageProvider.next();
	}
}
