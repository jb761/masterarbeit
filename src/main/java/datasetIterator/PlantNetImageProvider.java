package datasetIterator;

import netType.PlantNetType;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PlantNetImageProvider implements DataSetIterator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetImageProvider.class);

	private final PlantNetType PLANTNET_TYPE;

//	private SamplingDataSetIterator imageProvider;
	private DataSet dataSet;
	private int samplesShown;
	private int batchSize;
	private int maxSamples;
	private List<String> labels;
	private int trainingRotation;
	private int testRotation;

	private Future<float[][]> nextTrainingRAW;
	private Future<float[][]> nextTestRAW;

	public PlantNetImageProvider(PlantNetType plantNetType) {
		this.PLANTNET_TYPE = plantNetType;
		trainingRotation = 0;
		testRotation = 0;
		nextTrainingRAW = shadowLoadNextRAW(String.format("%s\\raw_0.raw", PLANTNET_TYPE.RAW_DATA_PATH_TRAINING));
		nextTestRAW = shadowLoadNextRAW(String.format("%s\\raw_0.raw", PLANTNET_TYPE.RAW_DATA_PATH_TEST));
		loadNextRotation(DataSetType.TRAIN);
	}

	public void loadNextRotation(DataSetType dataSetType) {
		boolean testOrTrain = DataSetType.TEST == dataSetType;
		try {
			if(dataSet != null) { // Collect Garbage
				dataSet.detach();
				Nd4j.getMemoryManager().invokeGc();
			}
			INDArray imageData = Nd4j.create(
					(testOrTrain? nextTestRAW.get() : nextTrainingRAW.get()));

			File classesFile = new File(String.format("%s\\classes_%d.raw",
					(testOrTrain? PLANTNET_TYPE.RAW_CLASSES_PATH_TEST : PLANTNET_TYPE.RAW_CLASSES_PATH_TRAINING),
					(testOrTrain? testRotation : trainingRotation)));
			FileInputStream fis = new FileInputStream(classesFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object classesRAW = ois.readObject();
			INDArray classesData = Nd4j.create((float[][]) classesRAW);

//			File labelFile = new File(String.format("%s\\labelList.raw",
//					(testOrTrain? PLANTNET_TYPE.RAW_CLASSES_PATH_TEST : PLANTNET_TYPE.RAW_CLASSES_PATH_TRAINING)));
//			fis = new FileInputStream(labelFile);
//			ois = new ObjectInputStream(fis);
//			Object labelsRAW = ois.readObject();
//			labels = (ArrayList<String>)labelsRAW;

			dataSet = new DataSet(imageData, classesData, null, null);
//			dataSet.setLabelNames(labels);

//			imageProvider = new SamplingDataSetIterator(dataSet,
//					(testOrTrain? PLANTNET_TYPE.TEST_BATCH_SIZE : PLANTNET_TYPE.TRAINING_BATCH_SIZE),
//					(testOrTrain? PLANTNET_TYPE.TEST_ROTATION_SIZE : PLANTNET_TYPE.TRAINING_ROTATION_SIZE));

			if(testOrTrain) {
				testRotation = (testRotation + 1) % PLANTNET_TYPE.TEST_ROTATION_COUNT;
				nextTestRAW = shadowLoadNextRAW(String.format("%s\\raw_%d.raw", PLANTNET_TYPE.RAW_DATA_PATH_TEST, testRotation));
			}else {
				trainingRotation = (trainingRotation + 1) % PLANTNET_TYPE.TRAINING_ROTATION_COUNT;
				nextTrainingRAW = shadowLoadNextRAW(String.format("%s\\raw_%d.raw", PLANTNET_TYPE.RAW_DATA_PATH_TRAINING, trainingRotation));
			}
			samplesShown = 0;
			batchSize = (testOrTrain? PLANTNET_TYPE.TEST_BATCH_SIZE : PLANTNET_TYPE.TRAINING_BATCH_SIZE);
			maxSamples = (testOrTrain? PLANTNET_TYPE.TEST_ROTATION_SIZE : PLANTNET_TYPE.TRAINING_ROTATION_SIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Future<float[][]> shadowLoadNextRAW(String path) {
		return Executors.newSingleThreadExecutor().submit(() -> {
			File rotationFile = new File(path);
			FileInputStream fis = new FileInputStream(rotationFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object rotationRAW = ois.readObject();
			return (float[][])rotationRAW;
		});
	}

	@Override
	public DataSet next(int num) {
		LOGGER.error("next(" + num + ")");
		throw new UnsupportedOperationException("next(num)");
	}

	@Override
	public int inputColumns() {
		return dataSet.numInputs();
	}

	@Override
	public int totalOutcomes() {
		return dataSet.numOutcomes();
	}

	@Override
	public boolean resetSupported() {
		return true;
	}

	@Override
	public boolean asyncSupported() {
		return false;
	}

	@Override
	public void reset() {
		samplesShown = 0;
	}

	@Override
	public int batch() {
		return batchSize;
	}

	@Override
	public void setPreProcessor(DataSetPreProcessor preProcessor) {
		// do nothing.
	}

	@Override
	public DataSetPreProcessor getPreProcessor() {
		return null;
	}

	@Override
	public List<String> getLabels() {
		return labels;
	}

	@Override
	public boolean hasNext() {
		return samplesShown < maxSamples;
	}

	@Override
	public DataSet next() {
		DataSet ret = (DataSet) dataSet.getRange(samplesShown, samplesShown + batchSize);
		samplesShown += batchSize;
		return ret;
	}

	public PlantNetType getType() {
		return PLANTNET_TYPE;
	}
}
