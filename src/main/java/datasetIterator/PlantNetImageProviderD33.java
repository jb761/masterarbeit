package datasetIterator;

import netType.D33;
import netType.D36;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PlantNetImageProviderD33 implements MultiDataSetIterator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetImageProvider.class);

	private MultiDataSet dataSet;
	private int samplesShown;
	private int batchSize;
	private int maxSamples;
	private int trainingRotation;
	private int testRotation;

//	private Future<float[][]> nextTrainingRAW;
//	private Future<float[][]> nextTestRAW;

	public PlantNetImageProviderD33() {
		trainingRotation = 0;
		testRotation = 0;
//		nextTrainingRAW = shadowLoadNextRAW(String.format("%s\\raw_0.raw", D33.RAW_DATA_PATH_TRAINING));
//		nextTestRAW = shadowLoadNextRAW(String.format("%s\\raw_0.raw", D33.RAW_DATA_PATH_TEST));
		loadNextRotation(DataSetType.TRAIN);
	}

	public void loadNextRotation(DataSetType dataSetType) {
		boolean testOrTrain = DataSetType.TEST == dataSetType;
		try {
//			if(dataSet != null) { // Collect Garbage
//				dataSet.detach();
//				Nd4j.getMemoryManager().invokeGc();
//			}
			Nd4j.getMemoryManager().invokeGc();
//			INDArray imageData = Nd4j.create(
//					(testOrTrain? nextTestRAW.get() : nextTrainingRAW.get()));
			INDArray imageData = Nd4j.create(activeLoadNextRAW(testOrTrain?
					String.format("%s\\raw_%d.raw", D33.RAW_DATA_PATH_TEST, testRotation) :
					String.format("%s\\raw_%d.raw", D33.RAW_DATA_PATH_TRAINING, trainingRotation)));
			Nd4j.getMemoryManager().invokeGc();

			float[][] classesCombined = FileUtils.readFormFile(String.format("%s\\classes_%d.raw",
					(testOrTrain? D33.RAW_CLASSES_PATH_TEST : D33.RAW_CLASSES_PATH_TRAINING),
					(testOrTrain? testRotation : trainingRotation)));

			int imageCount = classesCombined.length;

			float[][] E3matrix = new float[imageCount][];
			float[][] E2matrix = new float[imageCount][];
			float[][] E1matrix = new float[imageCount][];

			for(int i=0; i<imageCount; i++) {
				float[] labelsCombined = classesCombined[i];
				E3matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E3[0], D36.E3[1]);
				E2matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E2[0], D36.E2[1]);
				E1matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E1[0], D36.E1[1]);
			}

			dataSet = new MultiDataSet();
			dataSet.setFeatures(new INDArray[]{imageData});
			dataSet.setLabels(new INDArray[] {
					Nd4j.create(E3matrix),
					Nd4j.create(E2matrix),
					Nd4j.create(E1matrix),
			});

			if(testOrTrain) {
				testRotation = (testRotation + 1) % D33.TEST_ROTATION_COUNT;
//				nextTestRAW = shadowLoadNextRAW(String.format("%s\\raw_%d.raw", D33.RAW_DATA_PATH_TEST, testRotation));
			}else {
				trainingRotation = (trainingRotation + 1) % D33.TRAINING_ROTATION_COUNT;
//				nextTrainingRAW = shadowLoadNextRAW(String.format("%s\\raw_%d.raw", D33.RAW_DATA_PATH_TRAINING, trainingRotation));
			}
			samplesShown = 0;
			batchSize = (testOrTrain? D33.TEST_BATCH_SIZE : D33.TRAINING_BATCH_SIZE);
			maxSamples = (testOrTrain? D33.TEST_ROTATION_SIZE : D33.TRAINING_ROTATION_SIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Future<float[][]> shadowLoadNextRAW(String path) {
		return Executors.newSingleThreadExecutor().submit(() -> {
			File rotationFile = new File(path);
			FileInputStream fis = new FileInputStream(rotationFile);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object rotationRAW = ois.readObject();
			return (float[][])rotationRAW;
		});
	}

	private float[][] activeLoadNextRAW(String path) throws Exception {
		File rotationFile = new File(path);
		FileInputStream fis = new FileInputStream(rotationFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object rotationRAW = ois.readObject();
		return (float[][])rotationRAW;
	}

	@Override
	public MultiDataSet next(int num) {
		LOGGER.error("next(" + num + ")");
		throw new UnsupportedOperationException("next(num)");
	}

	@Override
	public boolean resetSupported() {
		return true;
	}

	@Override
	public boolean asyncSupported() {
		return false;
	}

	@Override
	public void reset() {
		samplesShown = 0;
	}

	@Override
	public void setPreProcessor(MultiDataSetPreProcessor preProcessor) {
		// do nothing.
	}

	@Override
	public MultiDataSetPreProcessor getPreProcessor() {
		return null;
	}

	@Override
	public boolean hasNext() {
		return samplesShown < maxSamples;
	}

	@Override
	public MultiDataSet next() {
		INDArray[] features = new INDArray[1];
		features[0] = dataSet.getFeatures(0)
				.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize), NDArrayIndex.all());

		INDArray[] labels = new INDArray[3];
		for(int i=0; i<3; i++) {
			labels[i] = dataSet.getLabels(i)
					.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize));
		}

		MultiDataSet next = new MultiDataSet();
		next.setFeatures(features);
		next.setLabels(labels);

		samplesShown += batchSize;
		return next;
	}
}
