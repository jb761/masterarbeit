package datasetIterator;

import netType.D36;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;

public class PlantNetImageProviderD36 implements MultiDataSetIterator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetImageProvider.class);

	private MultiDataSet dataSet;
	private int samplesShown;
	private int batchSize;
	private int maxSamples;
	private int trainingRotation;
	private int testRotation;

	public PlantNetImageProviderD36() {
		trainingRotation = 0;
		testRotation = 0;
		loadNextRotation(DataSetType.TRAIN);
	}

	public void loadNextRotation(DataSetType dataSetType) {
		boolean testOrTrain = DataSetType.TEST == dataSetType;
		try {
			Nd4j.getMemoryManager().invokeGc();
			INDArray imageData = Nd4j.create(activeLoadNextRAW(testOrTrain?
					String.format("%s\\raw_%d.raw", D36.RAW_DATA_PATH_TEST, testRotation) :
					String.format("%s\\raw_%d.raw", D36.RAW_DATA_PATH_TRAINING, trainingRotation)));
			Nd4j.getMemoryManager().invokeGc();

			float[][] classesCombined = FileUtils.readFormFile(String.format("%s\\classes_%d.raw",
					(testOrTrain? D36.RAW_CLASSES_PATH_TEST : D36.RAW_CLASSES_PATH_TRAINING),
					(testOrTrain? testRotation : trainingRotation)));

			int imageCount = classesCombined.length;

			float[][] E6matrix = new float[imageCount][];
			float[][] E5matrix = new float[imageCount][];
			float[][] E4matrix = new float[imageCount][];
			float[][] E3matrix = new float[imageCount][];
			float[][] E2matrix = new float[imageCount][];
			float[][] E1matrix = new float[imageCount][];

			for(int i=0; i<imageCount; i++) {
				float[] labelsCombined = classesCombined[i];
				E6matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E6[0], D36.E6[1]);
				E5matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E5[0], D36.E5[1]);
				E4matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E4[0], D36.E4[1]);
				E3matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E3[0], D36.E3[1]);
				E2matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E2[0], D36.E2[1]);
				E1matrix[i] = Arrays.copyOfRange(labelsCombined, D36.E1[0], D36.E1[1]);
			}

			dataSet = new MultiDataSet();
			dataSet.setFeatures(new INDArray[]{imageData});
			dataSet.setLabels(new INDArray[] {
					Nd4j.create(E6matrix),
					Nd4j.create(E5matrix),
					Nd4j.create(E4matrix),
					Nd4j.create(E3matrix),
					Nd4j.create(E2matrix),
					Nd4j.create(E1matrix),
			});

			if(testOrTrain) {
				testRotation = (testRotation + 1) % D36.TEST_ROTATION_COUNT;
			}else {
				trainingRotation = (trainingRotation + 1) % D36.TRAINING_ROTATION_COUNT;
			}
			samplesShown = 0;
			batchSize = (testOrTrain? D36.TEST_BATCH_SIZE : D36.TRAINING_BATCH_SIZE);
			maxSamples = (testOrTrain? D36.TEST_ROTATION_SIZE : D36.TRAINING_ROTATION_SIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private float[][] activeLoadNextRAW(String path) throws Exception {
		File rotationFile = new File(path);
		FileInputStream fis = new FileInputStream(rotationFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object rotationRAW = ois.readObject();
		return (float[][])rotationRAW;
	}

	@Override
	public MultiDataSet next(int num) {
		LOGGER.error("next(" + num + ")");
		throw new UnsupportedOperationException("next(num)");
	}

	@Override
	public boolean resetSupported() {
		return true;
	}

	@Override
	public boolean asyncSupported() {
		return false;
	}

	@Override
	public void reset() {
		samplesShown = 0;
	}

	@Override
	public void setPreProcessor(MultiDataSetPreProcessor preProcessor) {
		// do nothing.
	}

	@Override
	public MultiDataSetPreProcessor getPreProcessor() {
		return null;
	}

	@Override
	public boolean hasNext() {
		return samplesShown < maxSamples;
	}

	@Override
	public MultiDataSet next() {
		INDArray[] features = new INDArray[1];
		features[0] = dataSet.getFeatures(0)
				.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize), NDArrayIndex.all());

		INDArray[] labels = new INDArray[6];
		for(int i=0; i<6; i++) {
			labels[i] = dataSet.getLabels(i)
					.get(NDArrayIndex.interval(samplesShown, samplesShown + batchSize));
		}

		MultiDataSet next = new MultiDataSet();
		next.setFeatures(features);
		next.setLabels(labels);

		samplesShown += batchSize;
		return next;
	}
}
