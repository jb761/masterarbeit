package evaluation;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class CombinedMacroAVGTop1AccuracyEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(CombinedMacroAVGTop1AccuracyEvaluator.class);

	private final int C;
	private final String NAME;
	private final int START;
	private final int END;
	private float[] hit;
	private float[] count;
	private boolean[] resets;

	public CombinedMacroAVGTop1AccuracyEvaluator(int classes, String name, int start, int end) {
		this.C = classes;
		this.NAME = name;
		this.START = start;
		this.END = end;
		hit = new float[C];
		count = new float[C];
		resets = new boolean[C];
		for(int i=0; i<resets.length; i++) {
			resets[i] = true;
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for (int r = 0; r < rows; r++) {
			int label = getMaxIndex(Arrays.copyOfRange(labels.getRow(r).toFloatVector(), START, END));
			int current = getMaxIndex(Arrays.copyOfRange(networkPredictions.getRow(r).toFloatVector(), START, END));
			if(resets[label]) {
				resets[label] = false;
				hit[label] = 0;
				count[label] = 0;
			}
			if(label == current) {
				hit[label]++;
			}
			count[label]++;
		}
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		for(int i=0; i<resets.length; i++) {
			resets[i] = true;
		}
	}

	@Override
	public String stats() {
		float rel = 0.0f;
		int absClasses = 0;
		for(int c=0; c<C; c++) {
			if(count[c] != 0) {
				rel += hit[c] / count[c];
				absClasses++;
			}
		}
		return String.format("MacroAVG-Top-1_Accuracy: %s over %d (of %d) classes: %3.5f%%",NAME, absClasses, C, (100.0*rel)/absClasses);
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new CombinedMacroAVGTop1AccuracyEvaluator(C, NAME, START, END);
	}
}
