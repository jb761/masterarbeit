package evaluation;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class CombinedTop1AccuracyEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(CombinedTop1AccuracyEvaluator.class);

	private final String NAME;
	private final int START;
	private final int END;

	private float hit = 0;
	private float count = 0;

	public CombinedTop1AccuracyEvaluator(String name, int start, int end) {
		this.NAME = name;
		this.START = start;
		this.END = end;
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for(int r=0; r<rows; r++) {
			float[] labelRow = Arrays.copyOfRange(labels.getRow(r).toFloatVector(), START, END);
			float[] currentRow = Arrays.copyOfRange(networkPredictions.getRow(r).toFloatVector(), START, END);
			if(getMaxIndex(labelRow) == getMaxIndex(currentRow)) {
				hit++;
			}
		}
		count += rows;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		hit = 0;
		count = 0;
	}

	@Override
	public String stats() {
		return String.format("Top-1-Accuracy: %s: %3.2f%%", NAME, (100.0*hit/count));
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new CombinedTop1AccuracyEvaluator(NAME, START, END);
	}
}
