package evaluation;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MacroAVGTop1AccuracyEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(MacroAVGTop1AccuracyEvaluator.class);

	private final int C;
	private float[] hit;
	private float[] count;
	private boolean[] resets;

	public MacroAVGTop1AccuracyEvaluator(int classes) {
		this.C = classes;
		hit = new float[C];
		count = new float[C];
		resets = new boolean[C];
		for(int i=0; i<resets.length; i++) {
			resets[i] = true;
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for (int r = 0; r < rows; r++) {
			int label = getMaxIndex(labels.getRow(r));
			int current = getMaxIndex(networkPredictions.getRow(r));
			if(resets[label]) {
				resets[label] = false;
				hit[label] = 0;
				count[label] = 0;
			}
			if(label == current) {
				hit[label]++;
			}
			count[label]++;
		}

//		search: for(int i=0; i<1016; i++) {
//			if(labels.getRow(0).getFloat(i) > 0.9f) {
//				System.out.println("--> " + i);
//				break search;
//			}
//		}
//
//		System.out.println("Helper: " + helper++ + " -> " + rows + " | " + getMaxIndex(labels.getRow(0)));
	}

	private int getMaxIndex(INDArray array) {
		int length =  (int) (array.length());
		float max = -1000000.000f;
		int index = -1;
		for(int i=0; i<length; i++) {
			float value = array.getFloat(i);
			if(value > max) {
				max = value;
				index = i;
			}
		}
		return index;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		//hit = new float[C];
		//count = new float[C];
		for(int i=0; i<resets.length; i++) {
			resets[i] = true;
		}
	}

	@Override
	public String stats() {
		float rel = 0.0f;
		int absClasses = 0;
		for(int c=0; c<C; c++) {
			if(count[c] != 0) {
				rel += hit[c] / count[c];
				absClasses++;
			}
		}
		return String.format("MacroAVG-Top-1_Accuracy over %d (of %d) classes: %3.5f%%", absClasses, C, (100.0*rel)/absClasses);
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new MacroAVGTop1AccuracyEvaluator(C);
	}
}
