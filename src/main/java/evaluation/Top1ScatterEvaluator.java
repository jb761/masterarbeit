package evaluation;

import netType.D76;
import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.DAD76;

import java.util.List;

public class Top1ScatterEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(Top1ScatterEvaluator.class);

	private final String NAME;

	private int[] imagesPerClass;
	private double[] hit;
	private double[] count;

	public Top1ScatterEvaluator(String name) {
		this.NAME = name;
		switch (name) {
			case "EBENE 6": {
				hit = new double[D76.E6size];
				count = new double[D76.E6size];
				imagesPerClass = DAD76.imageCountPerClass_6;
				break;
			}
			case "EBENE 5": {
				hit = new double[D76.E5size];
				count = new double[D76.E5size];
				imagesPerClass = DAD76.imageCountPerClass_5;
				break;
			}
			case "EBENE 4": {
				hit = new double[D76.E4size];
				count = new double[D76.E4size];
				imagesPerClass = DAD76.imageCountPerClass_4;
				break;
			}
			case "EBENE 3": {
				hit = new double[D76.E3size];
				count = new double[D76.E3size];
				imagesPerClass = DAD76.imageCountPerClass_3;
				break;
			}
			case "EBENE 2": {
				hit = new double[D76.E2size];
				count = new double[D76.E2size];
				imagesPerClass = DAD76.imageCountPerClass_2;
				break;
			}
			case "EBENE 1": {
				hit = new double[D76.E1size];
				count = new double[D76.E1size];
				imagesPerClass = DAD76.imageCountPerClass_1;
				break;
			}
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for(int r=0; r<rows; r++) {
			int label = getMaxIndex(labels.getRow(r).toFloatVector());
			int current = getMaxIndex(networkPredictions.getRow(r).toFloatVector());
			if (current == label) {
				hit[label]++;
			}
			count[label]++;
		}
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		String result = String.format("\n%s:\nx,y\n", NAME);
		for(int i=0; i<hit.length; i++) {
			result += String.format("%dx %1.3f\n", imagesPerClass[i], (100.0*(hit[i] / count[i]))).replace(",", ".").replace("x",",");
		}
		return result;
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new Top1ScatterEvaluator(NAME);
	}
}
