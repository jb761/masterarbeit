package evaluation;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TopKAccuracyEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(TopKAccuracyEvaluator.class);

	private final int K;

	private float hit = 0;
	private float count = 0;

	public TopKAccuracyEvaluator(int k) {
		this.K = k;
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		int classes = (int)(labels.length() / rows);
		for(int r=0; r<rows; r++) {
			float[] labelRow = labels.getRow(r).toFloatVector();
			float[] currentRow = networkPredictions.getRow(r).toFloatVector();
			float[] maxValues = new float[K];
			int[] maxIndices = new int[K];
			for(int c=0; c<classes; c++) {
				float current = currentRow[c];
				int maxIndex = getMaxIndex(maxValues, current);
				if(maxIndex != -1) {
					maxValues[maxIndex] = current;
					maxIndices[maxIndex] = c;
				}
			}
			for(int i=0; i<K; i++) {
				if(labelRow[maxIndices[i]] == 1.0f) {
					hit++;
				}
			}
		}
		count += rows;
//		System.out.println("--> " + getMaxIndex(labels.getRow(0)));
	}

	private int getMaxIndex(float[] array, float value) {
		float min = 1000000000.000f;
		int index = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] < min) {
				min = array[i];
				index = i;
			}
		}
		if(value > min) {
			return index;
		} else {
			return -1;
		}
	}

	private int getMaxIndex(INDArray array) {
		int length =  (int) (array.length());
		float max = -1000000.000f;
		int index = -1;
		for(int i=0; i<length; i++) {
			float value = array.getFloat(i);
			if(value > max) {
				max = value;
				index = i;
			}
		}
		return index;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		hit = 0;
		count = 0;
	}

	@Override
	public String stats() {
		return String.format("Top-%d-Accuracy: %3.2f%%", K, (100.0*hit/count));
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new TopKAccuracyEvaluator(K);
	}
}
