package evaluation.condFF;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.util.*;

public class CombinedD36ConditionalSelectiveFeedForward extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(CombinedD36ConditionalSelectiveFeedForward.class);
	private static final Map<Integer, Map<Integer, List<float[][]>>> levelToPredictionListMap = new HashMap<>();
	private static final Map<Integer, Map<Integer, List<float[][]>>> levelToGTListMap = new HashMap<>();

	private int statsCount = 0;

	private final int ID;
	private final int threshold;

	private final int[] E6 = new int[] {0, 16};
	private final int[] E5 = new int[] {16, 45};
	private final int[] E4 = new int[] {45, 104};
	private final int[] E3 = new int[] {104, 217};
	private final int[] E2 = new int[] {217, 441};
	private final int[] E1 = new int[] {441, 1460};

	// OptAlexNet
	// 10 / 30 / 50 / MM
//	private final float[] T6 = new float[]{0.528f, 0.830f, 0.965f, 0.7395f};
//	private final float[] T5 = new float[]{0.499f, 0.831f, 0.970f, 0.7125f};
//	private final float[] T4 = new float[]{0.491f, 0.835f, 0.972f, 0.7015f};
//	private final float[] T3 = new float[]{0.483f, 0.824f, 0.969f, 0.6915f};
//	private final float[] T2 = new float[]{0.462f, 0.813f, 0.964f, 0.6775f};
//	private final float[] T1 = new float[]{0.310f, 0.590f, 0.797f, 0.5550f};

	// OptLFNet
	// 10 / 30 / 50 / MM
//	private final float[] T6 = new float[]{0.456f, 0.739f, 0.925f, 0.696f};
//	private final float[] T5 = new float[]{0.428f, 0.740f, 0.939f, 0.6715f};
//	private final float[] T4 = new float[]{0.416f, 0.744f, 0.941f, 0.659f};
//	private final float[] T3 = new float[]{0.444f, 0.792f, 0.958f, 0.6715f};
//	private final float[] T2 = new float[]{0.433f, 0.789f, 0.956f, 0.665f};
//	private final float[] T1 = new float[]{0.321f, 0.603f, 0.815f, 0.5685f};

	// BranchedResNet50
	// 10 / 30 / 50 / MM
	private final float[] T6 = new float[]{0.505f, 0.739f, 0.902f, 0.7375f};
	private final float[] T5 = new float[]{0.449f, 0.730f, 0.917f, 0.6895f};
	private final float[] T4 = new float[]{0.467f, 0.794f, 0.956f, 0.6980f};
	private final float[] T3 = new float[]{0.509f, 0.846f, 0.975f, 0.7210f};
	private final float[] T2 = new float[]{0.532f, 0.869f, 0.980f, 0.7310f};
	private final float[] T1 = new float[]{0.429f, 0.715f, 0.886f, 0.6595f};

	private double[] trueHit = new double[6];
	private double[] falseHit = new double[6];

	private double count = 0;

	private Map<Integer, Map<Integer, int[]>> levelToClassToStartEndArrayMap;

	public CombinedD36ConditionalSelectiveFeedForward(int id, int threshold) {
		this.ID = id;
		levelToPredictionListMap.computeIfAbsent(threshold, key -> new HashMap<>()).put(ID, new ArrayList<>());
		levelToGTListMap.computeIfAbsent(threshold, key -> new HashMap<>()).put(ID, new ArrayList<>());
		this.threshold = threshold;
		try {
			levelToClassToStartEndArrayMap = (Map<Integer, Map<Integer, int[]>>) FileUtils.readObjectFormFile("E:\\PlantNet_Dataset\\CondFF_levelToClassToStartEndArrayMap.map");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		float[][] predictions = new float[rows][];
		float[][] gt = new float[rows][];
		for(int r=0; r<rows; r++) {
			predictions[r] = networkPredictions.getRow(r).toFloatVector();
			gt[r] = labels.getRow(r).toFloatVector();
		}
		levelToPredictionListMap.get(threshold).get(ID).add(predictions);
		levelToGTListMap.get(threshold).get(ID).add(gt);
	}

	private void eval() {
		int rows = levelToGTListMap.get(threshold).get(0).get(statsCount).length;
		for(int r=0; r<rows; r++) {

			float[] combLabel = new float[1460];
			int i = 0;
			float[] current = levelToGTListMap.get(threshold).get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(threshold).get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(threshold).get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(threshold).get(3).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(threshold).get(4).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(threshold).get(5).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}

			float[] combCurrent = new float[1460];
			i = 0;
			current = levelToPredictionListMap.get(threshold).get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(threshold).get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(threshold).get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(threshold).get(3).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(threshold).get(4).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(threshold).get(5).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}

			// EBENE 6
			int[] se = new int[]{0, 16};
			float[] partialLabel = Arrays.copyOfRange(combLabel, E6[0], E6[1]);
			float[] partialCurrent = Arrays.copyOfRange(combCurrent, 0 +se[0], 0 + se[1]);
			int maxLabel = getMaxIndex(partialLabel);
			int maxCurrent = getMaxIndex(partialCurrent);
			float activation = partialCurrent[maxCurrent];
			if(activation >= T6[threshold]) { // todo <--
				if(0 + maxCurrent == maxLabel) {
					trueHit[0]++;
				}else{
					falseHit[0]++;
				}
				se = levelToClassToStartEndArrayMap.get(6).get(se[0] + maxCurrent);

				// EBENE 5
				partialLabel = Arrays.copyOfRange(combLabel, E5[0], E5[1]);
				partialCurrent = Arrays.copyOfRange(combCurrent, E5[0] + se[0], E5[0] + se[1]);
				maxLabel = getMaxIndex(partialLabel);
				maxCurrent = getMaxIndex(partialCurrent);
				activation = partialCurrent[maxCurrent];
				if(activation >= T5[threshold]) { // todo <--
					if(se[0] + maxCurrent == maxLabel) {
						trueHit[1]++;
					}else{
						falseHit[1]++;
					}
					se = levelToClassToStartEndArrayMap.get(5).get(se[0] + maxCurrent);

					// EBENE 4
					partialLabel = Arrays.copyOfRange(combLabel,  E4[0], E4[1]);
					partialCurrent = Arrays.copyOfRange(combCurrent, E4[0] + se[0], E4[0] + se[1]);
					maxLabel = getMaxIndex(partialLabel);
					maxCurrent = getMaxIndex(partialCurrent);
					activation = partialCurrent[maxCurrent];
					if(activation >= T4[threshold]) { // todo <--
						if(se[0] + maxCurrent == maxLabel) {
							trueHit[2]++;
						}else{
							falseHit[2]++;
						}
						se = levelToClassToStartEndArrayMap.get(4).get(se[0] + maxCurrent);

						// EBENE 3
						partialLabel = Arrays.copyOfRange(combLabel,  E3[0], E3[1]);
						partialCurrent = Arrays.copyOfRange(combCurrent, E3[0] + se[0], E3[0] + se[1]);
						maxLabel = getMaxIndex(partialLabel);
						maxCurrent = getMaxIndex(partialCurrent);
						activation = partialCurrent[maxCurrent];
						if(activation >= T3[threshold]) { // todo <--
							if(se[0] + maxCurrent == maxLabel) {
								trueHit[3]++;
							}else{
								falseHit[3]++;
							}
							se = levelToClassToStartEndArrayMap.get(3).get(se[0] + maxCurrent);

							// EBENE 2
							partialLabel = Arrays.copyOfRange(combLabel,  E2[0], E2[1]);
							partialCurrent = Arrays.copyOfRange(combCurrent, E2[0] + se[0], E2[0] + se[1]); // todo <--
//							partialCurrent = Arrays.copyOfRange(combCurrent, E2[0], E2[1]); // todo <--
							maxLabel = getMaxIndex(partialLabel);
							maxCurrent = getMaxIndex(partialCurrent);
							activation = partialCurrent[maxCurrent];
							if(activation >= T2[threshold]) {
								if(se[0] + maxCurrent == maxLabel) { // todo <--
//									if (maxCurrent == maxLabel) { // todo <--
									trueHit[4]++;
								} else {
									falseHit[4]++;
								}
								se = levelToClassToStartEndArrayMap.get(2).get(se[0] + maxCurrent); // todo <--
								// se = levelToClassToStartEndArrayMap.get(2).get(maxCurrent); // todo <--

								// EBENE 1
								partialLabel = Arrays.copyOfRange(combLabel,  E1[0], E1[1]);
								partialCurrent = Arrays.copyOfRange(combCurrent, E1[0] + se[0], E1[0] + se[1]);
								maxLabel = getMaxIndex(partialLabel);
								maxCurrent = getMaxIndex(partialCurrent);
								activation = partialCurrent[maxCurrent];
								if (activation >= T1[threshold]) {
									if (se[0] + maxCurrent == maxLabel) {
										trueHit[5]++;
									} else {
										falseHit[5]++;
									}
								}
							}
						} // todo <--
					} // todo <--
				} // todo <--
			} // todo <--
		}
		count += rows;
		statsCount++;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		if(this.ID == 5) {
			eval();
			return String.format("THRESHOLD: %d\n" +
					"\nSelective-FeedForward -> Ebene 6 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 5 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 4 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 3 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 2 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 1 -> TRUE:%3.3f%% FALSE:%3.3f%%",
					threshold,
					(100.0*trueHit[0]/count), (100.0*falseHit[0]/count),
					(100.0*trueHit[1]/count), (100.0*falseHit[1]/count),
					(100.0*trueHit[2]/count), (100.0*falseHit[2]/count),
					(100.0*trueHit[3]/count), (100.0*falseHit[3]/count),
					(100.0*trueHit[4]/count), (100.0*falseHit[4]/count),
					(100.0*trueHit[5]/count), (100.0*falseHit[5]/count))
					;
		}else {
			return "";
		}
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new CombinedD36ConditionalSelectiveFeedForward(ID, threshold);
	}
}
