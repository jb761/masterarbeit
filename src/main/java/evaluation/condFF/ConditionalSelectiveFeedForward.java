package evaluation.condFF;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ConditionalSelectiveFeedForward extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConditionalSelectiveFeedForward.class);

	private final int[] E6 = new int[] {0, 16};
	private final int[] E5 = new int[] {16, 45};
	private final int[] E4 = new int[] {45, 104};
	private final int[] E3 = new int[] {104, 217};
	private final int[] E2 = new int[] {217, 441};
	private final int[] E1 = new int[] {441, 1460};

//	// TRUE-50
//	private final float T6 = 0.9940f;
//	private final float T5 = 0.9940f;
//	private final float T4 = 0.9920f;
//	private final float T3 = 0.9980f;
//	private final float T2 = 0.9840f;
//	private final float T1 = 0.8900f;
	// Avg. Median
	private final float T6 = 0.8310f;
	private final float T5 = 0.8365f;
	private final float T4 = 0.8010f;
	private final float T3 = 0.7950f;
	private final float T2 = 0.7675f;
	private final float T1 = 0.6495f;
//	// TRUE-30
//	private final float T6 = 0.9380f;
//	private final float T5 = 0.9500f;
//	private final float T4 = 0.9300f;
//	private final float T3 = 0.9110f;
//	private final float T2 = 0.8960f;
//	private final float T1 = 0.7090f;
//	// TRUE-10
//	private final float T6 = 0.7160f;
//	private final float T5 = 0.7420f;
//	private final float T4 = 0.6870f;
//	private final float T3 = 0.6410f;
//	private final float T2 = 0.6420f;
//	private final float T1 = 0.5050f;


	private double[] trueHit = new double[6];
	private double[] falseHit = new double[6];

	private double count = 0;

	private Map<Integer, Map<Integer, int[]>> levelToClassToStartEndArrayMap;

	public ConditionalSelectiveFeedForward() {
		try {
			levelToClassToStartEndArrayMap = (Map<Integer, Map<Integer, int[]>>) FileUtils.readObjectFormFile("E:\\PlantNet_Dataset\\CondFF_levelToClassToStartEndArrayMap.map");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for(int r=0; r<rows; r++) {
			float[] combLabel = labels.getRow(r).toFloatVector();
			float[] combCurrent = networkPredictions.getRow(r).toFloatVector();
//			combCurrent = combLabel.clone();

			// EBENE 6
			int[] se = new int[]{0, 16};
			float[] partialLabel = Arrays.copyOfRange(combLabel, E6[0], E6[1]);
			float[] partialCurrent = Arrays.copyOfRange(combCurrent, 0 +se[0], 0 + se[1]);
			int maxLabel = getMaxIndex(partialLabel);
			int maxCurrent = getMaxIndex(partialCurrent);
			float activation = partialCurrent[maxCurrent];
			if(activation >= T6) {
				if(0 + maxCurrent == maxLabel) {
					trueHit[0]++;
				}else{
					falseHit[0]++;
				}
				se = levelToClassToStartEndArrayMap.get(6).get(se[0] + maxCurrent);

				// EBENE 5
				partialLabel = Arrays.copyOfRange(combLabel, E5[0], E5[1]);
				partialCurrent = Arrays.copyOfRange(combCurrent, E5[0] + se[0], E5[0] + se[1]);
				maxLabel = getMaxIndex(partialLabel);
				maxCurrent = getMaxIndex(partialCurrent);
				activation = partialCurrent[maxCurrent];
				if(activation >= T5) {
					if(se[0] + maxCurrent == maxLabel) {
						trueHit[1]++;
					}else{
						falseHit[1]++;
					}
					se = levelToClassToStartEndArrayMap.get(5).get(se[0] + maxCurrent);

					// EBENE 4
					partialLabel = Arrays.copyOfRange(combLabel,  E4[0], E4[1]);
					partialCurrent = Arrays.copyOfRange(combCurrent, E4[0] + se[0], E4[0] + se[1]);
					maxLabel = getMaxIndex(partialLabel);
					maxCurrent = getMaxIndex(partialCurrent);
					activation = partialCurrent[maxCurrent];
					if(activation >= T4) {
						if(se[0] + maxCurrent == maxLabel) {
							trueHit[2]++;
						}else{
							falseHit[2]++;
						}
						se = levelToClassToStartEndArrayMap.get(4).get(se[0] + maxCurrent);

						// EBENE 3
						partialLabel = Arrays.copyOfRange(combLabel,  E3[0], E3[1]);
						partialCurrent = Arrays.copyOfRange(combCurrent, E3[0] + se[0], E3[0] + se[1]);
						maxLabel = getMaxIndex(partialLabel);
						maxCurrent = getMaxIndex(partialCurrent);
						activation = partialCurrent[maxCurrent];
						if(activation >= T3) {
							if(se[0] + maxCurrent == maxLabel) {
								trueHit[3]++;
							}else{
								falseHit[3]++;
							}
							se = levelToClassToStartEndArrayMap.get(3).get(se[0] + maxCurrent);

							// EBENE 2
							partialLabel = Arrays.copyOfRange(combLabel,  E2[0], E2[1]);
							partialCurrent = Arrays.copyOfRange(combCurrent, E2[0] + se[0], E2[0] + se[1]);
							maxLabel = getMaxIndex(partialLabel);
							maxCurrent = getMaxIndex(partialCurrent);
							activation = partialCurrent[maxCurrent];
							if(activation >= T2) {
								if(se[0] + maxCurrent == maxLabel) {
									trueHit[4]++;
								}else{
									falseHit[4]++;
								}
								se = levelToClassToStartEndArrayMap.get(2).get(se[0] + maxCurrent);

								// EBENE 1
								partialLabel = Arrays.copyOfRange(combLabel,  E1[0], E1[1]);
								partialCurrent = Arrays.copyOfRange(combCurrent, E1[0] + se[0], E1[0] + se[1]);
								maxLabel = getMaxIndex(partialLabel);
								maxCurrent = getMaxIndex(partialCurrent);
								activation = partialCurrent[maxCurrent];
								if(activation >= T1) {
									if(se[0] + maxCurrent == maxLabel) {
										trueHit[5]++;
									}else{
										falseHit[5]++;
									}
								}
							}
						}
					}
				}
			}
		}
		count += rows;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		return String.format(
						"\nSelective-FeedForward -> Ebene 6 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nSelective-FeedForward -> Ebene 5 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nSelective-FeedForward -> Ebene 4 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nSelective-FeedForward -> Ebene 3 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nSelective-FeedForward -> Ebene 2 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nSelective-FeedForward -> Ebene 1 -> TRUE:%3.3f%% FALSE:%3.3f%%",
				(100.0*trueHit[0]/count), (100.0*falseHit[0]/count),
				(100.0*trueHit[1]/count), (100.0*falseHit[1]/count),
				(100.0*trueHit[2]/count), (100.0*falseHit[2]/count),
				(100.0*trueHit[3]/count), (100.0*falseHit[3]/count),
				(100.0*trueHit[4]/count), (100.0*falseHit[4]/count),
				(100.0*trueHit[5]/count), (100.0*falseHit[5]/count))
				;
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new ConditionalSelectiveFeedForward();
	}
}
