package evaluation.condFF;

import netType.D33;
import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.util.*;

public class D33ConditionalSelectiveFeedForward extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(D33ConditionalSelectiveFeedForward.class);
	private static final Map<Integer, List<float[][]>> levelToPredictionListMap = new HashMap<>();
	private static final Map<Integer, List<float[][]>> levelToGTListMap = new HashMap<>();

	private int statsCount = 0;

	private final int ID;
	private final int threshold;

	// BCNN_BaseC_224
	// 10 / 30 / 50 / MM
	private final float[] T3 = new float[]{0.410f, 0.745f, 0.941f, 0.6520f};
	private final float[] T2 = new float[]{0.519f, 0.886f, 0.987f, 0.7155f};
	private final float[] T1 = new float[]{0.468f, 0.756f, 0.910f, 0.6860f};

	private double[] trueHit = new double[3];
	private double[] falseHit = new double[3];

	private double count = 0;

	private Map<Integer, Map<Integer, int[]>> levelToClassToStartEndArrayMap;

	public D33ConditionalSelectiveFeedForward(int id, int threshold) {
		this.ID = id;
		levelToPredictionListMap.put(ID, new ArrayList<>());
		levelToGTListMap.put(ID, new ArrayList<>());
		this.threshold = threshold;
		try {
			levelToClassToStartEndArrayMap = (Map<Integer, Map<Integer, int[]>>) FileUtils.readObjectFormFile("E:\\PlantNet_Dataset\\CondFF_levelToClassToStartEndArrayMap.map");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		float[][] predictions = new float[rows][];
		float[][] gt = new float[rows][];
		for(int r=0; r<rows; r++) {
			predictions[r] = networkPredictions.getRow(r).toFloatVector();
			gt[r] = labels.getRow(r).toFloatVector();
		}
		levelToPredictionListMap.get(ID).add(predictions);
		levelToGTListMap.get(ID).add(gt);
	}

	private void eval() {
		int rows = levelToGTListMap.get(0).get(statsCount).length;
		for(int r=0; r<rows; r++) {

			float[] combLabel = new float[1356];
			int i = 0;
			float[] current = levelToGTListMap.get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}

			float[] combCurrent = new float[1356];
			i = 0;
			current = levelToPredictionListMap.get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}

			int[] se = new int[]{0, 113};
			// EBENE 3
			float[] partialLabel = Arrays.copyOfRange(combLabel, D33.E3[0], D33.E3[1]);
			float[] partialCurrent = Arrays.copyOfRange(combCurrent, D33.E3[0] + se[0], D33.E3[0] + se[1]);
			int maxLabel = getMaxIndex(partialLabel);
			int maxCurrent = getMaxIndex(partialCurrent);
			float activation = partialCurrent[maxCurrent];
			if(activation >= T3[threshold]) {
				if(se[0] + maxCurrent == maxLabel) {
					trueHit[0]++;
				}else{
					falseHit[0]++;
				}
				se = levelToClassToStartEndArrayMap.get(3).get(se[0] + maxCurrent);

				// EBENE 2
				partialLabel = Arrays.copyOfRange(combLabel,  D33.E2[0], D33.E2[1]);
				partialCurrent = Arrays.copyOfRange(combCurrent, D33.E2[0] + se[0], D33.E2[0] + se[1]);
				maxLabel = getMaxIndex(partialLabel);
				maxCurrent = getMaxIndex(partialCurrent);
				activation = partialCurrent[maxCurrent];
				if(activation >= T2[threshold]) {
					if(se[0] + maxCurrent == maxLabel) {
						trueHit[1]++;
					}else{
						falseHit[1]++;
					}
					se = levelToClassToStartEndArrayMap.get(2).get(se[0] + maxCurrent);

					// EBENE 1
					partialLabel = Arrays.copyOfRange(combLabel, D33.E1[0], D33.E1[1]);
					partialCurrent = Arrays.copyOfRange(combCurrent, D33.E1[0] + se[0], D33.E1[0] + se[1]);
					maxLabel = getMaxIndex(partialLabel);
					maxCurrent = getMaxIndex(partialCurrent);
					activation = partialCurrent[maxCurrent];
					if(activation >= T1[threshold]) {
						if(se[0] + maxCurrent == maxLabel) {
							trueHit[2]++;
						}else{
							falseHit[2]++;
						}
					}
				}
			}
		}
		count += rows;
		statsCount++;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		if(this.ID == 2) {
			eval();
			return String.format(
							"\nSelective-FeedForward -> Ebene 3 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 2 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 1 -> TRUE:%3.3f%% FALSE:%3.3f%%",
					(100.0*trueHit[0]/count), (100.0*falseHit[0]/count),
					(100.0*trueHit[1]/count), (100.0*falseHit[1]/count),
					(100.0*trueHit[2]/count), (100.0*falseHit[2]/count));
		}else {
			return "";
		}
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new D33ConditionalSelectiveFeedForward(ID, threshold);
	}
}
