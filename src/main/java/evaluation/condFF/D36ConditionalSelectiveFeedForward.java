package evaluation.condFF;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FileUtils;

import java.util.*;

public class D36ConditionalSelectiveFeedForward extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(D36ConditionalSelectiveFeedForward.class);
	private static final Map<Integer, List<float[][]>> levelToPredictionListMap = new HashMap<>();
	private static final Map<Integer, List<float[][]>> levelToGTListMap = new HashMap<>();

	private int statsCount = 0;

	private final int ID;
	private final int threshold;

	private final int[] E6 = new int[] {0, 16};
	private final int[] E5 = new int[] {16, 45};
	private final int[] E4 = new int[] {45, 104};
	private final int[] E3 = new int[] {104, 217};
	private final int[] E2 = new int[] {217, 441};
	private final int[] E1 = new int[] {441, 1460};

	// BNet6_BT
	// 10 / 30 / 50 / MM
	private final float[] T6 = new float[]{0.608f, 0.923f, 0.993f, 0.7965f};
	private final float[] T5 = new float[]{0.589f, 0.925f, 0.993f, 0.774f};
	private final float[] T4 = new float[]{0.581f, 0.920f, 0.991f, 0.759f};
	private final float[] T3 = new float[]{0.580f, 0.921f, 0.992f, 0.754f};
	private final float[] T2 = new float[]{0.569f, 0.921f, 0.992f, 0.7485f};
	private final float[] T1 = new float[]{0.422f, 0.687f, 0.863f, 0.641f};

	private double[] trueHit = new double[6];
	private double[] falseHit = new double[6];

	private double count = 0;

	private Map<Integer, Map<Integer, int[]>> levelToClassToStartEndArrayMap;

	public D36ConditionalSelectiveFeedForward(int id, int threshold) {
		this.ID = id;
		levelToPredictionListMap.put(ID, new ArrayList<>());
		levelToGTListMap.put(ID, new ArrayList<>());
		this.threshold = threshold;
		try {
			levelToClassToStartEndArrayMap = (Map<Integer, Map<Integer, int[]>>) FileUtils.readObjectFormFile("E:\\PlantNet_Dataset\\CondFF_levelToClassToStartEndArrayMap.map");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		float[][] predictions = new float[rows][];
		float[][] gt = new float[rows][];
		for(int r=0; r<rows; r++) {
			predictions[r] = networkPredictions.getRow(r).toFloatVector();
			gt[r] = labels.getRow(r).toFloatVector();
		}
		levelToPredictionListMap.get(ID).add(predictions);
		levelToGTListMap.get(ID).add(gt);
	}

	private void eval() {
		int rows = levelToGTListMap.get(0).get(statsCount).length;
		for(int r=0; r<rows; r++) {

			//float[] combLabel = labels.getRow(r).toFloatVector();
//			float[] combCurrent = networkPredictions.getRow(r).toFloatVector();
			float[] combLabel = new float[1460];
			int i = 0;
			float[] current = levelToGTListMap.get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(3).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(4).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}
			current = levelToGTListMap.get(5).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combLabel[i++] = current[l];
			}

			float[] combCurrent = new float[1460];
			i = 0;
			current = levelToPredictionListMap.get(0).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(1).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(2).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(3).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(4).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}
			current = levelToPredictionListMap.get(5).get(statsCount)[r];
			for(int l=0; l<current.length; l++) {
				combCurrent[i++] = current[l];
			}

			// EBENE 6
			int[] se = new int[]{0, 16};
			float[] partialLabel = Arrays.copyOfRange(combLabel, E6[0], E6[1]);
			float[] partialCurrent = Arrays.copyOfRange(combCurrent, 0 +se[0], 0 + se[1]);
			int maxLabel = getMaxIndex(partialLabel);
			int maxCurrent = getMaxIndex(partialCurrent);
			float activation = partialCurrent[maxCurrent];
			if(activation >= T6[threshold]) {
				if(0 + maxCurrent == maxLabel) {
					trueHit[0]++;
				}else{
					falseHit[0]++;
				}
				se = levelToClassToStartEndArrayMap.get(6).get(se[0] + maxCurrent);

				// EBENE 5
				partialLabel = Arrays.copyOfRange(combLabel, E5[0], E5[1]);
				partialCurrent = Arrays.copyOfRange(combCurrent, E5[0] + se[0], E5[0] + se[1]);
				maxLabel = getMaxIndex(partialLabel);
				maxCurrent = getMaxIndex(partialCurrent);
				activation = partialCurrent[maxCurrent];
				if(activation >= T5[threshold]) {
					if(se[0] + maxCurrent == maxLabel) {
						trueHit[1]++;
					}else{
						falseHit[1]++;
					}
					se = levelToClassToStartEndArrayMap.get(5).get(se[0] + maxCurrent);

					// EBENE 4
					partialLabel = Arrays.copyOfRange(combLabel,  E4[0], E4[1]);
					partialCurrent = Arrays.copyOfRange(combCurrent, E4[0] + se[0], E4[0] + se[1]);
					maxLabel = getMaxIndex(partialLabel);
					maxCurrent = getMaxIndex(partialCurrent);
					activation = partialCurrent[maxCurrent];
					if(activation >= T4[threshold]) {
						if(se[0] + maxCurrent == maxLabel) {
							trueHit[2]++;
						}else{
							falseHit[2]++;
						}
						se = levelToClassToStartEndArrayMap.get(4).get(se[0] + maxCurrent);

						// EBENE 3
						partialLabel = Arrays.copyOfRange(combLabel,  E3[0], E3[1]);
						partialCurrent = Arrays.copyOfRange(combCurrent, E3[0] + se[0], E3[0] + se[1]);
						maxLabel = getMaxIndex(partialLabel);
						maxCurrent = getMaxIndex(partialCurrent);
						activation = partialCurrent[maxCurrent];
						if(activation >= T3[threshold]) {
							if(se[0] + maxCurrent == maxLabel) {
								trueHit[3]++;
							}else{
								falseHit[3]++;
							}
							se = levelToClassToStartEndArrayMap.get(3).get(se[0] + maxCurrent);

							// EBENE 2
							partialLabel = Arrays.copyOfRange(combLabel,  E2[0], E2[1]);
							partialCurrent = Arrays.copyOfRange(combCurrent, E2[0] + se[0], E2[0] + se[1]);
							maxLabel = getMaxIndex(partialLabel);
							maxCurrent = getMaxIndex(partialCurrent);
							activation = partialCurrent[maxCurrent];
							if(activation >= T2[threshold]) {
								if(se[0] + maxCurrent == maxLabel) {
									trueHit[4]++;
								}else{
									falseHit[4]++;
								}
								se = levelToClassToStartEndArrayMap.get(2).get(se[0] + maxCurrent);

								// EBENE 1
								partialLabel = Arrays.copyOfRange(combLabel,  E1[0], E1[1]);
								partialCurrent = Arrays.copyOfRange(combCurrent, E1[0] + se[0], E1[0] + se[1]);
								maxLabel = getMaxIndex(partialLabel);
								maxCurrent = getMaxIndex(partialCurrent);
								activation = partialCurrent[maxCurrent];
								if(activation >= T1[threshold]) {
									if(se[0] + maxCurrent == maxLabel) {
										trueHit[5]++;
									}else{
										falseHit[5]++;
									}
								}
							}
						}
					}
				}
			}
		}
		count += rows;
		statsCount++;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		if(this.ID == 5) {
			eval();
			return String.format(
					"\nSelective-FeedForward -> Ebene 6 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 5 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 4 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 3 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 2 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
							"\nSelective-FeedForward -> Ebene 1 -> TRUE:%3.3f%% FALSE:%3.3f%%",
					(100.0*trueHit[0]/count), (100.0*falseHit[0]/count),
					(100.0*trueHit[1]/count), (100.0*falseHit[1]/count),
					(100.0*trueHit[2]/count), (100.0*falseHit[2]/count),
					(100.0*trueHit[3]/count), (100.0*falseHit[3]/count),
					(100.0*trueHit[4]/count), (100.0*falseHit[4]/count),
					(100.0*trueHit[5]/count), (100.0*falseHit[5]/count))
					;
		}else {
			return "";
		}
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new D36ConditionalSelectiveFeedForward(ID, threshold);
	}
}
