package evaluation.condFF;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class DistanceFeedForward extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistanceFeedForward.class);

	private final int[] E6 = new int[] {0, 16};
	private final int[] E5 = new int[] {16, 45};
	private final int[] E4 = new int[] {45, 104};
	private final int[] E3 = new int[] {104, 217};
	private final int[] E2 = new int[] {217, 441};
	private final int[] E1 = new int[] {441, 1460};

	private final float T[];

	private double[] trueHit = new double[6];
	private double[] falseHit = new double[6];

	private double count = 0;

	public DistanceFeedForward(float[] threshold) {
		T = threshold;
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for(int r=0; r<rows; r++) {
			float[] combLabel = labels.getRow(r).toFloatVector();
			float[] combCurrent = networkPredictions.getRow(r).toFloatVector();

			// EBENE 6
			float[] partialLabel = Arrays.copyOfRange(combLabel, E6[0], E6[1]);
			float[] partialCurrent = Arrays.copyOfRange(combCurrent, E6[0], E6[1]);
			int maxLabel = getMaxIndex(partialLabel);
			int maxCurrent = getMaxIndex(partialCurrent);

			if(validActivation(partialCurrent, 5)) {
				if(maxCurrent == maxLabel) {
					trueHit[0]++;
				}else{
					falseHit[0]++;
				}

				// EBENE 5
				partialLabel = Arrays.copyOfRange(combLabel, E5[0], E5[1]);
				partialCurrent = Arrays.copyOfRange(combCurrent, E5[0], E5[1]);
				maxLabel = getMaxIndex(partialLabel);
				maxCurrent = getMaxIndex(partialCurrent);

				if(validActivation(partialCurrent, 4)) {
					if(maxCurrent == maxLabel) {
						trueHit[1]++;
					}else{
						falseHit[1]++;
					}

					// EBENE 4
					partialLabel = Arrays.copyOfRange(combLabel,  E4[0], E4[1]);
					partialCurrent = Arrays.copyOfRange(combCurrent, E4[0], E4[1]);
					maxLabel = getMaxIndex(partialLabel);
					maxCurrent = getMaxIndex(partialCurrent);

					if(validActivation(partialCurrent, 3)) {
						if(maxCurrent == maxLabel) {
							trueHit[2]++;
						}else{
							falseHit[2]++;
						}

						// EBENE 3
						partialLabel = Arrays.copyOfRange(combLabel,  E3[0], E3[1]);
						partialCurrent = Arrays.copyOfRange(combCurrent, E3[0], E3[1]);
						maxLabel = getMaxIndex(partialLabel);
						maxCurrent = getMaxIndex(partialCurrent);

						if(validActivation(partialCurrent, 2)) {
							if(maxCurrent == maxLabel) {
								trueHit[3]++;
							}else{
								falseHit[3]++;
							}

							// EBENE 2
							partialLabel = Arrays.copyOfRange(combLabel,  E2[0], E2[1]);
							partialCurrent = Arrays.copyOfRange(combCurrent, E2[0], E2[1]);
							maxLabel = getMaxIndex(partialLabel);
							maxCurrent = getMaxIndex(partialCurrent);

							if(validActivation(partialCurrent, 1)) {
								if(maxCurrent == maxLabel) {
									trueHit[4]++;
								}else{
									falseHit[4]++;
								}

								// EBENE 1
								partialLabel = Arrays.copyOfRange(combLabel,  E1[0], E1[1]);
								partialCurrent = Arrays.copyOfRange(combCurrent, E1[0], E1[1]);
								maxLabel = getMaxIndex(partialLabel);
								maxCurrent = getMaxIndex(partialCurrent);

								if(validActivation(partialCurrent, 0)) {
									if(maxCurrent == maxLabel) {
										trueHit[5]++;
									}else{
										falseHit[5]++;
									}
								}
							}
						}
					}
				}
			}
		}
		count += rows;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	private boolean validActivation(float[] array, int pos) {
		if(array.length > 1) {
			float max = -100.0f;
			int maxA = -1;
			for(int i=0; i<array.length; i++) {
				if(array[i] > max) {
					max = array[i];
					maxA = i;
				}
			}
			max = -100.0f;
			int maxB = -1;
			for(int i=0; i<array.length; i++) {
				if(array[i] > max && i != maxA) {
					max = array[i];
					maxB = i;
				}
			}
			return (array[maxA] - array[maxB]) >= T[pos];
		} else {
			return true;
		}
	}


	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		return String.format(
						"\nDistance-FeedForward -> Ebene 6 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nDistance-FeedForward -> Ebene 5 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nDistance-FeedForward -> Ebene 4 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nDistance-FeedForward -> Ebene 3 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nDistance-FeedForward -> Ebene 2 -> TRUE:%3.3f%% FALSE:%3.3f%%" +
						"\nDistance-FeedForward -> Ebene 1 -> TRUE:%3.3f%% FALSE:%3.3f%%",
				(100.0*trueHit[0]/count), (100.0*falseHit[0]/count),
				(100.0*trueHit[1]/count), (100.0*falseHit[1]/count),
				(100.0*trueHit[2]/count), (100.0*falseHit[2]/count),
				(100.0*trueHit[3]/count), (100.0*falseHit[3]/count),
				(100.0*trueHit[4]/count), (100.0*falseHit[4]/count),
				(100.0*trueHit[5]/count), (100.0*falseHit[5]/count));
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new DistanceFeedForward(T);
	}
}
