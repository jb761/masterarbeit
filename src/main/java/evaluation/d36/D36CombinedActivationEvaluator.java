package evaluation.d36;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class D36CombinedActivationEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(D36CombinedActivationEvaluator.class);

	private final int C;
	private final String NAME;
	private List<Float> trueScores;
	private List<Float> falseScores;

	public D36CombinedActivationEvaluator(int classes, String name) {
		this.C = classes;
		this.NAME = name;
		trueScores = new ArrayList<>();
		falseScores = new ArrayList<>();
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for (int r = 0; r < rows; r++) {
			float[] labelArray = labels.getRow(r).toFloatVector();
			float[] currentArray = networkPredictions.getRow(r).toFloatVector();
			int label = getMaxIndex(labelArray);
			int current = getMaxIndex(currentArray);
			if(label == current) {
				trueScores.add(getMaxValue(currentArray));
			}else {
				falseScores.add(getMaxValue(currentArray));
			}
		}
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	private float getMaxValue(float[] array) {
		float max = -100.0f;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		// do nothing.
	}

	@Override
	public String stats() {
		Collections.sort(trueScores);
		Collections.sort(falseScores);
		double tsSize = trueScores.size();
		double fsSize = falseScores.size();
		String result = String.format("CombinedActivation: %s ", NAME);
		if(tsSize > 0) {
			result += String.format("TRUE(%1.3f - %1.3f - %1.3f - %1.3f - %1.3f) ",
					trueScores.get((int) (0.10 * tsSize)),
					trueScores.get((int) (0.30 * tsSize)),
					trueScores.get((int) (0.50 * tsSize)),
					trueScores.get((int) (0.70 * tsSize)),
					trueScores.get((int) (0.90 * tsSize)));
		}else {
			result += ("TRUE(0,5 - 0,5 - 0,5 - 0,5 - 0,5) ");
		}
		if(fsSize > 0) {
			result += String.format("FALSE(%1.3f - %1.3f - %1.3f - %1.3f - %1.3f) ",
					falseScores.get((int) (0.10 * fsSize)),
					falseScores.get((int) (0.30 * fsSize)),
					falseScores.get((int) (0.50 * fsSize)),
					falseScores.get((int) (0.70 * fsSize)),
					falseScores.get((int) (0.90 * fsSize)));
		}else {
			result += ("FALSE(0,5 - 0,5 - 0,5 - 0,5 - 0,5) ");
		}
		return result;
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new D36CombinedActivationEvaluator(C, NAME);
	}
}
