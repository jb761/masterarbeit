package evaluation.d36;

import org.nd4j.evaluation.BaseEvaluation;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.evaluation.IMetric;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class D36Top1AccuracyEvaluator extends BaseEvaluation {

	private static final Logger LOGGER = LoggerFactory.getLogger(D36Top1AccuracyEvaluator.class);

	private final String NAME;

	private float hit = 0;
	private float count = 0;

	public D36Top1AccuracyEvaluator(String name) {
		this.NAME = name;
	}

	@Override
	public void eval(INDArray labels, INDArray networkPredictions, INDArray maskArray, List recordMetaData) {
		int rows = labels.rows();
		for(int r=0; r<rows; r++) {
			float[] labelRow = labels.getRow(r).toFloatVector();
			float[] currentRow = networkPredictions.getRow(r).toFloatVector();
			if(getMaxIndex(labelRow) == getMaxIndex(currentRow)) {
				hit++;
			}
		}
		count += rows;
	}

	private int getMaxIndex(float[] array) {
		float max = -100.0f;
		int maxPos = -1;
		for(int i=0; i<array.length; i++) {
			if(array[i] > max) {
				max = array[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	@Override
	public void merge(IEvaluation other) {
		LOGGER.warn("Unsupported call: merge");
		// do nothing.
	}

	@Override
	public void reset() {
		hit = 0;
		count = 0;
	}

	@Override
	public String stats() {
		return String.format("Top-1-Accuracy: %s: %3.2f%%", NAME, (100.0*hit/count));
	}

	@Override
	public double getValue(IMetric metric) {
		LOGGER.warn("Unsupported call: getValue");
		return 0;
	}

	@Override
	public IEvaluation newInstance() {
		return new D36Top1AccuracyEvaluator(NAME);
	}
}
