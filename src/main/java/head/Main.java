package head;

import datasetIterator.*;
import evaluation.*;
import netType.D33;
import netType.D36;
import netType.D76;
import netType.PlantNetType;
import network.Network;
import network.impl.*;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.optimize.api.InvocationType;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trainingListeners.BundledEvaluationsListener;
import trainingListeners.PlantNetDatasetRotation;
import util.DAD76;
import util.Stopwatch;

import java.io.IOException;

public class Main {

	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

	public static final long SEED = 9875732;

	public static void main(String[] args) throws Exception {

		Nd4j.getMemoryManager().setAutoGcWindow(500);
		DAD76.init();

//		new DataSet().detach();
//		LOGGER.info("- >");

//		PlantNetUtil.crawlD12Log();
//		PlantNetUtil.createD12ClassRAW();

//		PlantNetUtil.removeImagesFromDirRec();
//		PlantNetUtil.createFlattenTaxoTree();
//		PlantNetUtil.normalizeD2X();
//		PlantNetUtil.analyseD2X();
//		PlantNetUtil.createAppendixFromNormalized();
//		PlantNetUtil.createD2XHistogram();
//		PlantNetUtil.createD2XClassRAWs();
//		PlantNetUtil.createSunburst();
//		PlantNetUtil.crawlD26Log();

//		doMNIST();
//		doAlexNetD11();
//		doAlexNetD12();
//		doAlexNetD26();
//		doAlexNetD22();
//		doAlexNetD23();
//		doAlexNetD24();
//		doAlexNetD25();

//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD22 net = Network.load(AlexNetD22.class, "E:\\playground\\AlexNetD23.cnn");
//		net.evaluate(PlantNetType.D23, DataSetType.TEST,
//				new IEvaluation[]{
//						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 0, 113),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 113, 337),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 337, 1356)
//				});
////		net.evaluate(PlantNetType.D23, DataSetType.TRAIN,
////				new IEvaluation[]{
////						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 0, 224),
////						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 224, 1243)
////				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.exit(1);

//		PlantNetUtil.crawlMALogs();
//		PlantNetUtil.addOne();

//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD26 net = Network.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
//		net.evaluate(PlantNetType.D26, DataSetType.TEST,
//				new IEvaluation[]{
//						new ConditionalFeedForward()
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.exit(1);
//
//		PlantNetUtil.createD26CondFFHelper();
//
//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD26 net = Network.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
//		net.evaluate(PlantNetType.D26, DataSetType.TEST,
//				new IEvaluation[]{
//						new ConditionalSelectiveFeedForward()
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.exit(1);

//		doBNet6();
//		PlantNetUtil.crawlD36Log();
//		doBNet6_featureE1();

//		Stopwatch sw = new Stopwatch().start();
//		BNet6 net = BNet6.load("E:\\playground\\BNet6_BT.cnn");
//		net.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.exit(1);

//		doBNet6BT(2, 1);

//		doBNet6Twisted();

//		eval();

//		PlantNetUtil.crawlD36Log();
//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD26 netD26 = AlexNetD26.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
//		netD26.evaluate(PlantNetType.D26, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedActivationEvaluator(16, "EBENE 6", 0, 16),
//						new CombinedActivationEvaluator(29, "EBENE 5", 16, 45),
//						new CombinedActivationEvaluator(59, "EBENE 4", 45, 104),
//						new CombinedActivationEvaluator(113, "EBENE 3", 104, 217),
//						new CombinedActivationEvaluator(224, "EBENE 2", 217, 441),
//						new CombinedActivationEvaluator(1019, "EBENE 1", 441, 1460)
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.exit(1);

//		Stopwatch sw = new Stopwatch().start();
//		BNet6 bNet6_BT = BNet6.load("E:\\playground\\BNet6_BT.cnn");
//		bNet6_BT.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36ConditionalSelectiveFeedForward(0, 3),
//						new D36ConditionalSelectiveFeedForward(1, 3),
//						new D36ConditionalSelectiveFeedForward(2, 3),
//						new D36ConditionalSelectiveFeedForward(3, 3),
//						new D36ConditionalSelectiveFeedForward(4, 3),
//						new D36ConditionalSelectiveFeedForward(5, 3),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_BT \n");
//		doBCNN_BaseC_32(1,1);
//		doBCNN_BaseC_224(1,1);
//		PlantNetUtil.crawlD36Log_3();

//		Stopwatch sw = new Stopwatch().start();
//		BCNN_BaseC_32 net = BCNN_BaseC_32.load("E:\\playground\\BCNN_BaseC_32_BT_5_5_5.cnn");
//		net.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D33Top1AccuracyEvaluator("Ebene 3"),
//						new D33Top1AccuracyEvaluator("Ebene 2"),
//						new D33Top1AccuracyEvaluator("Ebene 1"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D33CombinedActivationEvaluator(113, "Ebene 3"),
//						new D33CombinedActivationEvaluator(224, "Ebene 2"),
//						new D33CombinedActivationEvaluator(1019, "Ebene 1"),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BCNN_BaseC_32_BT_5_5_5 \n");
//
//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD25 net = AlexNetD25.load(AlexNetD25.class, "E:\\playground\\AlexNetD25.cnn");
//		net.evaluate(PlantNetType.D25, DataSetType.TEST,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 5", 0, 29),
//						new CombinedTop1AccuracyEvaluator("EBENE 4", 29, 88),
//						new CombinedTop1AccuracyEvaluator("EBENE 3", 88, 201),
//						new CombinedTop1AccuracyEvaluator("EBENE 2", 201, 425),
//						new CombinedTop1AccuracyEvaluator("EBENE 1", 425, 1444),
//						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 0, 29),
//						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 29, 88),
//						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88, 201),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201, 425),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425, 1444)
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD25 \n");
//		System.exit(1);

//		Stopwatch sw = new Stopwatch().start();
//		BranchedResNet50 net = BranchedResNet50.load("E:\\playground\\BranchedResNet50_BT.cnn");
//		net.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 6"),
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 5"),
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 4"),
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 3"),
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 2"),
//						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E6size, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E5size, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E4size, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E3size, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E2size, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E1size, "Ebene 1"),
//						new D36CombinedActivationEvaluator(D36.E6size, "Ebene 6"),
//						new D36CombinedActivationEvaluator(D36.E5size, "Ebene 5"),
//						new D36CombinedActivationEvaluator(D36.E4size, "Ebene 4"),
//						new D36CombinedActivationEvaluator(D36.E3size, "Ebene 3"),
//						new D36CombinedActivationEvaluator(D36.E2size, "Ebene 2"),
//						new D36CombinedActivationEvaluator(D36.E1size, "Ebene 1"),
////						new D33ConditionalSelectiveFeedForward(0, 3),
////						new D33ConditionalSelectiveFeedForward(1, 3),
////						new D33ConditionalSelectiveFeedForward(2, 3),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BranchedResNet50_BT \n");

//		doBCNN_BaseC_224(1, 1);
//		PlantNetUtil.crawlD36Log_3();
//		doBranchedResNet50(1,1);
//		doBranchedResNet50(1,1);
//		doBranchedResNet50(2,2);
//		doBranchedResNet50(2,1);
//		doBranchedResNet50(1,1);
//		PlantNetUtil.crawlD36Log();

//		doEFNet(6,6); // 1
//		doEFNet(6,6); // 2
//		doEFNet(6,5); // 3
//		doEFNet(5,5); // 4
//		doEFNet(5,4); // 5
//		doEFNet(4,4); // 6
//		doEFNet(4,3); // 7
//		doEFNet(3,3); // 8
//		doEFNet(3,2); // 9
//		doEFNet(2,2); // 10
//		doEFNet(2,1); // 11
//		doEFNet(1,1); // 12

//		PlantNetUtil.crawlD36Log();

//		LFNet.eval1("LFNet_All");
//		LFNet.eval3("LFNet_All");
//		LFNet.eval1("LFNet_BT");
//		LFNet.eval3("LFNet_BT");
//		LFNet.eval1("LFNet_All_Add");

//		LFNet.eval3("LFNet_All_Add");
//		LFNet.eval1("LFNet_BT_Add");
//		LFNet.eval3("LFNet_BT_Add");
//		EFNet.eval1("EFNet_All_concat");
//		EFNet.eval3("EFNet_All_concat");
//		EFNet.eval1("EFNet_BT_concat");
//		EFNet.eval3("EFNet_BT_concat");
//		EFNet.eval1("EFNet_All_add");
//		EFNet.eval3("EFNet_All_add");
//		EFNet.eval1("EFNet_BT_add");
//		EFNet.eval3("EFNet_BT_add");
//
//		Stopwatch sw = new Stopwatch().start();
//		AlexNetD26 net = AlexNetD26.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
//		net.evaluate2(DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 6",	0, 16),
//						new CombinedTop1AccuracyEvaluator("EBENE 5", 16, 45),
//						new CombinedTop1AccuracyEvaluator("EBENE 4", 45, 104),
//						new CombinedTop1AccuracyEvaluator("EBENE 3", 104, 217),
//						new CombinedTop1AccuracyEvaluator("EBENE 2", 217, 441),
//						new CombinedTop1AccuracyEvaluator("EBENE 1", 441, 1460),
//						new CombinedMacroAVGTop1AccuracyEvaluator(16, "EBENE 6", 0, 16),
//						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 16, 45),
//						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 45, 104),
//						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 104, 217),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 217, 441),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 441, 1460),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD26 \n");
//
//		Stopwatch sw = new Stopwatch().start();
//		BNet6 bNet6_singleE1 = BNet6.load("E:\\playground\\BNet6_singleE1.cnn");
//		bNet6_singleE1.evaluate(DataSetType.TRAIN,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D36CombinedActivationEvaluator(16, "Ebene 6"),
//						new D36CombinedActivationEvaluator(29, "Ebene 5"),
//						new D36CombinedActivationEvaluator(59, "Ebene 4"),
//						new D36CombinedActivationEvaluator(113, "Ebene 3"),
//						new D36CombinedActivationEvaluator(224, "Ebene 2"),
//						new D36CombinedActivationEvaluator(1019, "Ebene 1")
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_singleE1 \n");

//		doLFNetOpposite(6,6); // 1
//		doLFNetOpposite(6,6); // 2
//		doLFNetOpposite(6,5); // 3
//		doLFNetOpposite(5,5); // 4
//		doLFNetOpposite(5,4); // 5
//		doLFNetOpposite(4,4); // 6
//		doLFNetOpposite(4,3); // 7
//		doLFNetOpposite(3,3); // 8
//		doLFNetOpposite(3,2); // 9
//		doLFNetOpposite(2,2); // 10
//		doLFNetOpposite(2,1); // 11
//		doLFNetOpposite(1,1); // 12

//		LFNet_Opposite.eval1("LFNet_BT_Add");
//		LFNet_Opposite.eval3("LFNet_BT_Add_Opposite");

//		LFNet.eval2("LFNet_BT_Add");
//		PlantNetUtil.crawlD36Log();

//		LFNet net = LFNet.load( "E:\\playground\\LFNet_BT_Add.cnn");
//		net.test();

//		LFNet.testImport();

//		createOptLFNet("OptLFNet_SpareParts");
//		DAD76.init();
//		createOptLFNet("OptLFNet");
//		doOptLFNet("OptLFNet", 6);
//		doOptLFNet("OptLFNet", 6);
//		doOptLFNet("OptLFNet", 5);
//		doOptLFNet("OptLFNet", 5);
//		doOptLFNet("OptLFNet", 4);
//		doOptLFNet("OptLFNet", 4);
//		doOptLFNet("OptLFNet", 3);
//		doOptLFNet("OptLFNet", 3);
//		doOptLFNet("OptLFNet", 2);
//		doOptLFNet("OptLFNet", 2);
//		doOptLFNet("OptLFNet", 1);
//		doOptLFNet("OptLFNet", 1);
//		System.out.println("\n\n\n\n\n\n\n");
//		OptLFNet.eval1("OptLFNet");
//		OptLFNet.eval3("OptLFNet");

//		DAD76.init();
//		doOptAlexNet("OptAlexNet_CL_TFL_DA2", 1);


//		DAD76.init();
//		createOptAlexNet("OptAlexNet_XXX");
//		doOptAlexNet("OptAlexNet_XXX", 6);
//		doOptAlexNet("OptAlexNet_XXX", 6);
//		doOptAlexNet("OptAlexNet_XXX", 5);
//		doOptAlexNet("OptAlexNet_XXX", 5);
//		doOptAlexNet("OptAlexNet_XXX", 4);
//		doOptAlexNet("OptAlexNet_XXX", 4);
//		doOptAlexNet("OptAlexNet_XXX", 3);
//		doOptAlexNet("OptAlexNet_XXX", 3);
//		doOptAlexNet("OptAlexNet_XXX", 2);
//		doOptAlexNet("OptAlexNet_XXX", 2);
//		doOptAlexNet("OptAlexNet_XXX", 1);
//		doOptAlexNet("OptAlexNet_XXX", 1);
//		System.out.println("\n\n\n\n\n\n\n");
//		OptAlexNet.eval1("OptAlexNet_XXX");
//		OptAlexNet.eval3("OptAlexNet_XXX");

//		PlantNetUtil.crawlD36Log();
//		OptLFNet.eval2("OptLFNet");

//		Stopwatch sw = new Stopwatch().start();
//		BCNN_BaseC_32 net = BCNN_BaseC_32.load( "E:\\playground\\BCNN_BaseC_32_Single_E1.cnn");
//		net.evaluate(DataSetType.TRAIN,
//				new IEvaluation[]{
////						new CombinedD36ConditionalSelectiveFeedForward(0, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(1, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(2, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(3, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(4, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(5, 0),
////						new CombinedD36ConditionalSelectiveFeedForward(0, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(1, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(2, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(3, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(4, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(5, 1),
////						new CombinedD36ConditionalSelectiveFeedForward(0, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(1, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(2, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(3, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(4, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(5, 2),
////						new CombinedD36ConditionalSelectiveFeedForward(0, 3),
////						new CombinedD36ConditionalSelectiveFeedForward(1, 3),
////						new CombinedD36ConditionalSelectiveFeedForward(2, 3),
////						new CombinedD36ConditionalSelectiveFeedForward(3, 3),
////						new CombinedD36ConditionalSelectiveFeedForward(4, 3),
////						new CombinedD36ConditionalSelectiveFeedForward(5, 3),
////
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 6"),
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 5"),
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 4"),
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 3"),
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 2"),
////						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 1"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E6size, "Ebene 6"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E5size, "Ebene 5"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E4size, "Ebene 4"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E3size, "Ebene 3"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E2size, "Ebene 2"),
////						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E1size, "Ebene 1"),
//
//						new D33Top1AccuracyEvaluator("EBENE 3"),
//						new D33Top1AccuracyEvaluator("EBENE 2"),
//						new D33Top1AccuracyEvaluator("EBENE 1"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(D76.E3size, "EBENE 3"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(D76.E2size, "EBENE 2"),
//						new D33CombinedMacroAVGTop1AccuracyEvaluator(D76.E1size, "EBENE 1"),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BCNN_BaseC_32_BT_5_5_5 \n");

//		OptAlexNet.eval4("OptAlexNet_CL_TFL_DA2");
//
//		Stopwatch sw = new Stopwatch().start();
//		BNet6 net = BNet6.load("E:\\playground\\BNet6_Twisted.cnn");
//		net.evaluate(DataSetType.TRAIN,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 6"),
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 5"),
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 4"),
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 3"),
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 2"),
//						new D36Top1AccuracyEvaluatorWithoutReset("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_Twisted \n");

		eval();

		System.exit(1);
	}

	private static void createOptLFNet(String name) throws IOException {
		OptLFNet net = OptLFNet.load( "E:\\playground\\" + name + ".cnn");
		OptLFNet.safe(net, "E:\\playground\\" + name + ".cnn");
	}

	private static void doOptLFNet(String name, int focus) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		DAD76_PlantNetImageProvider imageProvider = new DAD76_PlantNetImageProvider(1,1,0);
		OptLFNet net = OptLFNet.load( "E:\\playground\\" + name + ".cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(imageProvider);
			LOGGER.info("Attached Training Listeners");
		}
		net.featureBranch(focus);
		LOGGER.info(String.format("INIT DONE (%1.3f Sec.)", sw.get(1000)));
		net.train(imageProvider, D76.TRAINING_ROTATION_COUNT);
//		net.train(imageProvider, 1);
		OptLFNet.safe(net, "E:\\playground\\" + name + ".cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + focus);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void createOptAlexNet(String name) throws IOException {
		OptAlexNet net = OptAlexNet.load( "E:\\playground\\" + name + ".cnn");
		OptAlexNet.safe(net, "E:\\playground\\" + name + ".cnn");
	}

	private static void doOptAlexNet(String name, int focus) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		DAD76_PlantNetImageProvider imageProvider = new DAD76_PlantNetImageProvider(1,1,0);
		OptAlexNet net = OptAlexNet.load( "E:\\playground\\" + name + ".cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(imageProvider);
			LOGGER.info("Attached Training Listeners");
		}
		net.featureBranch(focus);
		LOGGER.info(String.format("INIT DONE (%1.3f Sec.)", sw.get(1000)));
		net.train(imageProvider, D76.TRAINING_ROTATION_COUNT);
//		net.train(imageProvider, 1);
		OptAlexNet.safe(net, "E:\\playground\\" + name + ".cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + focus);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doLFNetOpposite(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36_Opposite d36ImageProvider = new PlantNetImageProviderD36_Opposite();
		LFNet_Opposite net = LFNet_Opposite.load( "E:\\playground\\LFNet_BT_Add_Opposite.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.featureNextBranch(current, next);
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
//		net.train(d36ImageProvider, 1);
		LFNet_Opposite.safe(net, "E:\\playground\\LFNet_BT_Add_Opposite.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doLFNet(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		LFNet net = LFNet.load( "E:\\playground\\LFNet_BT_Add.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.featureNextBranch(current, next);
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		LFNet.safe(net, "E:\\playground\\LFNet_BT_Add.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doEFNet(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		EFNet net = EFNet.load( "E:\\playground\\EFNet_BT_add.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.featureNextBranch(current, next);
//		net.train(d36ImageProvider, 1);
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		EFNet.safe(net, "E:\\playground\\EFNet_BT_add.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doBranchedResNet50(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		BranchedResNet50 net = BranchedResNet50.load( "E:\\playground\\BranchedResNet50_BT.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.featureNextBranch(current, next);
//		net.train(d36ImageProvider, 1);
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		BranchedResNet50.safe(net, "E:\\playground\\BranchedResNet50_BT.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doBCNN_BaseC_224(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD33 d33ImageProvider = new PlantNetImageProviderD33();
		BCNN_BaseC_224 net = BCNN_BaseC_224.load( "E:\\playground\\BCNN_BaseC_224_Single_E1.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d33ImageProvider);
		}
		net.featureNextBranch(current, next);
		net.train(d33ImageProvider, D33.TRAINING_ROTATION_COUNT);
		BCNN_BaseC_224.safe(net, "E:\\playground\\BCNN_BaseC_224_Single_E1.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
	}

	private static void doBCNN_BaseC_32(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD33 d33ImageProvider = new PlantNetImageProviderD33();
		BCNN_BaseC_32 net = BCNN_BaseC_32.load( "E:\\playground\\BCNN_BaseC_32_BT_5_5_5.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d33ImageProvider);
		}
		net.featureNextBranch(current, next);
		net.train(d33ImageProvider, D33.TRAINING_ROTATION_COUNT);
		BCNN_BaseC_32.safe(net, "E:\\playground\\BCNN_BaseC_32_BT_5_5_5.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void eval() {
		Stopwatch sw = new Stopwatch().start();
//		AlexNetD11 netD11 = AlexNetD11.load(AlexNetD11.class, "E:\\playground\\AlexNetD11.cnn");
//		netD11.evaluate(PlantNetType.D11, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 1",	0, 1019),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 0, 1019),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD11 \n");
//
//		AlexNetD12 netD12 = AlexNetD12.load(AlexNetD12.class, "E:\\playground\\AlexNetD12.cnn");
//		netD12.evaluate(PlantNetType.D12, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 1",	0, 1019),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 0, 1019),
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD12 \n");
//
//		AlexNetD22 netD22 = AlexNetD22.load(AlexNetD22.class, "E:\\playground\\AlexNetD22.cnn");
//		netD22.evaluate(PlantNetType.D22, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 2", 0, 425 - 201),
//						new CombinedTop1AccuracyEvaluator("EBENE 1", 425 - 201, 1444 - 201),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201 - 201, 425 - 201),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425 - 201, 1444 - 201)
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD22 \n");
//
//		AlexNetD23 netD23 = AlexNetD23.load(AlexNetD23.class, "E:\\playground\\AlexNetD23.cnn");
//		netD23.evaluate(PlantNetType.D23, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 3", 0, 201 - 88),
//						new CombinedTop1AccuracyEvaluator("EBENE 2", 201 - 88, 425 - 88),
//						new CombinedTop1AccuracyEvaluator("EBENE 1", 425 - 88, 1444 - 88),
//						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88 - 88, 201 - 88),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201 - 88, 425 - 88),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425 - 88, 1444 - 88)
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD23 \n");
//
//		AlexNetD24 netD24 = AlexNetD25.load(AlexNetD24.class, "E:\\playground\\AlexNetD24.cnn");
//		netD24.evaluate(PlantNetType.D24, DataSetType.TRAIN,
//				new IEvaluation[]{
//						new CombinedTop1AccuracyEvaluator("EBENE 4", 0, 88 - 29),
//						new CombinedTop1AccuracyEvaluator("EBENE 3", 88 - 29, 201 - 29),
//						new CombinedTop1AccuracyEvaluator("EBENE 2", 201 - 29, 425 - 29),
//						new CombinedTop1AccuracyEvaluator("EBENE 1", 425 - 29, 1444 - 29),
//						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 29 - 29, 88 - 29),
//						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88 - 29, 201 - 29),
//						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201 - 29, 425 - 29),
//						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425 - 29, 1444 - 29)
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> AlexNetD24 \n");

		AlexNetD25 netD25 = AlexNetD25.load(AlexNetD25.class, "E:\\playground\\AlexNetD25.cnn");
		netD25.evaluate(PlantNetType.D25, DataSetType.TRAIN,
				new IEvaluation[]{
						new CombinedTop1AccuracyEvaluator("EBENE 5", 0, 29),
						new CombinedTop1AccuracyEvaluator("EBENE 4", 29, 88),
						new CombinedTop1AccuracyEvaluator("EBENE 3", 88, 201),
						new CombinedTop1AccuracyEvaluator("EBENE 2", 201, 425),
						new CombinedTop1AccuracyEvaluator("EBENE 1", 425, 1444),
						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 0, 29),
						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 29, 88),
						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88, 201),
						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201, 425),
						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425, 1444)
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.out.println("--> AlexNetD25 \n");

		AlexNetD26 netD26 = AlexNetD26.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
		netD26.evaluate(PlantNetType.D26, DataSetType.TRAIN,
				new IEvaluation[]{
						new CombinedTop1AccuracyEvaluator("EBENE 6",	0, 16),
						new CombinedTop1AccuracyEvaluator("EBENE 5", 16, 45),
						new CombinedTop1AccuracyEvaluator("EBENE 4", 45, 104),
						new CombinedTop1AccuracyEvaluator("EBENE 3", 104, 217),
						new CombinedTop1AccuracyEvaluator("EBENE 2", 217, 441),
						new CombinedTop1AccuracyEvaluator("EBENE 1", 441, 1460),
						new CombinedMacroAVGTop1AccuracyEvaluator(16, "EBENE 6", 0, 16),
						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 16, 45),
						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 45, 104),
						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 104, 217),
						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 217, 441),
						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 441, 1460),
						new CombinedActivationEvaluator(16, "EBENE 6", 0, 16),
						new CombinedActivationEvaluator(29, "EBENE 5", 16, 45),
						new CombinedActivationEvaluator(59, "EBENE 4", 45, 104),
						new CombinedActivationEvaluator(113, "EBENE 3", 104, 217),
						new CombinedActivationEvaluator(224, "EBENE 2", 217, 441),
						new CombinedActivationEvaluator(1019, "EBENE 1", 441, 1460)
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.out.println("--> AlexNetD26 \n");

//		BNet6 bNet6_singleE1 = BNet6.load("E:\\playground\\BNet6_singleE1.cnn");
//		bNet6_singleE1.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D36CombinedActivationEvaluator(16, "Ebene 6"),
//						new D36CombinedActivationEvaluator(29, "Ebene 5"),
//						new D36CombinedActivationEvaluator(59, "Ebene 4"),
//						new D36CombinedActivationEvaluator(113, "Ebene 3"),
//						new D36CombinedActivationEvaluator(224, "Ebene 2"),
//						new D36CombinedActivationEvaluator(1019, "Ebene 1")
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_singleE1 \n");
//
//
//		BNet6 bNet6 = BNet6.load("E:\\playground\\BNet6.cnn");
//		bNet6.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D36CombinedActivationEvaluator(16, "Ebene 6"),
//						new D36CombinedActivationEvaluator(29, "Ebene 5"),
//						new D36CombinedActivationEvaluator(59, "Ebene 4"),
//						new D36CombinedActivationEvaluator(113, "Ebene 3"),
//						new D36CombinedActivationEvaluator(224, "Ebene 2"),
//						new D36CombinedActivationEvaluator(1019, "Ebene 1")
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6 \n");
//
//
//		BNet6 bNet6_BT = BNet6.load("E:\\playground\\BNet6_BT.cnn");
//		bNet6_BT.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D36CombinedActivationEvaluator(16, "Ebene 6"),
//						new D36CombinedActivationEvaluator(29, "Ebene 5"),
//						new D36CombinedActivationEvaluator(59, "Ebene 4"),
//						new D36CombinedActivationEvaluator(113, "Ebene 3"),
//						new D36CombinedActivationEvaluator(224, "Ebene 2"),
//						new D36CombinedActivationEvaluator(1019, "Ebene 1")
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_BT \n");
//
//		BNet6 bNet6_Twisted = BNet6.load("E:\\playground\\BNet6_Twisted.cnn");
//		bNet6_Twisted.evaluate(DataSetType.TEST,
//				new IEvaluation[]{
//						new D36Top1AccuracyEvaluator("EBENE 6"),
//						new D36Top1AccuracyEvaluator("EBENE 5"),
//						new D36Top1AccuracyEvaluator("EBENE 4"),
//						new D36Top1AccuracyEvaluator("EBENE 3"),
//						new D36Top1AccuracyEvaluator("EBENE 2"),
//						new D36Top1AccuracyEvaluator("EBENE 1"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(16, "Ebene 6"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(29, "Ebene 5"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(59, "Ebene 4"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(113, "Ebene 3"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(224, "Ebene 2"),
//						new D36CombinedMacroAVGTop1AccuracyEvaluator(1019, "Ebene 1"),
//						new D36CombinedActivationEvaluator(16, "Ebene 6"),
//						new D36CombinedActivationEvaluator(29, "Ebene 5"),
//						new D36CombinedActivationEvaluator(59, "Ebene 4"),
//						new D36CombinedActivationEvaluator(113, "Ebene 3"),
//						new D36CombinedActivationEvaluator(224, "Ebene 2"),
//						new D36CombinedActivationEvaluator(1019, "Ebene 1")
//				});
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
//		LOGGER.info("--------------------------------------------------------------------------------------------------");
//		System.out.println("--> BNet6_Twisted \n");

	}

	private static void doBNet6Twisted() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		BNet6Twisted net = BNet6Twisted.load( "E:\\playground\\BNet6_Twisted.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		BNet6Twisted.safe(net, "E:\\playground\\BNet6_Twisted.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doBNet6BT(int current, int next) {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		BNet6 net = BNet6.load( "E:\\playground\\BNet6_BT.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.featureNextBranch(current, next);
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		BNet6.safe(net, "E:\\playground\\BNet6_BT.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("FEATURED BRANCH: " + next);
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doBNet6() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		BNet6 net = BNet6.load( "E:\\playground\\BNet6.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
//		net.featureNextBranch(5, 4);
		BNet6.safe(net, "E:\\playground\\BNet6.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doBNet6_featureE1() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetImageProviderD36 d36ImageProvider = new PlantNetImageProviderD36();
		BNet6 net = BNet6.load( "E:\\playground\\BNet6_singleE1.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d36ImageProvider);
		}
//		net.featureNextBranch();
		net.train(d36ImageProvider, D36.TRAINING_ROTATION_COUNT);
		BNet6.safe(net, "E:\\playground\\BNet6_singleE1.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD25() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d25 = PlantNetType.D25;
		PlantNetImageProvider d25ImageProvider = new PlantNetImageProvider(d25);
		AlexNetD25 net = Network.load(AlexNetD25.class, "E:\\playground\\AlexNetD25.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d25ImageProvider);
		}
		net.train(d25ImageProvider, d25.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD25.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD24() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d24 = PlantNetType.D24;
		PlantNetImageProvider d24ImageProvider = new PlantNetImageProvider(d24);
		AlexNetD24 net = Network.load(AlexNetD24.class, "E:\\playground\\AlexNetD24.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d24ImageProvider);
		}
		net.train(d24ImageProvider, d24.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD24.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD23() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d23 = PlantNetType.D23;
		PlantNetImageProvider d23ImageProvider = new PlantNetImageProvider(d23);
		AlexNetD23 net = Network.load(AlexNetD23.class, "E:\\playground\\AlexNetD23.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d23ImageProvider);
		}
		net.train(d23ImageProvider, d23.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD23.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD22() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d22 = PlantNetType.D22;
		PlantNetImageProvider d22ImageProvider = new PlantNetImageProvider(d22);
		AlexNetD22 net = Network.load(AlexNetD22.class, "E:\\playground\\AlexNetD22.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d22ImageProvider);
		}
		net.train(d22ImageProvider, d22.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD22.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD26() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d26 = PlantNetType.D26;
		PlantNetImageProvider d26ImageProvider = new PlantNetImageProvider(d26);
		AlexNetD26 net = Network.load(AlexNetD26.class, "E:\\playground\\AlexNetD26.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachInitialTrainingListeners(d26ImageProvider);
		}
		net.train(d26ImageProvider, d26.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD26.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD12() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d12 = PlantNetType.D12;
		PlantNetImageProvider d11ImageProvider = new PlantNetImageProvider(d12);
		AlexNetD12 net = Network.load(AlexNetD12.class, "E:\\playground\\AlexNetD12.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachTrainingListener(new PlantNetDatasetRotation(
					d11ImageProvider,
					5,
					15,
					new IEvaluation[]{
							new TopKAccuracyEvaluator(1),
							new TopKAccuracyEvaluator(5),
							new MacroAVGTop1AccuracyEvaluator(d12.CLASS_SIZE)
					},
					new IEvaluation[]{
							new TopKAccuracyEvaluator(1),
							new TopKAccuracyEvaluator(5),
							new MacroAVGTop1AccuracyEvaluator(d12.CLASS_SIZE)
					}));
		}
		net.train(d11ImageProvider, d12.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD12.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doAlexNetD11() {
		Stopwatch sw = new Stopwatch().start();
		PlantNetType d11 = PlantNetType.D11;
		PlantNetImageProvider d11ImageProvider = new PlantNetImageProvider(d11);
		AlexNetD11 net = Network.load(AlexNetD11.class, "E:\\playground\\AlexNetD11.cnn");
		if(net.getAttachedListener().isEmpty()) {
			net.attachTrainingListener(new PlantNetDatasetRotation(
					d11ImageProvider,
					5,
					15,
					new IEvaluation[]{
							new TopKAccuracyEvaluator(1),
							new TopKAccuracyEvaluator(5),
							new MacroAVGTop1AccuracyEvaluator(d11.CLASS_SIZE)
					},
					new IEvaluation[]{
							new TopKAccuracyEvaluator(1),
							new TopKAccuracyEvaluator(5),
							new MacroAVGTop1AccuracyEvaluator(d11.CLASS_SIZE)
					}));
		}
		net.train(d11ImageProvider, d11.TRAINING_ROTATION_COUNT);
		Network.safe(net, "E:\\playground\\AlexNetD11.cnn");
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		System.exit(1);
	}

	private static void doMNIST() throws IOException {
		MNISTImageProvider mnistTest = new MNISTImageProvider(DataSetType.TEST, 1000, 10000, SEED);
		MNISTImageProvider mnistTrain = new MNISTImageProvider(DataSetType.TRAIN, 1000, 60000, SEED);

		MNISTTestNet net = Network.load(MNISTTestNet.class, String.format("E:\\playground\\%s.cnn", MNISTTestNet.class.getSimpleName()));
		if(net.getAttachedListener().isEmpty()) {
			net.attachTrainingListener(new BundledEvaluationsListener(InvocationType.EPOCH_END, DataSetType.TEST, mnistTest, 100,
					new TopKAccuracyEvaluator(1),
					new TopKAccuracyEvaluator(5),
					new MacroAVGTop1AccuracyEvaluator(10)));
			net.attachTrainingListener(new BundledEvaluationsListener(InvocationType.ITERATION_END, DataSetType.TRAIN, mnistTrain, 100,
					new TopKAccuracyEvaluator(1),
					new TopKAccuracyEvaluator(5),
					new MacroAVGTop1AccuracyEvaluator(10)));
		}

		net.train(mnistTrain, 3);

		Network.safe(net, String.format("E:\\playground\\%s.cnn", net.getClass().getSimpleName()));
	}
}
