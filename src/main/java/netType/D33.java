package netType;

public class D33 {

	public static final int E3size = 113;
	public static final int E2size = 224;
	public static final int E1size = 1019;

	public static final int[] E3 = new int[] {0, 113};
	public static final int[] E2 = new int[] {113, 337};
	public static final int[] E1 = new int[] {337, 1356};

	public static final String RAW_DATA_PATH_TEST = "E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224";
	public static final String RAW_DATA_PATH_TRAINING = "E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224";
	public static final String RAW_CLASSES_PATH_TEST = "E:\\PlantNet_Dataset\\RAW_Classes_Test\\D26";
	public static final String RAW_CLASSES_PATH_TRAINING = "E:\\PlantNet_Dataset\\RAW_Classes_Training\\D26";

	public static final int TEST_BATCH_SIZE = 70;
	public static final int TEST_ROTATION_SIZE = 490;
	public static final int TEST_ROTATION_COUNT = 127;

	public static final int TRAINING_BATCH_SIZE = 34;
	public static final int TRAINING_ROTATION_SIZE = 578;
	public static final int TRAINING_ROTATION_COUNT = 422;
}
