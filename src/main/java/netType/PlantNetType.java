package netType;

public enum PlantNetType {
	D11("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D11",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D11",
			224, 224,
			70, 490, 127, // 7 Batches a 70 img, 127 Partitionen, 62230 img total
			34,578, 422, // 17 Batches a 34 img, 442 Partitionen, 243.916 img total
			1019, null),
	D12("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D12",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D12",
			224, 224,
			70, 490, 127, // 7 Batches a 70 img, 127 Partitionen, 62230 img total
			34,578, 422, // 17 Batches a 34 img, 442 Partitionen, 243.916 img total
			303, null),

	D26("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D26",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D26",
			224, 224, 70, 490, 127, 34, 578, 422,
			1460, new int[]{16, 29, 59, 113, 224}),
	D25("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D25",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D25",
			224, 224, 70, 490, 127, 34, 578, 422,
			1444, new int[]{29, 59, 113, 224}),
	D24("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D24",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D24",
			224, 224, 70, 490, 127, 34, 578, 422,
			1415, new int[]{59, 113, 224}),
	D23("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D23",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D23",
			224, 224, 70, 490, 127, 34, 578, 422,
			1356, new int[]{113, 224}),
	D22("E:\\PlantNet_Dataset\\RAW_Images_Test\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Images_Training\\224x224",
			"E:\\PlantNet_Dataset\\RAW_Classes_Test\\D22",
			"E:\\PlantNet_Dataset\\RAW_Classes_Training\\D22",
			224, 224, 70, 490, 127, 34, 578, 422,
			1243, new int[]{224});

	public final String RAW_DATA_PATH_TEST;
	public final String RAW_DATA_PATH_TRAINING;
	public final String RAW_CLASSES_PATH_TEST;
	public final String RAW_CLASSES_PATH_TRAINING;
	public final int X_SIZE;
	public final int Y_SIZE;
	public final int TEST_BATCH_SIZE;
	public final int TEST_ROTATION_SIZE;
	public final int TEST_ROTATION_COUNT;
	public final int TRAINING_BATCH_SIZE;
	public final int TRAINING_ROTATION_SIZE;
	public final int TRAINING_ROTATION_COUNT;
	public final int CLASS_SIZE;
	public final int[] COMB_CLASS_SEP;

	PlantNetType(String rawTest, String rawTraining, String classesTest, String classesTraining, int x, int y,
					 int testBS, int testRS, int testRC, int trainingBS, int trainingRS, int trainingRC, int classSize, int[] combinedClasses) {
		this.RAW_DATA_PATH_TEST = rawTest;
		this.RAW_DATA_PATH_TRAINING = rawTraining;
		this.RAW_CLASSES_PATH_TEST = classesTest;
		this.RAW_CLASSES_PATH_TRAINING = classesTraining;
		this.X_SIZE = x;
		this.Y_SIZE = y;
		this.TEST_BATCH_SIZE = testBS;
		this.TEST_ROTATION_SIZE = testRS;
		this.TEST_ROTATION_COUNT = testRC;
		this.TRAINING_BATCH_SIZE = trainingBS;
		this.TRAINING_ROTATION_SIZE = trainingRS;
		this.TRAINING_ROTATION_COUNT = trainingRC;
		this.CLASS_SIZE = classSize;
		this.COMB_CLASS_SEP = combinedClasses;
	}
}
