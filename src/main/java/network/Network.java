package network;

import datasetIterator.PlantNetImageProvider;
import netType.D36;
import netType.PlantNetType;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.Collection;

public abstract class Network {

	private static final Logger LOGGER = LoggerFactory.getLogger(Network.class);
	protected final MultiLayerNetwork model;

	public Network() {
		this.model = new MultiLayerNetwork(getConfig());
		model.init();
	}

	protected Network(MultiLayerNetwork model) {
		this.model = model;
	}

	abstract protected MultiLayerConfiguration getConfig();

	abstract public void attachInitialTrainingListeners(PlantNetImageProvider imageProvider);

	public Collection<TrainingListener> getAttachedListener() {
		return model.getListeners();
	}

	public void attachTrainingListener(TrainingListener iterator) {
		model.addListeners(iterator);
	}

	public void train(DataSetIterator trainIterator, int epochs) {
		model.fit(trainIterator, epochs);
	}

	public void evaluate(PlantNetType plantNetType, DataSetType dataSetType, IEvaluation... evaluations) {
		PlantNetImageProvider imageProvider = new PlantNetImageProvider(plantNetType);
		int rotations;
		double factor;
		if(dataSetType != DataSetType.TRAIN) {
			imageProvider.loadNextRotation(dataSetType);
			rotations = plantNetType.TEST_ROTATION_COUNT;
		}else {
			rotations = plantNetType.TRAINING_ROTATION_COUNT;
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doEvaluation(imageProvider, evaluations)){
				LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	public void evaluate2(DataSetType dataSetType, IEvaluation... evaluations) {
		PlantNetImageProvider imageProvider = new PlantNetImageProvider(PlantNetType.D26);
		int rotations;
		double factor;
		if(dataSetType != DataSetType.TRAIN) {
			imageProvider.loadNextRotation(dataSetType);
			rotations = D36.TEST_ROTATION_COUNT;
		}else {
			rotations = D36.TRAINING_ROTATION_COUNT;
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doEvaluation(imageProvider, evaluations)){
//				if(((1.0 + i)*factor) >= 0.98){
					LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
//				}else {
//					eval.stats();
//				}
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	protected MultiLayerNetwork getCore() {
		return model;
	}

	public static void safe(Network network, String path) {
		try {
			File file = new File(path);
			boolean fileExists = file.exists();
			network.getCore().save(file);
			LOGGER.info(String.format("Network saved%s to path: '%s'", (fileExists? " (Overwrite)" : ""), path));
		} catch (Exception e) {
			LOGGER.error(String.format("Could not serialize and safe Network to path: '%s'\n\tReason:%s", path, e.getMessage()));
			e.printStackTrace();
		}
	}

	public static <T> T load(Class<T> netClass, String path) {
		LOGGER.info(String.format("Start loading %s from %s", netClass.getSimpleName(), path));
		File file = new File(path);
		T network = null;
		if(file.exists()) {
			try {
				for(Constructor constructor : netClass.getConstructors()) {
					if(constructor.getParameterCount() == 1 && constructor.getParameterTypes()[0].equals(MultiLayerNetwork.class)) {
						MultiLayerNetwork netCore = MultiLayerNetwork.load(file, true);
						network = (T) constructor.newInstance(netCore);
					}
				}
				if(network == null) {
					LOGGER.warn(String.format("Could not find a pleasing Constructor for: '%s'\n\tCreating new default Network...", netClass.getSimpleName()));
					network = instantiate(netClass);
				} else {
					LOGGER.info(String.format("Network (%s) loaded from path: '%s'", network.getClass().getSimpleName(), path));
				}
			}catch (Exception e) {
				LOGGER.error(String.format("Could not load Network from path: '%s'\n\tReason:%s", path, e.getMessage()));
				e.printStackTrace();
			}
		}else {
			LOGGER.info(String.format("Could not find a Network-File under path: '%s'\n\tCreating new one...", path));
			network = instantiate(netClass);
		}
		return network;
	}

	private static <T> T instantiate(Class<T> netClass) {
		T network = null;
		try {
			for(Constructor constructor : netClass.getConstructors()) {
				if(constructor.getParameterCount() == 0) {
					network = (T) constructor.newInstance(new Object[0]);
				}
			}
			if(network == null) {
				LOGGER.error(String.format("Could not find a pleasing Constructor for: '%s', returning NULL", netClass.getSimpleName()));
			}
		}catch (Exception e) {
			LOGGER.error(String.format("Could not instantiate new Network for class: '%s'\nReason:", netClass.getSimpleName(), e.getMessage()));
			e.printStackTrace();
		}
		return network;
	}
}
