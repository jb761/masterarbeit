package network.impl;

import datasetIterator.PlantNetImageProvider;
import netType.PlantNetType;
import head.Main;
import network.Network;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlexNetD12 extends Network {

	private static final Logger LOGGER = LoggerFactory.getLogger(AlexNetD12.class);

	public AlexNetD12() {
		super();
	}

	public AlexNetD12(MultiLayerNetwork multiLayerNetwork) {
		super(multiLayerNetwork);
	}

	@Override
	public MultiLayerConfiguration getConfig() {
		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(0.005, 0.9))
				.biasUpdater(new Nesterovs(0.005, 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.weightDecay(0.0001)
				.weightDecayBias(0.0001)
				.list()
				.layer(new ConvolutionLayer.Builder(11, 11)
						.nIn(3)
						.stride(4,4)
						.nOut(96)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3,3)
						.stride(2,2)
						.build())
				.layer(new ConvolutionLayer.Builder(5, 5)
						.padding(2, 2)
						.nOut(256)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3,3)
						.stride(2,2)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(384)
						.activation(Activation.RELU)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(384)
						.activation(Activation.RELU)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(256)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3, 3)
						.stride(2, 2)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(4096)
						.dropOut(0.5)
						.activation(Activation.RELU)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(4096)
						.dropOut(0.5)
						.activation(Activation.RELU)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(PlantNetType.D11.CLASS_SIZE)
						.activation(Activation.RELU)
						.build())
				.layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
						.nOut(PlantNetType.D12.CLASS_SIZE)
						.activation(Activation.SOFTMAX)
						.build())
				.setInputType(InputType.convolutionalFlat(224,224,3))
				.backpropType(BackpropType.Standard)
				.build();
	}

	@Override
	public void attachInitialTrainingListeners(PlantNetImageProvider imageProvider) {
		// do nothing
	}
}
