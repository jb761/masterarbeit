package network.impl;

import datasetIterator.PlantNetImageProvider;
import netType.PlantNetType;
import evaluation.CombinedActivationEvaluator;
import evaluation.CombinedMacroAVGTop1AccuracyEvaluator;
import evaluation.CombinedTop1AccuracyEvaluator;
import head.Main;
import network.Network;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import trainingListeners.PlantNetDatasetRotation;

public class AlexNetD25 extends Network {

	public AlexNetD25() {
		super();
	}

	public AlexNetD25(MultiLayerNetwork multiLayerNetwork) {
		super(multiLayerNetwork);
	}

	@Override
	public MultiLayerConfiguration getConfig() {
		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(0.005, 0.9))
				.biasUpdater(new Nesterovs(0.005, 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.weightDecay(0.0001)
				.weightDecayBias(0.0001)
				.list()
				.layer(new ConvolutionLayer.Builder(11, 11)
						.nIn(3)
						.stride(4,4)
						.nOut(96)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3,3)
						.stride(2,2)
						.build())
				.layer(new ConvolutionLayer.Builder(5, 5)
						.padding(2, 2)
						.nOut(256)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3,3)
						.stride(2,2)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(384)
						.activation(Activation.RELU)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(384)
						.activation(Activation.RELU)
						.build())
				.layer(new ConvolutionLayer.Builder(3, 3)
						.padding(1, 1)
						.nOut(256)
						.activation(Activation.RELU)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(3, 3)
						.stride(2, 2)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(4096)
						.dropOut(0.5)
						.activation(Activation.RELU)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(4096)
						.dropOut(0.5)
						.activation(Activation.RELU)
						.build())
				.layer(new DenseLayer.Builder()
						.nOut(PlantNetType.D25.CLASS_SIZE)
						.activation(Activation.RELU)
						.build())
				.layer(new OutputLayer.Builder(LossFunctions.LossFunction.XENT)
						.nOut(PlantNetType.D25.CLASS_SIZE)
						.activation(Activation.SIGMOID)
						.build())
				.setInputType(InputType.convolutionalFlat(224,224,3))
				.backpropType(BackpropType.Standard)
				.build();
	}

	@Override
	public void attachInitialTrainingListeners(PlantNetImageProvider imageProvider) {
		this.attachTrainingListener(new PlantNetDatasetRotation(
				imageProvider,
				5,
				15,
				new IEvaluation[]{
						new CombinedTop1AccuracyEvaluator("EBENE 5", 0, 29),
						new CombinedTop1AccuracyEvaluator("EBENE 4", 29, 88),
						new CombinedTop1AccuracyEvaluator("EBENE 3", 88, 201),
						new CombinedTop1AccuracyEvaluator("EBENE 2", 201, 425),
						new CombinedTop1AccuracyEvaluator("EBENE 1", 425, 1444),
						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 0, 29),
						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 29, 88),
						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88, 201),
						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201, 425),
						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425, 1444),
						new CombinedActivationEvaluator(29, "EBENE 5", 0, 29),
						new CombinedActivationEvaluator(59, "EBENE 4", 29, 88),
						new CombinedActivationEvaluator(113, "EBENE 3", 88, 201),
						new CombinedActivationEvaluator(224, "EBENE 2", 201, 425),
						new CombinedActivationEvaluator(1019, "EBENE 1", 425, 1444)
				},
				new IEvaluation[]{
						new CombinedTop1AccuracyEvaluator("EBENE 5", 0, 29),
						new CombinedTop1AccuracyEvaluator("EBENE 4", 29, 88),
						new CombinedTop1AccuracyEvaluator("EBENE 3", 88, 201),
						new CombinedTop1AccuracyEvaluator("EBENE 2", 201, 425),
						new CombinedTop1AccuracyEvaluator("EBENE 1", 425, 1444),
						new CombinedMacroAVGTop1AccuracyEvaluator(29, "EBENE 5", 0, 29),
						new CombinedMacroAVGTop1AccuracyEvaluator(59, "EBENE 4", 29, 88),
						new CombinedMacroAVGTop1AccuracyEvaluator(113, "EBENE 3", 88, 201),
						new CombinedMacroAVGTop1AccuracyEvaluator(224, "EBENE 2", 201, 425),
						new CombinedMacroAVGTop1AccuracyEvaluator(1019, "EBENE 1", 425, 1444),
						new CombinedActivationEvaluator(29, "EBENE 5", 0, 29),
						new CombinedActivationEvaluator(59, "EBENE 4", 29, 88),
						new CombinedActivationEvaluator(113, "EBENE 3", 88, 201),
						new CombinedActivationEvaluator(224, "EBENE 2", 201, 425),
						new CombinedActivationEvaluator(1019, "EBENE 1", 425, 1444)
				}));
	}
}
