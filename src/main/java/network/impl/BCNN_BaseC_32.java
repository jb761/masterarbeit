package network.impl;

import computationGraphExtension.CustomComputationGraph;
import computationGraphExtension.CustomLoss;
import datasetIterator.PlantNetImageProviderD33;
import evaluation.d36.D36Top1AccuracyEvaluator;
import head.Main;
import netType.D33;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import schedules.BCNNScheduler;
import trainingListeners.PlantNetDatasetRotationD33;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public class BCNN_BaseC_32 {

	private static final Logger LOGGER = LoggerFactory.getLogger(BCNN_BaseC_32.class);
	private static final double featuredOutputWeight = 0.96;
	private static final double commonOutputWeight = 0.02;

	private final CustomComputationGraph model;

	public BCNN_BaseC_32() {
		this.model = new CustomComputationGraph(getConfig());
		model.init();
	}

	public BCNN_BaseC_32(CustomComputationGraph model) {
		this.model = model;
	}

	private ComputationGraphConfiguration getConfig() {
		new CustomLoss(3, Nd4j.zeros(1, D33.E3size).addi(featuredOutputWeight));
		new CustomLoss(2, Nd4j.zeros(1, D33.E2size).addi(commonOutputWeight));
		new CustomLoss(1, Nd4j.zeros(1, D33.E1size).addi(commonOutputWeight));

		Layer p0 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).stride(7,7).build();

		Layer c11 = new ConvolutionLayer.Builder(3,3).nIn(3).nOut(64).padding(1,1).activation(Activation.RELU).build();
		Layer c12 = new ConvolutionLayer.Builder(3,3).nIn(64).nOut(64).padding(1,1).activation(Activation.RELU).build();
		Layer p1 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(2,2).build();

		Layer c21 = new ConvolutionLayer.Builder(3,3).nIn(64).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer c22 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer p2 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(2,2).build();

		Layer c31 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c32 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c33 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer p3 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(2,2).build();

		Layer c41 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c42 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c43 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer p4 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(2,2).build();

		Layer c51 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c52 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c53 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer p5 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(2,2).build();

		Layer d11 = new DenseLayer.Builder().nIn(512).nOut(4096).activation(Activation.RELU).dropOut(0.5).build();
		Layer d12 = new DenseLayer.Builder().nIn(4096).nOut(4096).activation(Activation.RELU).dropOut(0.5).build();
		Layer o1 = new OutputLayer.Builder().nIn(4096).nOut(1019).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(1)).build();

		Layer d21 = new DenseLayer.Builder().nIn(512).nOut(1024).activation(Activation.RELU).dropOut(0.5).build();
		Layer d22 = new DenseLayer.Builder().nIn(1024).nOut(1024).activation(Activation.RELU).dropOut(0.5).build();
		Layer o2 = new OutputLayer.Builder().nIn(1024).nOut(224).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(2)).build();

		Layer d31 = new DenseLayer.Builder().nIn(256).nOut(512).activation(Activation.RELU).dropOut(0.5).build();
		Layer d32 = new DenseLayer.Builder().nIn(512).nOut(512).activation(Activation.RELU).dropOut(0.5).build();
		Layer o3 = new OutputLayer.Builder().nIn(512).nOut(113).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(3)).build();

		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(new BCNNScheduler(), 0.9))
				.biasUpdater(new Nesterovs(0.003, 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.graphBuilder()
				.setInputTypes(InputType.convolutionalFlat(224,224,3))
				.addInputs("E")
				.addLayer("p0", p0, "E")
				.addLayer("c11", c11, "p0")
				.addLayer("c12", c12, "c11")
				.addLayer("p1", p1, "c12")
				.addLayer("c21", c21, "p1")
				.addLayer("c22", c22, "c21")
				.addLayer("p2", p2, "c22")
				.addLayer("c31", c31, "p2")
				.addLayer("c32", c32, "c31")
				.addLayer("c33", c33, "c32")
				.addLayer("p3", p3, "c33")
						.addLayer("d31", d31, "p3")
						.addLayer("d32", d32, "d31")
						.addLayer("o3", o3, "d32")
				.addLayer("c41", c41, "p3")
				.addLayer("c42", c42, "c41")
				.addLayer("c43", c43, "c42")
				.addLayer("p4", p4, "c43")
						.addLayer("d21", d21, "p4")
						.addLayer("d22", d22, "d21")
						.addLayer("o2", o2, "d22")
				.addLayer("c51", c51, "p4")
				.addLayer("c52", c52, "c51")
				.addLayer("c53", c53, "c52")
				.addLayer("p5", p5, "c53")
						.addLayer("d11", d11, "p5")
						.addLayer("d12", d12, "d11")
						.addLayer("o1", o1, "d12")
				.setOutputs("o3", "o2", "o1")
				.backpropType(BackpropType.Standard)
				.build();
	}

	public void attachInitialTrainingListeners(PlantNetImageProviderD33 imageProvider) {
		this.attachTrainingListener(new PlantNetDatasetRotationD33(
				imageProvider,
				5,
				15,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				},
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				}));
	}

	public Collection<TrainingListener> getAttachedListener() {
		return model.getListeners();
	}

	public void attachTrainingListener(TrainingListener iterator) {
		model.addListeners(iterator);
	}

	public void train(MultiDataSetIterator trainIterator, int epochs) {
		model.fit(trainIterator, epochs);
	}

	public void evaluate(DataSetType dataSetType, IEvaluation... evaluations) {
		PlantNetImageProviderD33 imageProvider = new PlantNetImageProviderD33();
		int rotations;
		double factor;
		if(dataSetType != DataSetType.TRAIN) {
			imageProvider.loadNextRotation(dataSetType);
			rotations = D33.TEST_ROTATION_COUNT;
		}else {
			rotations = D33.TRAINING_ROTATION_COUNT;
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doCustomEvaluation(imageProvider, evaluations)){
				LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	public void featureNextBranch(int from, int to) {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		LOGGER.info("Switch featured Branch: " + from);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
		long dim = CustomLoss.staticCustomLossMap.get(from).getWeights().length();
		CustomLoss.staticCustomLossMap.get(from).setWeights(Nd4j.zeros(1, dim).addi(commonOutputWeight));

		dim = CustomLoss.staticCustomLossMap.get(to).getWeights().length();
		CustomLoss.staticCustomLossMap.get(to).setWeights(Nd4j.zeros(1, dim).addi(featuredOutputWeight));
		LOGGER.info("Now featuring Output: " + to);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
	}

	public static void safe(BCNN_BaseC_32 network, String path) {
		try {
			File file = new File(path);
			boolean fileExists = file.exists();
			network.model.save(file);
			LOGGER.info(String.format("Network saved%s to path: '%s'", (fileExists? " (Overwrite)" : ""), path));
		} catch (Exception e) {
			LOGGER.error(String.format("Could not serialize and safe Network to path: '%s'\n\tReason:%s", path, e.getMessage()));
			e.printStackTrace();
		}
	}

	public static BCNN_BaseC_32 load(String path) {
		LOGGER.info(String.format("Start loading %s from %s", BCNN_BaseC_32.class.getSimpleName(), path));
		File file = new File(path);
		BCNN_BaseC_32 network = null;
		if(file.exists()) {
			try {
				network = new BCNN_BaseC_32(CustomComputationGraph.customLoad(file));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			LOGGER.info(String.format("Could not find a Network-File under path: '%s'\n\tCreating new one...", path));
			network = instantiate();
		}
		return network;
	}

	private static BCNN_BaseC_32 instantiate() {
		BCNN_BaseC_32 net = new BCNN_BaseC_32();
		return net;
	}
}
