package network.impl;

import computationGraphExtension.CustomComputationGraph;
import computationGraphExtension.CustomLoss;
import datasetIterator.PlantNetImageProviderD36;
import evaluation.d36.D36Top1AccuracyEvaluator;
import head.Main;
import netType.D36;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trainingListeners.PlantNetDatasetRotationD36;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public class BNet6 {

	private static final Logger LOGGER = LoggerFactory.getLogger(BNet6.class);
	private static final double featuredOutputWeight = 0.9;
	private static final double commonOutputWeight = 0.02;

	private final CustomComputationGraph model;

	public BNet6() {
		this.model = new CustomComputationGraph(getConfig());
		model.init();
	}

	public BNet6(CustomComputationGraph model) {
		this.model = model;
	}

	private ComputationGraphConfiguration getConfig() {
		new CustomLoss(6, Nd4j.zeros(1, D36.E6size).addi(featuredOutputWeight));
		new CustomLoss(5, Nd4j.zeros(1, D36.E5size).addi(commonOutputWeight));
		new CustomLoss(4, Nd4j.zeros(1, D36.E4size).addi(commonOutputWeight));
		new CustomLoss(3, Nd4j.zeros(1, D36.E3size).addi(commonOutputWeight));
		new CustomLoss(2, Nd4j.zeros(1, D36.E2size).addi(commonOutputWeight));
		new CustomLoss(1, Nd4j.zeros(1, D36.E1size).addi(commonOutputWeight));

		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(0.005, 0.9))
				.biasUpdater(new Nesterovs(0.005, 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.weightDecay(0.0001)
				.weightDecayBias(0.0001)
				.graphBuilder()
				.setInputTypes(InputType.convolutionalFlat(224,224,3))
				.addInputs("E")
				.addLayer("C1",
						new ConvolutionLayer.Builder(11, 11)
								.nIn(3)
								.stride(4,4)
								.nOut(96)
								.activation(Activation.RELU)
								.build(), "E")
				.addLayer("P1",
						new SubsamplingLayer.Builder(PoolingType.MAX)
								.kernelSize(3,3)
								.stride(2,2)
								.build(), "C1")
				.addLayer("C2",
						new ConvolutionLayer.Builder(5, 5)
								.padding(2, 2)
								.nIn(96)
								.nOut(256)
								.activation(Activation.RELU)
								.build(), "P1")
				.addLayer("P2",
						new SubsamplingLayer.Builder(PoolingType.MAX)
								.kernelSize(3,3)
								.stride(2,2)
								.build(), "C2")
				.addLayer("C3",
						new ConvolutionLayer.Builder(3, 3)
								.padding(1, 1)
								.nIn(256)
								.nOut(384)
								.activation(Activation.RELU)
								.build(), "P2")
				.addLayer("C4",
						new ConvolutionLayer.Builder(3, 3)
								.padding(1, 1)
								.nIn(384)
								.nOut(384)
								.activation(Activation.RELU)
								.build(), "C3")
				.addLayer("C5",
						new ConvolutionLayer.Builder(3, 3)
								.padding(1, 1)
								.nIn(384)
								.nOut(256)
								.activation(Activation.RELU)
								.build(), "C4")
				.addLayer("P3",
						new SubsamplingLayer.Builder(PoolingType.MAX)
								.kernelSize(3, 3)
								.stride(2, 2)
								.build(), "C5")
				.addLayer("D1",
						new DenseLayer.Builder()
								.nIn(6*6*256)
								.nOut(4096)
								.dropOut(0.5)
								.activation(Activation.RELU)
								.build(), "P3")
							// BRANCH 1
							.addLayer("B1",
									new DenseLayer.Builder()
											.nIn(4096)
											.nOut(600)
											.activation(Activation.RELU)
											.build(), "D1")
							.addLayer("O1",
									new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(6))
											.nIn(600)
											.nOut(D36.E6size)
											.activation(Activation.SOFTMAX)
											.build(), "B1")
				.addLayer("D2",
						new DenseLayer.Builder()
								.nIn(4096)
								.nOut(1500)
								.activation(Activation.RELU)
								.build(), "D1")
							// BRANCH 2
							.addLayer("B2",
									new DenseLayer.Builder()
											.nIn(1500)
											.nOut(600)
											.activation(Activation.RELU)
											.build(), "D2")
							.addLayer("O2",
									new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(5))
											.nIn(600)
											.nOut(D36.E5size)
											.activation(Activation.SOFTMAX)
											.build(), "B2")
				.addLayer("D3",
						new DenseLayer.Builder()
								.nIn(1500)
								.nOut(1500)
								.activation(Activation.RELU)
								.build(), "D2")
							// BRANCH 3
							.addLayer("B3",
									new DenseLayer.Builder()
											.nIn(1500)
											.nOut(600)
											.activation(Activation.RELU)
											.build(), "D3")
							.addLayer("O3",
									new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(4))
											.nIn(600)
											.nOut(D36.E4size)
											.activation(Activation.SOFTMAX)
											.build(), "B3")
				.addLayer("D4",
						new DenseLayer.Builder()
								.nIn(1500)
								.nOut(1500)
								.activation(Activation.RELU)
								.build(), "D3")
							// BRANCH 4
							.addLayer("B4",
									new DenseLayer.Builder()
											.nIn(1500)
											.nOut(600)
											.activation(Activation.RELU)
											.build(), "D4")
							.addLayer("O4",
									new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(3))
											.nIn(600)
											.nOut(D36.E3size)
											.activation(Activation.SOFTMAX)
											.build(), "B4")
				.addLayer("D5",
						new DenseLayer.Builder()
								.nIn(1500)
								.nOut(1500)
								.activation(Activation.RELU)
								.build(), "D4")
							// BRANCH 5
							.addLayer("B5",
									new DenseLayer.Builder()
											.nIn(1500)
											.nOut(600)
											.activation(Activation.RELU)
											.build(), "D5")
							.addLayer("O5",
									new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(2))
											.nIn(600)
											.nOut(D36.E2size)
											.activation(Activation.SOFTMAX)
											.build(), "B5")
				.addLayer("D6",
						new DenseLayer.Builder()
								.nIn(1500)
								.nOut(1500)
								.activation(Activation.RELU)
								.build(), "D5")
				.addLayer("O6",
						new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(1))
								.nIn(1500)
								.nOut(D36.E1size)
								.activation(Activation.SOFTMAX)
								.build(), "D6")
				.setOutputs("O1", "O2", "O3", "O4", "O5", "O6")
				.backpropType(BackpropType.Standard)
				.build();
	}

	public void attachInitialTrainingListeners(PlantNetImageProviderD36 imageProvider) {
		this.attachTrainingListener(new PlantNetDatasetRotationD36(
				imageProvider,
				5,
				15,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				},
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				}));
	}

	public Collection<TrainingListener> getAttachedListener() {
		return model.getListeners();
	}

	public void attachTrainingListener(TrainingListener iterator) {
		model.addListeners(iterator);
	}

	public void train(MultiDataSetIterator trainIterator, int epochs) {
		model.fit(trainIterator, epochs);
	}

	public void evaluate(DataSetType dataSetType, IEvaluation... evaluations) {
		PlantNetImageProviderD36 imageProvider = new PlantNetImageProviderD36();
		int rotations;
		double factor;
		if(dataSetType != DataSetType.TRAIN) {
			imageProvider.loadNextRotation(dataSetType);
			rotations = D36.TEST_ROTATION_COUNT;
		}else {
			rotations = D36.TRAINING_ROTATION_COUNT;
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doCustomEvaluation(imageProvider, evaluations)){
				LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	public void featureNextBranch(int from, int to) {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		LOGGER.info("Switch featured Branch: " + from);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
		long dim = CustomLoss.staticCustomLossMap.get(from).getWeights().length();
		CustomLoss.staticCustomLossMap.get(from).setWeights(Nd4j.zeros(1, dim).addi(commonOutputWeight));

		dim = CustomLoss.staticCustomLossMap.get(to).getWeights().length();
		CustomLoss.staticCustomLossMap.get(to).setWeights(Nd4j.zeros(1, dim).addi(featuredOutputWeight));
		LOGGER.info("Now featuring Output: " + to);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
	}

	public static void safe(BNet6 network, String path) {
		try {
			File file = new File(path);
			boolean fileExists = file.exists();
			network.model.save(file);
			LOGGER.info(String.format("Network saved%s to path: '%s'", (fileExists? " (Overwrite)" : ""), path));
		} catch (Exception e) {
			LOGGER.error(String.format("Could not serialize and safe Network to path: '%s'\n\tReason:%s", path, e.getMessage()));
			e.printStackTrace();
		}
	}

	public static BNet6 load(String path) {
		LOGGER.info(String.format("Start loading %s from %s", BNet6.class.getSimpleName(), path));
		File file = new File(path);
		BNet6 network = null;
		if(file.exists()) {
			try {
				network = new BNet6(CustomComputationGraph.customLoad(file));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			LOGGER.info(String.format("Could not find a Network-File under path: '%s'\n\tCreating new one...", path));
			network = instantiate();
		}
		return network;
	}

	private static BNet6 instantiate() {
		BNet6 net = new BNet6();
		return net;
	}
}
