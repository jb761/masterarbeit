package network.impl;

import computationGraphExtension.CustomComputationGraph;
import computationGraphExtension.CustomLoss;
import datasetIterator.PlantNetImageProviderD36;
import evaluation.d36.D36Top1AccuracyEvaluator;
import head.Main;
import netType.D36;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.ElementWiseVertex;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import schedules.ResNet50Scheduler;
import trainingListeners.PlantNetDatasetRotationD36;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public class BranchedResNet50 {

	private static final Logger LOGGER = LoggerFactory.getLogger(BranchedResNet50.class);
	private static final double featuredOutputWeight = 0.92;
	private static final double commonOutputWeight = 0.02;

	private final CustomComputationGraph model;

	public BranchedResNet50() {
		this.model = new CustomComputationGraph(getConfig());
		model.init();
	}

	public BranchedResNet50(CustomComputationGraph model) {
		this.model = model;
	}

	private ComputationGraphConfiguration getConfig() {
		new CustomLoss(6, Nd4j.zeros(1, D36.E6size).addi(featuredOutputWeight));
		new CustomLoss(5, Nd4j.zeros(1, D36.E5size).addi(commonOutputWeight));
		new CustomLoss(4, Nd4j.zeros(1, D36.E4size).addi(commonOutputWeight));
		new CustomLoss(3, Nd4j.zeros(1, D36.E3size).addi(commonOutputWeight));
		new CustomLoss(2, Nd4j.zeros(1, D36.E2size).addi(commonOutputWeight));
		new CustomLoss(1, Nd4j.zeros(1, D36.E1size).addi(commonOutputWeight));

		// --> 224
		Layer c1 = new ConvolutionLayer.Builder(7,7).nIn(3).nOut(64).stride(2,2).padding(3,3).activation(Activation.RELU).build();

		// --> 112
		Layer p1 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(3,3).stride(2,2).padding(1,1).build();

		// --> 56
		Layer c211 = new ConvolutionLayer.Builder(1,1).nIn(64).nOut(64).padding(0,0).activation(Activation.RELU).build();
		Layer c212 = new ConvolutionLayer.Builder(3,3).nIn(64).nOut(64).padding(1,1).activation(Activation.RELU).build();
		Layer c213 = new ConvolutionLayer.Builder(1,1).nIn(64).nOut(256).padding(0,0).activation(Activation.RELU).build();

		Layer c221 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(64).padding(0,0).activation(Activation.RELU).build();
		Layer c222 = new ConvolutionLayer.Builder(3,3).nIn(64).nOut(64).padding(1,1).activation(Activation.RELU).build();
		Layer c223 = new ConvolutionLayer.Builder(1,1).nIn(64).nOut(256).padding(0,0).activation(Activation.RELU).build();

		Layer c231 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(64).padding(0,0).activation(Activation.RELU).build();
		Layer c232 = new ConvolutionLayer.Builder(3,3).nIn(64).nOut(64).padding(1,1).activation(Activation.RELU).build();
		Layer c233 = new ConvolutionLayer.Builder(1,1).nIn(64).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer p321 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(2,2).stride(2,2).build();

		Layer c311 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(128).stride(2,2).padding(0,0).activation(Activation.RELU).build();
		// --> 28
		Layer c312 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer c313 = new ConvolutionLayer.Builder(1,1).nIn(128).nOut(512).padding(0,0).activation(Activation.RELU).build();

		Layer c321 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(128).padding(0,0).activation(Activation.RELU).build();
		Layer c322 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer c323 = new ConvolutionLayer.Builder(1,1).nIn(128).nOut(512).padding(0,0).activation(Activation.RELU).build();

		Layer c331 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(128).padding(0,0).activation(Activation.RELU).build();
		Layer c332 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer c333 = new ConvolutionLayer.Builder(1,1).nIn(128).nOut(512).padding(0,0).activation(Activation.RELU).build();
		// BRANCH EBENE 6
		Layer c341 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(128).padding(0,0).activation(Activation.RELU).build();
		Layer c342 = new ConvolutionLayer.Builder(3,3).nIn(128).nOut(128).padding(1,1).activation(Activation.RELU).build();
		Layer c343 = new ConvolutionLayer.Builder(1,1).nIn(128).nOut(512).padding(0,0).activation(Activation.RELU).build();
		Layer p421 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(2,2).stride(2,2).build();

		Layer c411 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(256).stride(2,2).padding(0,0).activation(Activation.RELU).build();
		// --> 14
		Layer c412 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c413 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();
		// BRANCH EBENE 5
		Layer c421 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer c422 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c423 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();

		Layer c431 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer c432 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c433 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();
		// BRANCH EBENE 4
		Layer c441 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer c442 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c443 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();

		Layer c451 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer c452 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c453 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();
		// BRANCH EBENE 3
		Layer c461 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(256).padding(0,0).activation(Activation.RELU).build();
		Layer c462 = new ConvolutionLayer.Builder(3,3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c463 = new ConvolutionLayer.Builder(1,1).nIn(256).nOut(1024).padding(0,0).activation(Activation.RELU).build();
		Layer p521 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(2,2).stride(2,2).build();

		Layer c511 = new ConvolutionLayer.Builder(1,1).nIn(1024).nOut(512).stride(2,2).padding(0,0).activation(Activation.RELU).build();
		// --> 7
		Layer c512 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c513 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(2048).padding(0,0).activation(Activation.RELU).build();
		// BRANCH EBENE 2
		Layer c521 = new ConvolutionLayer.Builder(1,1).nIn(2048).nOut(512).padding(0,0).activation(Activation.RELU).build();
		Layer c522 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c523 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(2048).padding(0,0).activation(Activation.RELU).build();

		Layer c531 = new ConvolutionLayer.Builder(1,1).nIn(2048).nOut(512).padding(0,0).activation(Activation.RELU).build();
		Layer c532 = new ConvolutionLayer.Builder(3,3).nIn(512).nOut(512).padding(1,1).activation(Activation.RELU).build();
		Layer c533 = new ConvolutionLayer.Builder(1,1).nIn(512).nOut(2048).padding(0,0).activation(Activation.RELU).build();

		Layer po1 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).build();

		// --> 1
		Layer o1 = new OutputLayer.Builder().nIn(2048).nOut(D36.E1size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(1)).build();

		Layer po2 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).build();
		Layer o2 = new OutputLayer.Builder().nIn(2048).nOut(D36.E2size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(2)).build();

		Layer po3 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).stride(7,7).build();
		Layer o3 = new OutputLayer.Builder().nIn(1024*2*2).nOut(D36.E3size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(3)).build();

		Layer po4 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).stride(7,7).build();
		Layer o4 = new OutputLayer.Builder().nIn(1024*2*2).nOut(D36.E4size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(4)).build();

		Layer po5 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).stride(7,7).build();
		Layer o5 = new OutputLayer.Builder().nIn(1024*2*2).nOut(D36.E5size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(5)).build();

		Layer po6 = new SubsamplingLayer.Builder(PoolingType.AVG).kernelSize(7,7).stride(7,7).build();
		Layer o6 = new OutputLayer.Builder().nIn(512*4*4).nOut(D36.E6size).activation(Activation.SOFTMAX).lossFunction(CustomLoss.staticCustomLossMap.get(6)).build();

		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(new ResNet50Scheduler(), 0.9))
				.biasUpdater(new Nesterovs(new ResNet50Scheduler(), 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.weightDecay(0.0001)
				.weightDecayBias(0.0001)
				.graphBuilder()
				.setInputTypes(InputType.convolutionalFlat(224,224,3))
				.addInputs("E")
				.addLayer("c1",c1,"E")
				.addLayer("p1",p1,"c1")
				.addVertex("concat_p1", new MergeVertex(), "p1", "p1", "p1", "p1")
				.addLayer("c211",c211,"p1")
				.addLayer("c212",c212,"c211")
				.addLayer("c213",c213,"c212")
				.addVertex("add_concat_p1_c212", new ElementWiseVertex(ElementWiseVertex.Op.Add), "concat_p1", "c213")
				.addLayer("c221",c221,"add_concat_p1_c212")
				.addLayer("c222",c222,"c221")
				.addLayer("c223",c223,"c222")
				.addVertex("add_c213_c223", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c213", "c223")
				.addLayer("c231",c231,"add_c213_c223")
				.addLayer("c232",c232,"c231")
				.addLayer("c233",c233,"c232")
				.addLayer("p321",p321,"c233")
				.addVertex("concat_p321", new MergeVertex(), "p321", "p321")
				.addVertex("add_c223_c233", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c223", "c233")
				.addLayer("c311",c311,"add_c223_c233")
				.addLayer("c312",c312,"c311")
				.addLayer("c313",c313,"c312")
				.addVertex("add_concat_p321_c313", new ElementWiseVertex(ElementWiseVertex.Op.Add), "concat_p321", "c313")
				.addLayer("c321",c321,"add_concat_p321_c313")
				.addLayer("c322",c322,"c321")
				.addLayer("c323",c323,"c322")
				.addVertex("add_c313_c323", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c313", "c323")
				.addLayer("c331",c331,"add_c313_c323")
				.addLayer("c332",c332,"c331")
				.addLayer("c333",c333,"c332")
				.addVertex("add_c323_c333", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c323", "c333")
				.addLayer("c341",c341,"add_c323_c333")
				.addLayer("c342",c342,"c341")
				.addLayer("c343",c343,"c342")
				.addLayer("p421",p421,"c343")
				.addVertex("concat_p421", new MergeVertex(), "p421", "p421")
				.addVertex("add_c333_c343", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c333", "c343")
				.addLayer("c411",c411,"add_c333_c343")
				.addLayer("c412",c412,"c411")
				.addLayer("c413",c413,"c412")
				.addVertex("add_concat_p421_413", new ElementWiseVertex(ElementWiseVertex.Op.Add), "concat_p421", "c413")
				.addLayer("c421",c421,"add_concat_p421_413")
				.addLayer("c422",c422,"c421")
				.addLayer("c423",c423,"c422")
				.addVertex("add_c413_c423", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c413", "c423")
				.addLayer("c431",c431,"add_c413_c423")
				.addLayer("c432",c432,"c431")
				.addLayer("c433",c433,"c432")
				.addVertex("add_c423_c433", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c423", "c433")
				.addLayer("c441",c441,"add_c423_c433")
				.addLayer("c442",c442,"c441")
				.addLayer("c443",c443,"c442")
				.addVertex("add_c433_c443", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c433", "c443")
				.addLayer("c451",c451,"add_c433_c443")
				.addLayer("c452",c452,"c451")
				.addLayer("c453",c453,"c452")
				.addVertex("add_c443_c453", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c443", "c453")
				.addLayer("c461",c461,"add_c443_c453")
				.addLayer("c462",c462,"c461")
				.addLayer("c463",c463,"c462")
				.addLayer("p521",p521,"c463")
				.addVertex("concat_p521", new MergeVertex(), "p521", "p521")
				.addVertex("add_c453_c463", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c453", "c463")
				.addLayer("c511",c511,"add_c453_c463")
				.addLayer("c512",c512,"c511")
				.addLayer("c513",c513,"c512")
				.addVertex("add_concat_p521_c513", new ElementWiseVertex(ElementWiseVertex.Op.Add), "concat_p521", "c513")
				.addLayer("c521",c521,"add_concat_p521_c513")
				.addLayer("c522",c522,"c521")
				.addLayer("c523",c523,"c522")
				.addVertex("add_c513_c523", new ElementWiseVertex(ElementWiseVertex.Op.Add), "c513", "c523")
				.addLayer("c531",c531,"add_c513_c523")
				.addLayer("c532",c532,"c531")
				.addLayer("c533",c533,"c532")

				.addLayer("po1",po1,"c533")
				.addLayer("O1",o1,"po1")

				.addLayer("po2",po2,"c513")
				.addLayer("O2",o2,"po2")

				.addLayer("po3",po3,"c453")
				.addLayer("O3",o3,"po3")

				.addLayer("po4",po4,"c433")
				.addLayer("O4",o4,"po4")

				.addLayer("po5",po5,"c413")
				.addLayer("O5",o5,"po5")

				.addLayer("po6",po6,"c333")
				.addLayer("O6",o6,"po6")
				.setOutputs("O6", "O5", "O4", "O3", "O2", "O1")
				.backpropType(BackpropType.Standard)
				.build();
	}

	public void attachInitialTrainingListeners(PlantNetImageProviderD36 imageProvider) {
		this.attachTrainingListener(new PlantNetDatasetRotationD36(
				imageProvider,
				5,
				15,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				},
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				}));
	}

	public Collection<TrainingListener> getAttachedListener() {
		return model.getListeners();
	}

	public void attachTrainingListener(TrainingListener iterator) {
		model.addListeners(iterator);
	}

	public void train(MultiDataSetIterator trainIterator, int epochs) {
		model.fit(trainIterator, epochs);
	}

	public void evaluate(DataSetType dataSetType, IEvaluation... evaluations) {
		PlantNetImageProviderD36 imageProvider = new PlantNetImageProviderD36();
		int rotations;
		double factor;
		if(dataSetType != DataSetType.TRAIN) {
			imageProvider.loadNextRotation(dataSetType);
			rotations = D36.TEST_ROTATION_COUNT;
		}else {
			rotations = D36.TRAINING_ROTATION_COUNT;
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doCustomEvaluation(imageProvider, evaluations)){
				LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	public void featureNextBranch(int from, int to) {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		LOGGER.info("Switch featured Branch: " + from);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
		long dim = CustomLoss.staticCustomLossMap.get(from).getWeights().length();
		CustomLoss.staticCustomLossMap.get(from).setWeights(Nd4j.zeros(1, dim).addi(commonOutputWeight));

		dim = CustomLoss.staticCustomLossMap.get(to).getWeights().length();
		CustomLoss.staticCustomLossMap.get(to).setWeights(Nd4j.zeros(1, dim).addi(featuredOutputWeight));
		LOGGER.info("Now featuring Output: " + to);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			System.out.println(String.format("Output %d >--> value: %1.3f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
	}

	public static void safe(BranchedResNet50 network, String path) {
		try {
			File file = new File(path);
			boolean fileExists = file.exists();
			network.model.save(file);
			LOGGER.info(String.format("Network saved%s to path: '%s'", (fileExists? " (Overwrite)" : ""), path));
		} catch (Exception e) {
			LOGGER.error(String.format("Could not serialize and safe Network to path: '%s'\n\tReason:%s", path, e.getMessage()));
			e.printStackTrace();
		}
	}

	public static BranchedResNet50 load(String path) {
		LOGGER.info(String.format("Start loading %s from %s", BranchedResNet50.class.getSimpleName(), path));
		File file = new File(path);
		BranchedResNet50 network = null;
		if(file.exists()) {
			try {
				network = new BranchedResNet50(CustomComputationGraph.customLoad(file));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			LOGGER.info(String.format("Could not find a Network-File under path: '%s'\n\tCreating new one...", path));
			network = instantiate();
		}
		return network;
	}

	private static BranchedResNet50 instantiate() {
		BranchedResNet50 net = new BranchedResNet50();
		return net;
	}
}
