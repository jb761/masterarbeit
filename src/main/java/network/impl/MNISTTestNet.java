package network.impl;

import datasetIterator.PlantNetImageProvider;
import network.Network;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MNISTTestNet extends Network {

	private static final Logger LOGGER = LoggerFactory.getLogger(MNISTTestNet.class);

	public MNISTTestNet() {
		super();
	}

	public MNISTTestNet(MultiLayerNetwork multiLayerNetwork) {
		super(multiLayerNetwork);
	}

	@Override
	public MultiLayerConfiguration getConfig() {
		return new NeuralNetConfiguration.Builder()
				.seed(9875732)
				.weightInit(WeightInit.XAVIER)
				.updater(new Sgd.Builder().learningRate(0.01).build())
				.list()
				.layer(new ConvolutionLayer.Builder(5, 5)
						//nIn and nOut specify depth. nIn here is the nChannels and nOut is the number of filters to be applied
						.nIn(1)
						.stride(1,1)
						.nOut(20)
						.activation(Activation.IDENTITY)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(2,2)
						.stride(2,2)
						.build())
				.layer(new ConvolutionLayer.Builder(5, 5)
						//Note that nIn need not be specified in later layers
						.stride(1,1)
						.nOut(50)
						.activation(Activation.IDENTITY)
						.build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX)
						.kernelSize(2,2)
						.stride(2,2)
						.build())
				.layer(new DenseLayer.Builder().activation(Activation.RELU)
						.nOut(500).build())
				.layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
						.nOut(10)
						.activation(Activation.SOFTMAX)
						.build())
				.setInputType(InputType.convolutionalFlat(28,28,1))
				.backpropType(BackpropType.Standard)
				.build();
	}

	@Override
	public void attachInitialTrainingListeners(PlantNetImageProvider imageProvider) {
		// do nothing
	}
}
