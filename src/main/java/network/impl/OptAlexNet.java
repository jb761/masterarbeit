package network.impl;

import computationGraphExtension.CustomComputationGraph;
import computationGraphExtension.CustomLoss;
import datasetIterator.DAD76_PlantNetImageProvider;
import evaluation.Top1ScatterEvaluator;
import evaluation.condFF.CombinedD36ConditionalSelectiveFeedForward;
import evaluation.d36.D36CombinedActivationEvaluator;
import evaluation.d36.D36CombinedMacroAVGTop1AccuracyEvaluator;
import evaluation.d36.D36Top1AccuracyEvaluator;
import evaluation.d36.D36Top1AccuracyEvaluatorWithoutReset;
import head.Main;
import netType.D36;
import netType.D76;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.TrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import schedules.ResNet50Scheduler;
import trainingListeners.DAD76_PlantNetDatasetRotation;
import util.DAD76;
import util.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class OptAlexNet {

	private static final Logger LOGGER = LoggerFactory.getLogger(OptAlexNet.class);
	private static final double PRE_FOCUS_WEIGHT = 0.02;
	private static final double MID_FOCUS_WEIGHT = 0.92;
	private static final double POST_FOCUS_WEIGHT = 0.10;

	private final CustomComputationGraph model;

	public OptAlexNet() throws IOException {
		this.model = new CustomComputationGraph(getConfig());
		INDArray preTrained = getWeights("E:\\Informatik Visual_Computing\\Semester XI\\modelTranslation\\AlexNetPretrained");
		preTrained = preTrained.reshape(new int[]{1,(int)preTrained.shape()[0]}); // what? (https://github.com/eclipse/deeplearning4j/issues/7889#issuecomment-500091949)
		model.init(preTrained, true);
//		model.init();
	}

	public OptAlexNet(CustomComputationGraph model) {
		this.model = model;
	}

	private ComputationGraphConfiguration getConfig() {
		new CustomLoss(6, Nd4j.zeros(1, D76.E6size).addi(MID_FOCUS_WEIGHT));
		new CustomLoss(5, Nd4j.zeros(1, D76.E5size).addi(PRE_FOCUS_WEIGHT));
		new CustomLoss(4, Nd4j.zeros(1, D76.E4size).addi(PRE_FOCUS_WEIGHT));
		new CustomLoss(3, Nd4j.zeros(1, D76.E3size).addi(PRE_FOCUS_WEIGHT));
		new CustomLoss(2, Nd4j.zeros(1, D76.E2size).addi(PRE_FOCUS_WEIGHT));
		new CustomLoss(1, Nd4j.zeros(1, D76.E1size).addi(PRE_FOCUS_WEIGHT));

		// EBENE 6
		Layer c11 = new ConvolutionLayer.Builder(11, 11).nIn(3).nOut(64).stride(4,4).activation(Activation.RELU).build();
		Layer p12 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(3,3).stride(2,2).build();
		Layer c13 = new ConvolutionLayer.Builder(5, 5).nIn(64).nOut(192).padding(2,2).activation(Activation.RELU).build();
		Layer p14 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(3,3).stride(2,2).build();
		Layer c15 = new ConvolutionLayer.Builder(3, 3).nIn(192).nOut(384).padding(1,1).activation(Activation.RELU).build();
		Layer c16 = new ConvolutionLayer.Builder(3, 3).nIn(384).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer c17 = new ConvolutionLayer.Builder(3, 3).nIn(256).nOut(256).padding(1,1).activation(Activation.RELU).build();
		Layer p18 = new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(3,3).stride(2,2).build();
		Layer d19 = new DenseLayer.Builder().nIn(5*5*256).nOut(2048).dropOut(0.5).activation(Activation.RELU).build();

		Layer o1 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(1)).nIn(1*2048).nOut(D76.E1size).activation(Activation.SOFTMAX).build();
		Layer o2 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(2)).nIn(1*2048).nOut(D76.E2size).activation(Activation.SOFTMAX).build();
		Layer o3 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(3)).nIn(1*2048).nOut(D76.E3size).activation(Activation.SOFTMAX).build();
		Layer o4 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(4)).nIn(1*2048).nOut(D76.E4size).activation(Activation.SOFTMAX).build();
		Layer o5 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(5)).nIn(1*2048).nOut(D76.E5size).activation(Activation.SOFTMAX).build();
		Layer o6 = new OutputLayer.Builder(CustomLoss.staticCustomLossMap.get(6)).nIn(1*2048).nOut(D76.E6size).activation(Activation.SOFTMAX).build();

		return new NeuralNetConfiguration.Builder()
				.seed(Main.SEED)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(new ResNet50Scheduler(), 0.9))
				.biasUpdater(new Nesterovs(new ResNet50Scheduler(), 0.9))
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
				.weightDecay(0.0001)
				.weightDecayBias(0.0001)
				.graphBuilder()
				.setInputTypes(InputType.convolutionalFlat(224,224,3))
				.addInputs("E")
				// EBENE 6
				.addLayer("c11",c11,"E")
				.addLayer("p12",p12,"c11")
				.addLayer("c13",c13,"p12")
				.addLayer("p14",p14,"c13")
				.addLayer("c15",c15,"p14")
				.addLayer("c16",c16,"c15")
				.addLayer("c17",c17,"c16")
				.addLayer("p18",p18,"c17")
				.addLayer("d19",d19,"p18")

				.addLayer("O1",o1,"d19")
				.addLayer("O2",o2,"d19")
				.addLayer("O3",o3,"d19")
				.addLayer("O4",o4,"d19")
				.addLayer("O5",o5,"d19")
				.addLayer("O6",o6,"d19")

				.setOutputs("O6", "O5", "O4", "O3", "O2", "O1")
				.backpropType(BackpropType.Standard)
				.build();
	}

	public void attachInitialTrainingListeners(DAD76_PlantNetImageProvider imageProvider) {
		this.attachTrainingListener(new DAD76_PlantNetDatasetRotation(
				imageProvider,
				5,
				15,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				},
				new IEvaluation[]{
						new D36Top1AccuracyEvaluator("EBENE 6"),
						new D36Top1AccuracyEvaluator("EBENE 5"),
						new D36Top1AccuracyEvaluator("EBENE 4"),
						new D36Top1AccuracyEvaluator("EBENE 3"),
						new D36Top1AccuracyEvaluator("EBENE 2"),
						new D36Top1AccuracyEvaluator("EBENE 1"),
				}));
	}

	public Collection<TrainingListener> getAttachedListener() {
		return model.getListeners();
	}

	public void attachTrainingListener(TrainingListener iterator) {
		model.addListeners(iterator);
	}

	public void train(MultiDataSetIterator trainIterator, int epochs) {
		model.fit(trainIterator, epochs);
	}

	public void evaluate(DataSetType dataSetType, IEvaluation... evaluations) {
		DAD76_PlantNetImageProvider imageProvider = null;
		int rotations = -1;
		double factor;
		switch (dataSetType) {
			case TRAIN: {
				imageProvider = new DAD76_PlantNetImageProvider(1, 0, 0);
				imageProvider.loadNextRotation(dataSetType);
				rotations = D76.TRAINING_ROTATION_COUNT;
				break;
			}
			case TEST: {
				imageProvider = new DAD76_PlantNetImageProvider(1, 1, 0);
				imageProvider.loadNextRotation(dataSetType);
				rotations = D76.TEST_ROTATION_COUNT;
				break;
			}
			case VALIDATION: {
				imageProvider = new DAD76_PlantNetImageProvider(1, 0, 1);
				imageProvider.loadNextRotation(dataSetType);
				rotations = D76.VALIDATION_ROTATION_COUNT;
				break;
			}
		}
		factor = 1.0 / (double)rotations;
		for(int i=0; i<rotations; i++) {
			for(IEvaluation eval : model.doCustomEvaluation(imageProvider, evaluations)){
				if(((1.0 + i)*factor) >= 0.99 || i==0){
					LOGGER.info(dataSetType.name() + " -> rel. Partition: " + ((1.0 + i)*factor) + "\t" +eval.stats());
				}else {
					eval.stats();
				}
			}
			imageProvider.loadNextRotation(dataSetType);
		}
	}

	public void featureBranch(int focus) {
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		LOGGER.info("Switch featured Branch: " + focus);
		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			LOGGER.info(String.format("Output %d >--> value: %1.5f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
		LOGGER.info("- - - - - - - - - - - - -");
		long dim;
		for(int i=6; i>focus; i--) {
			CustomLoss.staticCustomLossMap.get(i).setWeights(DAD76.getBalancedLossWeights(i, POST_FOCUS_WEIGHT));
//			dim = CustomLoss.staticCustomLossMap.get(i).getWeights().length();
//			CustomLoss.staticCustomLossMap.get(i).setWeights(Nd4j.zeros(1, dim).addi(POST_FOCUS_WEIGHT));
		}
		CustomLoss.staticCustomLossMap.get(focus).setWeights(DAD76.getBalancedLossWeights(focus, MID_FOCUS_WEIGHT));
//		dim = CustomLoss.staticCustomLossMap.get(focus).getWeights().length();
//		CustomLoss.staticCustomLossMap.get(focus).setWeights(Nd4j.zeros(1, dim).addi(MID_FOCUS_WEIGHT));
		for(int i=focus-1; i>=1; i--) {
			CustomLoss.staticCustomLossMap.get(i).setWeights(DAD76.getBalancedLossWeights(i, PRE_FOCUS_WEIGHT));
//			dim = CustomLoss.staticCustomLossMap.get(i).getWeights().length();
//			CustomLoss.staticCustomLossMap.get(i).setWeights(Nd4j.zeros(1, dim).addi(PRE_FOCUS_WEIGHT));
		}

		for(Map.Entry<Integer, CustomLoss> lossEntry : CustomLoss.staticCustomLossMap.entrySet()) {
			LOGGER.info(String.format("Output %d >--> value: %1.5f",
					lossEntry.getKey(),
					(lossEntry.getValue().getWeights().sumNumber().doubleValue()
							/ (double)lossEntry.getValue().getWeights().length())));
		}
	}

	public static void eval1(String name) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		OptAlexNet net = OptAlexNet.load("E:\\playground\\"+name+".cnn");
		net.evaluate(DataSetType.TEST,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 6"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 5"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 4"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 3"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 2"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 1"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E6size, "Ebene 6"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E5size, "Ebene 5"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E4size, "Ebene 4"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E3size, "Ebene 3"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E2size, "Ebene 2"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E1size, "Ebene 1"),
						new D36CombinedActivationEvaluator(D36.E6size, "Ebene 6"),
						new D36CombinedActivationEvaluator(D36.E5size, "Ebene 5"),
						new D36CombinedActivationEvaluator(D36.E4size, "Ebene 4"),
						new D36CombinedActivationEvaluator(D36.E3size, "Ebene 3"),
						new D36CombinedActivationEvaluator(D36.E2size, "Ebene 2"),
						new D36CombinedActivationEvaluator(D36.E1size, "Ebene 1"),
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("TEST --> "+name+" \n");
	}

	public static void eval2(String name) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		OptAlexNet net = OptAlexNet.load("E:\\playground\\"+name+".cnn");
		net.evaluate(DataSetType.VALIDATION,
				new IEvaluation[]{
						new CombinedD36ConditionalSelectiveFeedForward(0, 0),
						new CombinedD36ConditionalSelectiveFeedForward(1, 0),
						new CombinedD36ConditionalSelectiveFeedForward(2, 0),
						new CombinedD36ConditionalSelectiveFeedForward(3, 0),
						new CombinedD36ConditionalSelectiveFeedForward(4, 0),
						new CombinedD36ConditionalSelectiveFeedForward(5, 0),
						new CombinedD36ConditionalSelectiveFeedForward(0, 1),
						new CombinedD36ConditionalSelectiveFeedForward(1, 1),
						new CombinedD36ConditionalSelectiveFeedForward(2, 1),
						new CombinedD36ConditionalSelectiveFeedForward(3, 1),
						new CombinedD36ConditionalSelectiveFeedForward(4, 1),
						new CombinedD36ConditionalSelectiveFeedForward(5, 1),
						new CombinedD36ConditionalSelectiveFeedForward(0, 2),
						new CombinedD36ConditionalSelectiveFeedForward(1, 2),
						new CombinedD36ConditionalSelectiveFeedForward(2, 2),
						new CombinedD36ConditionalSelectiveFeedForward(3, 2),
						new CombinedD36ConditionalSelectiveFeedForward(4, 2),
						new CombinedD36ConditionalSelectiveFeedForward(5, 2),
						new CombinedD36ConditionalSelectiveFeedForward(0, 3),
						new CombinedD36ConditionalSelectiveFeedForward(1, 3),
						new CombinedD36ConditionalSelectiveFeedForward(2, 3),
						new CombinedD36ConditionalSelectiveFeedForward(3, 3),
						new CombinedD36ConditionalSelectiveFeedForward(4, 3),
						new CombinedD36ConditionalSelectiveFeedForward(5, 3),
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("VALIDATION --> "+name+" \n");
	}

	public static void eval3(String name) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		OptAlexNet net = OptAlexNet.load("E:\\playground\\"+name+".cnn");
		net.evaluate(DataSetType.TRAIN,
				new IEvaluation[]{
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 6"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 5"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 4"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 3"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 2"),
						new D36Top1AccuracyEvaluatorWithoutReset("Ebene 1"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E6size, "Ebene 6"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E5size, "Ebene 5"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E4size, "Ebene 4"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E3size, "Ebene 3"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E2size, "Ebene 2"),
						new D36CombinedMacroAVGTop1AccuracyEvaluator(D36.E1size, "Ebene 1")
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("TRAIN --> "+name+" \n");
	}

	public static void eval4(String name) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		OptAlexNet net = OptAlexNet.load("E:\\playground\\"+name+".cnn");
		net.evaluate(DataSetType.TEST,
				new IEvaluation[]{
						new Top1ScatterEvaluator("EBENE 6"),
						new Top1ScatterEvaluator("EBENE 5"),
						new Top1ScatterEvaluator("EBENE 4"),
						new Top1ScatterEvaluator("EBENE 3"),
						new Top1ScatterEvaluator("EBENE 2"),
						new Top1ScatterEvaluator("EBENE 1"),
				});
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info(String.format("TOTAL TIME: %1.3f Sec.", sw.get(1000)));
		LOGGER.info("--------------------------------------------------------------------------------------------------");
		LOGGER.info("TEST --> "+name+" \n");
	}

	public static void safe(OptAlexNet network, String path) {
		try {
			File file = new File(path);
			boolean fileExists = file.exists();
			network.model.save(file);
			LOGGER.info(String.format("Network saved%s to path: '%s'", (fileExists? " (Overwrite)" : ""), path));
		} catch (Exception e) {
			LOGGER.error(String.format("Could not serialize and safe Network to path: '%s'\n\tReason:%s", path, e.getMessage()));
			e.printStackTrace();
		}
	}

	public static OptAlexNet load(String path) throws IOException {
		LOGGER.info(String.format("Start loading %s from %s", OptAlexNet.class.getSimpleName(), path));
		File file = new File(path);
		OptAlexNet network = null;
		if(file.exists()) {
			try {
				network = new OptAlexNet(CustomComputationGraph.customLoad(file));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			LOGGER.info(String.format("Could not find a Network-File under path: '%s'\n\tCreating new one...", path));
			network = instantiate();
		}
		return network;
	}

	private static OptAlexNet instantiate() throws IOException {
		OptAlexNet net = new OptAlexNet();
		return net;
	}

	private INDArray getWeights(String path) throws IOException {
		Stopwatch sw = new Stopwatch().start();
		LOGGER.info("Load pretrained weights from '" + path +"' for all Conv. Layers and normal initialized for everything else... (OptAlexNet_SpareParts.cnn)");
		Map<String, File> weightFiles = new HashMap<>();
		for(File child : new File(path).listFiles()) {
			weightFiles.put(child.getName(), child);
		}

		OptAlexNet spareParts = load("E:\\playground\\OptAlexNet_SpareParts.cnn");

		float[] result = new float[0
						+23296
						+307392
						+663936
						+884992
						+590080
						+13109248 // warn --> This should be 6x6x256 but who cares?
						+2087931
						+458976
						+231537
						+120891
						+59421
						+32784
				];
		int index = 0;
		for(float value : load(weightFiles.get("features.0.bias")).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.0.weight")).reshape(23232).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.3.bias")).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.3.weight")).reshape(307200).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.6.bias")).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.6.weight")).reshape(663552).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.8.bias")).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.8.weight")).reshape(884736).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.10.bias")).toFloatVector()) {result[index++] = value;}
		for(float value : load(weightFiles.get("features.10.weight")).reshape(589824).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("d19").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("d19").paramTable().get("W").reshape(5*5*256*2048).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O1").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O1").paramTable().get("W").reshape(2048*D76.E1size).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O2").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O2").paramTable().get("W").reshape(2048*D76.E2size).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O3").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O3").paramTable().get("W").reshape(2048*D76.E3size).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O4").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O4").paramTable().get("W").reshape(2048*D76.E4size).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O5").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O5").paramTable().get("W").reshape(2048*D76.E5size).toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O6").paramTable().get("b").toFloatVector()) {result[index++] = value;}
		for(float value : spareParts.model.getLayer("O6").paramTable().get("W").reshape(2048*D76.E6size).toFloatVector()) {result[index++] = value;}
		LOGGER.info("...done within " + sw.get(1000) + " Sec.\t(" + result.length + " params in total)");
		return Nd4j.create(result);
	}

	private INDArray load(File file) throws IOException {
		return Nd4j.readNumpy(DataType.FLOAT, file.getPath(), ",");
	}
}
