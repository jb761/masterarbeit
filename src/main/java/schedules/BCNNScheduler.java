package schedules;

import netType.D33;
import org.nd4j.linalg.schedule.ISchedule;

public class BCNNScheduler implements ISchedule {

	@Override
	public double valueAt(int iteration, int epoch) {
		if(epoch <= 40 * D33.TRAINING_ROTATION_COUNT) {
			return 0.003;
		}
		if(epoch <= 50 * D33.TRAINING_ROTATION_COUNT) {
			return 0.0005;
		}
		return 0.0001;
	}

	@Override
	public ISchedule clone() {
		return new BCNNScheduler();
	}
}
