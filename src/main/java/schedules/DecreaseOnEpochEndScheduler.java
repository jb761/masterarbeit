package schedules;

import org.nd4j.linalg.schedule.ISchedule;

public class DecreaseOnEpochEndScheduler implements ISchedule {

	private double currentValue;
	private double factor;
	private int currentEpoch;

	public DecreaseOnEpochEndScheduler() {
		// do nothing.
	}

	public DecreaseOnEpochEndScheduler(double startValue, double factor, int currentEpoch) {
		this.currentValue = startValue;
		this.factor = factor;
		this.currentEpoch = currentEpoch;
	}

	@Override
	public double valueAt(int iteration, int epoch) {
		if(epoch > currentEpoch) {
			epoch++;
			currentValue *= factor;
		}
		return currentValue;
	}

	@Override
	public ISchedule clone() {
		return new DecreaseOnEpochEndScheduler(currentValue, factor, currentEpoch);
	}

	public double getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(double currentValue) {
		this.currentValue = currentValue;
	}

	public double getFactor() {
		return factor;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public int getCurrentEpoch() {
		return currentEpoch;
	}

	public void setCurrentEpoch(int currentEpoch) {
		this.currentEpoch = currentEpoch;
	}
}
