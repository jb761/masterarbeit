package schedules;

import org.nd4j.linalg.schedule.ISchedule;

public class ResNet50Scheduler implements ISchedule {

	@Override
	public double valueAt(int iteration, int epoch) {
//		if(epoch <= 10 * D36.TRAINING_ROTATION_COUNT) {
//			return 0.01;
//		}
//		if(epoch <= 20 * D36.TRAINING_ROTATION_COUNT) {
//			return 0.001;
//		}
//		if(epoch <= 30 * D36.TRAINING_ROTATION_COUNT) {
//			return 0.0001;
//		}
//		return 0.00001;
		return 0.005;
	}

	@Override
	public ISchedule clone() {
		return new ResNet50Scheduler();
	}
}
