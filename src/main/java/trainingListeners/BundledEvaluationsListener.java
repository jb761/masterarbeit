package trainingListeners;

import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.deeplearning4j.optimize.api.InvocationType;
import org.nd4j.evaluation.IEvaluation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

public class BundledEvaluationsListener  extends BaseTrainingListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(BundledEvaluationsListener.class);

	private final DataSetIterator DATASET_ITERATOR;
	private final DataSetType DATASET_TYPE;
	private final InvocationType INVOCATION_TYPE;
	private final int INTERVAL;
	private final IEvaluation[] EVALUATIONS;

	public BundledEvaluationsListener(InvocationType invocationType, DataSetType dataSetType, DataSetIterator datasetIterator,
												 int interval, IEvaluation... evaluations) {
		this.DATASET_ITERATOR = datasetIterator;
		this.DATASET_TYPE = dataSetType;
		this.INVOCATION_TYPE = invocationType;
		this.INTERVAL = interval;
		this.EVALUATIONS = evaluations;
	}

	@Override
	public void iterationDone(Model model, int iteration, int epoch) {
		if(INVOCATION_TYPE == InvocationType.ITERATION_END && iteration % INTERVAL == 0) {
			LOGGER.info(String.format("ITERATION_END: %d", iteration));
			invokeListener(model);
		}
	}

	@Override
	public void onEpochEnd(Model model) {
		if(INVOCATION_TYPE == InvocationType.EPOCH_END) {
			LOGGER.info("EPOCH_END:");
			invokeListener(model);
		}
	}

	private void invokeListener(Model model) {
		for(IEvaluation eval : EVALUATIONS) {
			eval.reset();
		}
		Stopwatch swDoEvaluation = new Stopwatch().start();
		((MultiLayerNetwork) model).doEvaluation(DATASET_ITERATOR, EVALUATIONS);
		swDoEvaluation.stop();
		for(IEvaluation eval : EVALUATIONS) {
			LOGGER.info(String.format("%s | %s | %s", DATASET_TYPE.name(), eval.getClass().getSimpleName(), eval.stats()));
		}
		LOGGER.info(String.format("(MultiLayerNetwork::doEvaluation: %d milliSec)", swDoEvaluation.get()));
	}
}
