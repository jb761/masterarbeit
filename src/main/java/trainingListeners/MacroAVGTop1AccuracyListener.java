package trainingListeners;

import evaluation.MacroAVGTop1AccuracyEvaluator;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.deeplearning4j.optimize.api.InvocationType;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

public class MacroAVGTop1AccuracyListener extends BaseTrainingListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(MacroAVGTop1AccuracyListener.class);

	private final int C;
	private final DataSetIterator DATASET_ITERATOR;
	private final int INTERVAL;
	private final DataSetType DATASET_TYPE;
	private final InvocationType INVOCATION_TYPE;

	public MacroAVGTop1AccuracyListener(int classes, int interval, DataSetIterator datasetIterator,
													DataSetType dataSetType, InvocationType invocationType) {
		this.C = classes;
		this.INTERVAL = interval;
		this.DATASET_ITERATOR = datasetIterator;
		this.DATASET_TYPE = dataSetType;
		this.INVOCATION_TYPE = invocationType;
	}

	@Override
	public void iterationDone(Model model, int iteration, int epoch) {
		if(INVOCATION_TYPE == InvocationType.ITERATION_END && iteration % INTERVAL == 0) {
			LOGGER.info(String.format("ITERATION_END: %d | %s", iteration, invokeListener(model)));
		}
	}

	@Override
	public void onEpochEnd(Model model) {
		if(INVOCATION_TYPE == InvocationType.EPOCH_END) {
			LOGGER.info("EPOCH_END : " + invokeListener(model));
		}
	}

	private String invokeListener(Model model) {
		Stopwatch sw = new Stopwatch().start();
		MacroAVGTop1AccuracyEvaluator evaluation = new MacroAVGTop1AccuracyEvaluator(C);
		if (model instanceof MultiLayerNetwork) {
			((MultiLayerNetwork) model).doEvaluation(DATASET_ITERATOR, evaluation);
		} else {
			throw new UnsupportedOperationException("Unsupported Model Type: " + model.getClass().getCanonicalName());
		}
		return String.format("%s | %s (%d milliSec)", DATASET_TYPE.name(), evaluation.stats(), sw.get());
	}
}
