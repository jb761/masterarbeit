package trainingListeners;

import datasetIterator.PlantNetImageProvider;
import netType.PlantNetType;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.NeuralNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

public class PlantNetDatasetRotation extends BaseTrainingListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetDatasetRotation.class);

	private final PlantNetImageProvider PLANTNET_IMAGE_PROVIDER;
	private final int TOTAL_TO_TRAINING_RATIO;
	private final int TOTAL_TO_TEST_RATIO;
	private final IEvaluation[] TRAINING_EVALUATIONS;
	private final IEvaluation[] TEST_EVALUATIONS;
	private PlantNetType plantNetType;
	private int trainingImagesSeen;
	private int epochCount;

	private Stopwatch makroSW;

	public PlantNetDatasetRotation(PlantNetImageProvider imageProvider, int totalToTrainingRatio, int totalToTestRatio, IEvaluation[] trainingListener, IEvaluation[] testListener) {
		this.PLANTNET_IMAGE_PROVIDER = imageProvider;
		this.TOTAL_TO_TRAINING_RATIO = totalToTrainingRatio;
		this.TOTAL_TO_TEST_RATIO = totalToTestRatio;
		this.TRAINING_EVALUATIONS = trainingListener;
		this.TEST_EVALUATIONS = testListener;
		plantNetType = imageProvider.getType();
		trainingImagesSeen = 0;
		epochCount = 0;
		makroSW = new Stopwatch().start();
	}

	@Override
	public void onEpochEnd(Model model) {
		if((TOTAL_TO_TRAINING_RATIO == 1) || ((epochCount+1) % TOTAL_TO_TRAINING_RATIO == 1)) {
			doTrainingEvaluation(model);
		}
		if((TOTAL_TO_TEST_RATIO == 1) ||((epochCount+1) % TOTAL_TO_TEST_RATIO == 1)) {
			doTestEvaluation(model);
		}

//		LOGGER.info(String.format("Model-score: %1.15f (Rotation: %d)", model.score(), epochCount));

		PLANTNET_IMAGE_PROVIDER.loadNextRotation(DataSetType.TRAIN);
		epochCount++;
		trainingImagesSeen += plantNetType.TRAINING_ROTATION_SIZE;
	}

	private void doTrainingEvaluation(Model model) {
		System.out.println("");
		LOGGER.info(String.format("EPOCH END: - %d -\t( training images seen: %d | time since lase evaluation: %1.3f Sec. )", epochCount, trainingImagesSeen, makroSW.get(1000)));
		Stopwatch sw = new Stopwatch().start();
		for(IEvaluation eval : TRAINING_EVALUATIONS) {
			eval.reset();
		}
		((NeuralNetwork) model).doEvaluation(PLANTNET_IMAGE_PROVIDER, TRAINING_EVALUATIONS);
		for(IEvaluation eval : TRAINING_EVALUATIONS) {
			LOGGER.info(String.format("TRAINING | %s | %s", eval.getClass().getSimpleName(), eval.stats()));
		}
		LOGGER.info(String.format("Time spend evaluating TRAINING: %1.3f Sec.", sw.get(1000)));
	}

	private void doTestEvaluation(Model model) {
		PLANTNET_IMAGE_PROVIDER.loadNextRotation(DataSetType.TEST);
		Stopwatch sw = new Stopwatch().start();
		for(IEvaluation eval : TEST_EVALUATIONS) {
			eval.reset();
		}
		((NeuralNetwork) model).doEvaluation(PLANTNET_IMAGE_PROVIDER, TEST_EVALUATIONS);
		for(IEvaluation eval : TEST_EVALUATIONS) {
			LOGGER.info(String.format("TEST | %s | %s", eval.getClass().getSimpleName(), eval.stats()));
		}
		LOGGER.info(String.format("Time spend evaluating TEST: %1.3f Sec.", sw.get(1000)));
	}
}
