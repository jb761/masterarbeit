package trainingListeners;

import computationGraphExtension.CustomComputationGraph;
import datasetIterator.PlantNetImageProviderD33;
import netType.D36;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.nd4j.evaluation.IEvaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

public class PlantNetDatasetRotationD33 extends BaseTrainingListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlantNetDatasetRotationD33.class);

	private final PlantNetImageProviderD33 PLANTNET_IMAGE_PROVIDER;
	private final int TOTAL_TO_TRAINING_RATIO;
	private final int TOTAL_TO_TEST_RATIO;
	private final IEvaluation[] TRAINING_EVALUATIONS;
	private final IEvaluation[] TEST_EVALUATIONS;
	private int trainingImagesSeen;
	private int epochCount;

	private Stopwatch makroSW;

	public PlantNetDatasetRotationD33(PlantNetImageProviderD33 imageProvider, int totalToTrainingRatio, int totalToTestRatio, IEvaluation[] trainingListener, IEvaluation[] testListener) {
		this.PLANTNET_IMAGE_PROVIDER = imageProvider;
		this.TOTAL_TO_TRAINING_RATIO = totalToTrainingRatio;
		this.TOTAL_TO_TEST_RATIO = totalToTestRatio;
		this.TRAINING_EVALUATIONS = trainingListener;
		this.TEST_EVALUATIONS = testListener;
		trainingImagesSeen = 0;
		epochCount = 0;
		makroSW = new Stopwatch().start();
	}

	@Override
	public void onEpochEnd(Model model) {
		if((TOTAL_TO_TRAINING_RATIO == 1) || ((epochCount+1) % TOTAL_TO_TRAINING_RATIO == 1)) {
			doTrainingEvaluation((CustomComputationGraph) model);
		}
		if((TOTAL_TO_TEST_RATIO == 1) ||((epochCount+1) % TOTAL_TO_TEST_RATIO == 1)) {
			doTestEvaluation((CustomComputationGraph) model);
		}

		PLANTNET_IMAGE_PROVIDER.loadNextRotation(DataSetType.TRAIN);
		epochCount++;
		trainingImagesSeen += D36.TRAINING_ROTATION_SIZE;
	}

	private void doTrainingEvaluation(CustomComputationGraph model) {
		System.out.println("");
		LOGGER.info(String.format("EPOCH END: - %d -\t( training images seen: %d | time since lase evaluation: %1.3f Sec. )", epochCount, trainingImagesSeen, makroSW.get(1000)));
		Stopwatch sw = new Stopwatch().start();
		for(IEvaluation eval : TRAINING_EVALUATIONS) {
			eval.reset();
		}
		model.doCustomEvaluation(PLANTNET_IMAGE_PROVIDER, TRAINING_EVALUATIONS);
		for(IEvaluation eval : TRAINING_EVALUATIONS) {
			LOGGER.info(String.format("TRAINING | %s | %s", eval.getClass().getSimpleName(), eval.stats()));
		}
		LOGGER.info(String.format("Time spend evaluating TRAINING: %1.3f Sec.", sw.get(1000)));
	}

	private void doTestEvaluation(CustomComputationGraph model) {
		PLANTNET_IMAGE_PROVIDER.loadNextRotation(DataSetType.TEST);
		Stopwatch sw = new Stopwatch().start();
		for(IEvaluation eval : TEST_EVALUATIONS) {
			eval.reset();
		}
		model.doCustomEvaluation(PLANTNET_IMAGE_PROVIDER, TEST_EVALUATIONS);
		for(IEvaluation eval : TEST_EVALUATIONS) {
			LOGGER.info(String.format("TEST | %s | %s", eval.getClass().getSimpleName(), eval.stats()));
		}
		LOGGER.info(String.format("Time spend evaluating TEST: %1.3f Sec.", sw.get(1000)));
	}
}
