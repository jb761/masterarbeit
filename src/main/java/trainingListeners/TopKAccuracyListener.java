package trainingListeners;

import evaluation.TopKAccuracyEvaluator;
import org.deeplearning4j.datasets.fetchers.DataSetType;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.deeplearning4j.optimize.api.InvocationType;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.Stopwatch;

public class TopKAccuracyListener extends BaseTrainingListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(TopKAccuracyListener.class);

	private final int K;
	private final int INTERVAL;
	private final DataSetIterator DATASET_ITERATOR;
	private final DataSetType DATASET_TYPE;
	private final InvocationType INVOCATION_TYPE;

	public TopKAccuracyListener(int k, int interval, DataSetIterator datasetIterator,
										 DataSetType dataSetType, InvocationType invocationType) {
		this.K = k;
		this.INTERVAL = interval;
		this.DATASET_ITERATOR = datasetIterator;
		this.DATASET_TYPE = dataSetType;
		this.INVOCATION_TYPE = invocationType;
	}

	@Override
	public void iterationDone(Model model, int iteration, int epoch) {
		if(INVOCATION_TYPE == InvocationType.ITERATION_END && iteration % INTERVAL == 0) {
			LOGGER.info(String.format("ITERATION_END: %d | %s", iteration, invokeListener(model)));
		}
	}

	@Override
	public void onEpochEnd(Model model) {
		if(INVOCATION_TYPE == InvocationType.EPOCH_END) {
			LOGGER.info("EPOCH_END : " + invokeListener(model));
		}
	}

	private String invokeListener(Model model) {
		Stopwatch sw = new Stopwatch().start();
		TopKAccuracyEvaluator evaluation = new TopKAccuracyEvaluator(K);
		if (model instanceof MultiLayerNetwork) {
				((MultiLayerNetwork) model).doEvaluation(DATASET_ITERATOR, evaluation);
		} else {
			throw new UnsupportedOperationException("Unsupported Model Type: " + model.getClass().getCanonicalName());
		}

		return String.format("%s | %s (%d milliSec)", DATASET_TYPE.name(), evaluation.stats(), sw.get());
	}
}
