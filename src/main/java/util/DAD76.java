package util;

import nu.pattern.OpenCV;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

public class DAD76 {

	public static final Logger LOGGER = LoggerFactory.getLogger(DAD76.class);

	public static List<ImageData> trainingImageList; // Contains all training Images
	public static List<ImageData> testImageList; // Contains all test images
	public static List<ImageData> validationImageList; // Contains all validation images

	private static List<String> labelsAt_6;
	private static List<String> labelsAt_5;
	private static List<String> labelsAt_4;
	private static List<String> labelsAt_3;
	private static List<String> labelsAt_2;
	private static List<String> labelsAt_1;

	public static int[] imageCountPerClass_6 = new int[16];
	public static int[] imageCountPerClass_5 = new int[29];
	public static int[] imageCountPerClass_4 = new int[59];
	public static int[] imageCountPerClass_3 = new int[113];
	public static int[] imageCountPerClass_2 = new int[224];
	public static int[] imageCountPerClass_1 = new int[1019];

	public static void init() {
		Stopwatch sw = new Stopwatch().start();
		LOGGER.info("--> Start initializing");
		Map<String, String> normPathToQualifiedNameMap = new HashMap<>();
		collectQualifiedNamesForPath(normPathToQualifiedNameMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized"));

		Map<String, int[]> qualifiedNameToClassAtLevel = new HashMap<>();
		createClasses(normPathToQualifiedNameMap, qualifiedNameToClassAtLevel);

		LOGGER.info(String.format("--> Classes created (%dms)", sw.get()));

		trainingImageList = new ArrayList<>();
		List<File> trainingFiles = new ArrayList<>();
		collectImageFiles(trainingFiles, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\_images_train"));
		for (File image : trainingFiles) {
			ImageData imageData = new ImageData(image);
			imageData.qualifiedName = createQualifiedNameFromPath(image.getAbsolutePath());
			imageData.classAtLevel = qualifiedNameToClassAtLevel.get(imageData.qualifiedName);
			trainingImageList.add(imageData);
		}
		LOGGER.info(String.format("Training Images Found: %d", trainingImageList.size()));

		testImageList = new ArrayList<>();
		List<File> testFiles = new ArrayList<>();
		collectImageFiles(testFiles, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\_images_test"));
		for (File image : testFiles) {
			ImageData imageData = new ImageData(image);
			imageData.qualifiedName = createQualifiedNameFromPath(image.getAbsolutePath());
			imageData.classAtLevel = qualifiedNameToClassAtLevel.get(imageData.qualifiedName);
			testImageList.add(imageData);
		}
		LOGGER.info(String.format("Test Images Found: %d", testImageList.size()));

		validationImageList = new ArrayList<>();
		List<File> validationFiles = new ArrayList<>();
		collectImageFiles(validationFiles, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\_images_val"));
		for (File image : validationFiles) {
			ImageData imageData = new ImageData(image);
			imageData.qualifiedName = createQualifiedNameFromPath(image.getAbsolutePath());
			imageData.classAtLevel = qualifiedNameToClassAtLevel.get(imageData.qualifiedName);
			validationImageList.add(imageData);
		}
		LOGGER.info(String.format("Validation Images Found: %d", validationImageList.size()));
		LOGGER.info(String.format("--> Images accounted (%dms)", sw.get()));

		countImages(trainingImageList);
		LOGGER.info(String.format("--> Training imbalance counted (%dms)", sw.get()));

		// Load  OpenCV
		OpenCV.loadShared();
		LOGGER.info(String.format("--> OpenCV libraries loaded (%dms)", sw.get()));
	}

	private static void collectQualifiedNamesForPath(Map<String, String> result, File root) {
		File[] childs = root.listFiles();
		if(childs.length==0) {
			result.put(root.getAbsolutePath(), createQualifiedNameFromFlatten(root.getAbsolutePath()));
		}else {
			for(File child : childs) {
				collectQualifiedNamesForPath(result, child);
			}
		}
	}

	private static void createClasses(Map<String, String> normPathToQualifiedNameMap, Map<String, int[]> result) {
		labelsAt_6 = new ArrayList<>();
		labelsAt_5 = new ArrayList<>();
		labelsAt_4 = new ArrayList<>();
		labelsAt_3 = new ArrayList<>();
		labelsAt_2 = new ArrayList<>();
		labelsAt_1 = new ArrayList<>();
		List<String> normPaths = new ArrayList<>(normPathToQualifiedNameMap.keySet());
		Collections.sort(normPaths);
		for(String path : normPaths) {
			String[] splitted = path.split("\\\\");
			int length = splitted.length;
			if(labelsAt_6.size() < 1 || !labelsAt_6.get(labelsAt_6.size()-1).equals(splitted[length-6])) {
				labelsAt_6.add(splitted[length-6]);
				labelsAt_5.add(splitted[length-5]);
				labelsAt_4.add(splitted[length-4]);
				labelsAt_3.add(splitted[length-3]);
				labelsAt_2.add(splitted[length-2]);
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
			if(labelsAt_5.size() < 1 || !labelsAt_5.get(labelsAt_5.size()-1).equals(splitted[length-5])) {
				labelsAt_5.add(splitted[length-5]);
				labelsAt_4.add(splitted[length-4]);
				labelsAt_3.add(splitted[length-3]);
				labelsAt_2.add(splitted[length-2]);
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
			if(labelsAt_4.size() < 1 || !labelsAt_4.get(labelsAt_4.size()-1).equals(splitted[length-4])) {
				labelsAt_4.add(splitted[length-4]);
				labelsAt_3.add(splitted[length-3]);
				labelsAt_2.add(splitted[length-2]);
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
			if(labelsAt_3.size() < 1 || !labelsAt_3.get(labelsAt_3.size()-1).equals(splitted[length-3])) {
				labelsAt_3.add(splitted[length-3]);
				labelsAt_2.add(splitted[length-2]);
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
			if(labelsAt_2.size() < 1 || !labelsAt_2.get(labelsAt_2.size()-1).equals(splitted[length-2])) {
				labelsAt_2.add(splitted[length-2]);
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
			if(labelsAt_1.size() < 1 || !labelsAt_1.get(labelsAt_1.size()-1).equals(splitted[length-1])) {
				labelsAt_1.add(splitted[length-1]);
				result.put(normPathToQualifiedNameMap.get(path), new int[]{
						labelsAt_6.size()-1,
						labelsAt_5.size()-1,
						labelsAt_4.size()-1,
						labelsAt_3.size()-1,
						labelsAt_2.size()-1,
						labelsAt_1.size()-1});
				continue;
			}
		}

		LOGGER.info("CLASSES FOUND:");
		LOGGER.info("EBENE 6: " + (labelsAt_6.size()));
		LOGGER.info("EBENE 5: " + (labelsAt_5.size()));
		LOGGER.info("EBENE 4: " + (labelsAt_4.size()));
		LOGGER.info("EBENE 3: " + (labelsAt_3.size()));
		LOGGER.info("EBENE 2: " + (labelsAt_2.size()));
		LOGGER.info("EBENE 1: " + (labelsAt_1.size()));
	}

	private static void collectImageFiles(List<File> result, File root) {
		for(File child : root.listFiles()) {
			if(child.isDirectory()) {
				collectImageFiles(result, child);
			}else if(child.getName().endsWith(".jpg")){
				result.add(child);
			}
		}
	}

	private static void countImages(List<ImageData> images) {
		for(ImageData image : images) {
			imageCountPerClass_6[image.classAtLevel[0]]++;
			imageCountPerClass_5[image.classAtLevel[1]]++;
			imageCountPerClass_4[image.classAtLevel[2]]++;
			imageCountPerClass_3[image.classAtLevel[3]]++;
			imageCountPerClass_2[image.classAtLevel[4]]++;
			imageCountPerClass_1[image.classAtLevel[5]]++;
		}
	}

	public static INDArray getBalancedLossWeights(int level, double baseWeight) {
		if(level==6) {return getBalancedLossWeights(imageCountPerClass_6, baseWeight);}
		if(level==5) {return getBalancedLossWeights(imageCountPerClass_5, baseWeight);}
		if(level==4) {return getBalancedLossWeights(imageCountPerClass_4, baseWeight);}
		if(level==3) {return getBalancedLossWeights(imageCountPerClass_3, baseWeight);}
		if(level==2) {return getBalancedLossWeights(imageCountPerClass_2, baseWeight);}
		if(level==1) {return getBalancedLossWeights(imageCountPerClass_1, baseWeight);}
		return null;
	}

	private static INDArray getBalancedLossWeights(int[] imageCount, double baseWeight) {
		double avg = 0;
		for(int i=0; i<imageCount.length; i++) {
			avg += (double) imageCount[i] / (double)imageCount.length;
		}
		float[] weights = new float[imageCount.length];
		for(int i=0; i<imageCount.length; i++) {
			weights[i] = (float) ((1.0 + (avg / (avg + imageCount[i]))) * baseWeight);
		}

//		double max = 0;
//		for(int i=0; i<imageCount.length; i++) {
//			if(imageCount[i] > max) {
//				max = imageCount[i];
//			}
//		}
//		System.out.println("AVG: " + avg + "\tMAX: " + max);

		return Nd4j.create(weights);
	}

	// Use for Dirs
	private static String createQualifiedNameFromFlatten(String path) {
		String[] splitted = path.replace("_", "\\").split("\\\\");
//		System.out.println("QUALIFIED\t" + String.format("%s___%s", splitted[splitted.length-2], splitted[splitted.length-1]));
		return String.format("%s___%s", splitted[splitted.length-2], splitted[splitted.length-1]);
	}

	// Use for image-Files
	private static String createQualifiedNameFromPath(String path) {
		String[] splitted = path.replace("_", "\\").split("\\\\");
//		System.out.println("QUALIFIED\t" + String.format("%s___%s", splitted[splitted.length-3], splitted[splitted.length-2]));
		return String.format("%s___%s", splitted[splitted.length-3], splitted[splitted.length-2]);
	}
}
