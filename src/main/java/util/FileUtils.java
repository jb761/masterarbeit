package util;

import java.io.*;

public class FileUtils {

	private FileUtils() {
		// do nothing.
	}

	public static Object readObjectFormFile(String path) throws Exception {
		File metaFile = new File(path);
		FileInputStream fis = new FileInputStream(metaFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		return ois.readObject();
	}
	public static <T> T readFormFile(String path) throws Exception {
		File metaFile = new File(path);
		FileInputStream fis = new FileInputStream(metaFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		return (T) ois.readObject();
	}

	public static void writeObjectToFile(String path, Object obj) throws Exception {
		File classFile = new File(path);
		FileOutputStream fos = new FileOutputStream(classFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(obj);
		oos.close();
	}
}
