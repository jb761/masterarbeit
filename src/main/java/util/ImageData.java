package util;

import head.Main;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ImageData {

	public static final Random random = new Random(Main.SEED);
	private static final ExecutorService threadPool = Executors.newFixedThreadPool(2);

	public final File FILE;
	public String qualifiedName;
	public int[] classAtLevel;

	public ImageData(File file) {
		this.FILE = file;
		this.classAtLevel = new int[6];
		for(int i=0; i<6; i++) {
			classAtLevel[i] = -1;
		}
	}

	public Future<float[]> getDataVector(boolean training) {
		return training? getDataVectorTraining() : getDataVectorTest();
	}

	private Future<float[]> getDataVectorTraining() {
		return threadPool.submit(() -> {

//			int RAN = (int) (10000 * Math.random());

			float[] result = new float[224 * 224 * 3];
			int rIndex = 0;
			Imgcodecs imageCodecs = new Imgcodecs();
			Mat matA = imageCodecs.imread(FILE.getAbsolutePath()); // read
			Mat matB = new Mat(); // rotate
			Mat matC = new Mat(); // resize
			Mat matD = new Mat(); // crop
			Mat matE = new Mat(); // contrast
			Mat matF = new Mat(); // brightness
			// Rotate
			Size size = matA.size();
			double alpha = -15.0 + 30.0 * random.nextDouble();
			double w = size.width;
			double h = size.height;
			double scale = (Math.cos((((Math.atan(h / w)) * 180.0 / Math.PI) - Math.abs(alpha)) * Math.PI / 180.0) * Math.sqrt(0.25 * w * w + 0.25 * h * h) / (0.5 * w));
			Mat rota = Imgproc.getRotationMatrix2D(new Point(w / 2, h / 2), alpha, scale);
			Imgproc.warpAffine(matA, matB, rota, new Size(w, h));
			// Resize
			Imgproc.resize(matB, matC, new Size(250, 250));
			// Crop
			int xRan = (int) (26 * random.nextDouble());
			int yRan = (int) (26 * random.nextDouble());
			matD = matC.submat(xRan, 224 + xRan, yRan, 224 + yRan);
			// Contrast
			Imgproc.cvtColor(matD, matE, Imgproc.COLOR_RGB2HSV);
			double c1 = 0.8 + 1.2 * random.nextDouble();
			double c2 = 0.8 + 1.2 * random.nextDouble();
			for (int x = 0; x < 224; x++) {
				for (int y = 0; y < 224; y++) {
					double[] hsv = matE.get(x, y);
					hsv[1] = between_0_255(hsv[1] * c1);
					hsv[2] = between_0_255(hsv[2] * c2);
					matE.put(x, y, hsv);
				}
			}
			Imgproc.cvtColor(matE, matF, Imgproc.COLOR_HSV2RGB);
			// Brightness
			double lum = -20 + 40.0 * random.nextDouble();
			for (int y = 0; y < 224; y++) {
				for (int x = 0; x < 224; x++) {
					double[] rgb = matF.get(x, y);
					result[rIndex + 0] = (((float) between_0_255(rgb[0] + lum)) / 255.0f) * 2.0f - 1.0f;
					result[rIndex + 50176] = (((float) between_0_255(rgb[1] + lum)) / 255.0f) * 2.0f - 1.0f;
					result[rIndex + 100352] = (((float) between_0_255(rgb[2] + lum)) / 255.0f) * 2.0f - 1.0f;
					rIndex++;
//					matF.put(x, y, new double[]{
//							between_0_255(rgb[0] + lum),
//							between_0_255(rgb[1] + lum),
//							between_0_255(rgb[2] + lum)
//					});
				}
			}

//			Mat matX = new Mat();
//			Imgproc.resize(matA, matX, new Size(224, 224));
//			imageCodecs.imwrite("E:\\TEST\\" + RAN + "_A.jpg", matX);
//			Mat matY = new Mat();
//			Imgproc.resize(matB, matY, new Size(224, 224));
//			imageCodecs.imwrite("E:\\TEST\\" + RAN + "_B.jpg", matY);
//			imageCodecs.imwrite("E:\\TEST\\" + RAN + "_D.jpg", matD);
//			Mat matZ = new Mat();
//			Imgproc.cvtColor(matE, matZ, Imgproc.COLOR_HSV2RGB);
//			imageCodecs.imwrite("E:\\TEST\\" + RAN + "_E.jpg", matZ);
//			imageCodecs.imwrite("E:\\TEST\\" + RAN + "_F.jpg", matF);

			return result;
		});
	}

	private Future<float[]> getDataVectorTest() {
		return threadPool.submit(() -> {
			float[] result = new float[224*224*3];
			int rIndex = 0;
			Imgcodecs imageCodecs = new Imgcodecs();
			Mat matA = imageCodecs.imread(FILE.getAbsolutePath()); // read
			Mat matB = new Mat(); // resize
			Mat matC = new Mat(); // crop
			// Resize
			Imgproc.resize(matA, matB, new Size(250, 250));
			matC = matB.submat(13, 237, 13, 237);
			for(int y=0; y<224; y++) {
				for(int x=0; x<224; x++) {
					double[] rgb = matC.get(x,y);
					result[rIndex + 0] = (((float) rgb[0] / 255.0f)) * 2.0f - 1.0f;
					result[rIndex + 50176] = (((float) rgb[1] / 255.0f)) * 2.0f - 1.0f;
					result[rIndex + 100352] = (((float) rgb[2] / 255.0f)) * 2.0f - 1.0f;
					rIndex++;
				}
			}
			return result;
		});
	}

	public float[] getClassVector() {
		float[] result = new float[1460]; // 16 + 29 + 59 + 113 + 224 + 1019
		result[classAtLevel[0]] = 1;
		result[16 + classAtLevel[1]] = 1;
		result[16 + 29 + classAtLevel[2]] = 1;
		result[16 + 29 + 59 + classAtLevel[3]] = 1;
		result[16 + 29 + 59 + 113 + classAtLevel[4]] = 1;
		result[16 + 29 + 59 + 113 + 224 + classAtLevel[5]] = 1;
		return result;
	}

	private double between_0_255(double value) {
		if(value > 255.0) {
			return 255.0;
		}else if(value < 0.0) {
			return 0.0;
		}else {
			return value;
		}
	}
}
