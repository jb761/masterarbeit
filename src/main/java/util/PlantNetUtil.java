package util;

import netType.PlantNetType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.List;

public class PlantNetUtil {

//	static String result = "";
//	static Map<String, List<String>> genusSpecToIDMap;
//
//	public static void transferDataset() throws Exception {
//		File namesJson = new File("E:\\PlantNet_Dataset\\plantnet_300K\\plantnet300K_species_names.json");
//		BufferedReader br = new BufferedReader(new FileReader(namesJson));
//		String json = br.readLine();
//		String[] pairsUnsplit = json.replace("{", "").replace("}", "").split(",");
//		Map<String, List<Map<String, String>>> nameToSubListToIdMap = new HashMap<>();
//		for(String pairUnsplit : pairsUnsplit) {
//			String[] pair = pairUnsplit.replace("\"", "").split(":");
//			String id = pair[0].trim();
//			String[] name = pair[1].trim().replace("_", " ").split(" ");
//			nameToSubListToIdMap
//					.computeIfAbsent(name[0], key -> new ArrayList<>())
//					.add(new HashMap() {{put(name[1], id);}});
//		}
//		for(String currDir : new String[]{"images_test", "images_train", "images_val"}) {
//			String baseTaxo = "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\"+currDir;
//			String baseRoot = "E:\\PlantNet_Dataset\\plantnet_300K\\"+currDir;
//			for(Map.Entry<String, List<Map<String, String>>> nameToSubListToId : nameToSubListToIdMap.entrySet()) {
//				File nameDir = new File(baseTaxo+"\\"+nameToSubListToId.getKey());
//				nameDir.mkdirs();
//				for(Map<String, String> subListToId : nameToSubListToId.getValue()) {
//					Map.Entry<String, String> sub = subListToId.entrySet().iterator().next();
//					File subDir = new File(baseTaxo+"\\"+nameToSubListToId.getKey()+"\\"+sub.getKey());
//					subDir.mkdirs();
//
//					File rootDir = new File(baseRoot+"\\"+sub.getValue());
//					for(File img : rootDir.listFiles()) {
//						File copyImg = new File(baseTaxo+"\\"+nameToSubListToId.getKey()+"\\"+sub.getKey()+"\\"+img.getName());
//						Files.copy(img.toPath(), copyImg.toPath());
//					}
//				}
//			}
//		}
//	}
//
//	public static void buildEntireTaxoTree() throws IOException {
//		Map<String, String> genSpecToTaxoTreeMap = new HashMap<>();
//		search(genSpecToTaxoTreeMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\_images_test"));
//		for(String currDir : new String[]{"images_train", "images_val"}) {
//			String baseTree = "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\_"+currDir;
//			String baseRoot = "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\"+currDir;
//			for(File genus : new File(baseRoot).listFiles()) {
//				for(File spec : genus.listFiles()) {
//					String key = genus.getName() + "/" + spec.getName();
//					String path = genSpecToTaxoTreeMap.get(key);
//					for(File img : spec.listFiles()) {
//						File copyImg = new File(baseTree+"\\"+path+"\\"+img.getName());
//						copyImg.getParentFile().mkdirs();
//						Files.copy(img.toPath(), copyImg.toPath());
//					}
//				}
////				System.out.println("-> " + Stopwatch.sGet());
//			}
////			System.out.println("currDir -> " + Stopwatch.sTotal());
//		}
//	}
//
//	public static void createMap() throws IOException {
//		System.out.println("\n");
//		Map<String, String> genusSpecToTaxoTreeMap = new HashMap<>();
//		search(genusSpecToTaxoTreeMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\_images_test"));
//		genusSpecToIDMap = new HashMap<>();
//		File namesJson = new File("E:\\PlantNet_Dataset\\plantnet_300K\\plantnet300K_species_names.json");
//		BufferedReader br = new BufferedReader(new FileReader(namesJson));
//		String json = br.readLine();
//		String[] pairsUnsplit = json.replace("{", "").replace("}", "").split(",");
//		for(String pairUnsplit : pairsUnsplit) {
//			String[] pair = pairUnsplit.replace("\"", "").split(":");
//			String id = pair[0].trim();
//			String name = pair[1].replace("_x_", "_").replace(" ", "").replace("_", "/").replace(".", "");
//			genusSpecToIDMap
//					.computeIfAbsent(name, key -> new ArrayList<>())
//					.add(id);
//		}
//		latexAdd(new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\_images_test\\Tracheophytes"));
//
//		System.out.println(result + "\n");
//	}
//
//	private static int latexAdd(File parent) {
//		int size = 1;
//		result += "child{node{" + parent.getName().replace("_", "$\\_$") + "}";
//		int imageCount = 0;
//		for(File child : parent.listFiles()) {
//			if(child.isDirectory()) {
//				size += latexAdd(child);
//			}else {
//				imageCount++;
//			}
//		}
//		if(imageCount > 0) {
//			result = result.substring(0, result.length()-1) + " ("; // remove "...{"
//			for(String id : genusSpecToIDMap.get(parent.getParentFile().getName() + "/" + parent.getName())) {
//				result += id + ", ";
//			}
//			result = result.substring(0, result.length()-2) + ")}"; // remove "..., "
//			result += "}";
//		}else {
//			result += "}";
//			for(int i=0; i<size-1; i++) {
//				result += "child[missing]{}";
////				result += "child{node{"+size+"}}";
//			}
//		}
//		return size;
//	}
//
//	private static void search(Map<String, String> map, File parent) {
//		int imageCount = 0;
//		int dirCount = 0;
//		for(File child : parent.listFiles()) {
//			if(child.isDirectory()) {
//				dirCount++;
//				search(map, child);
//			} else {
//				imageCount++;
//			}
//		}
//		if(imageCount > 0 && dirCount > 0) {
//			System.out.println("warn: " + parent.getParentFile().getName() + "/" + parent.getName());
//		}
//		if(imageCount > 0) {
//			String[] dirs = parent.getAbsolutePath().split("\\\\");
//			String dir = "";
//			for(int i=4; i<dirs.length; i++) {
//				dir += dirs[i] + "\\";
//			}
//			dir = dir.substring(0, dir.length()-1);
//			map.put(parent.getParentFile().getName() + "/" + parent.getName(), dir);
//		}
//	}
//
//	public static void createResizedCopies(String path, int X, int Y) throws IOException {
//		Stopwatch sw = new Stopwatch().start();
//		List<String> filePaths = new ArrayList<>();
//		File root = new File(path);
//		for(File child : root.listFiles()) {
//			if(child.isDirectory()) {
//				System.out.println(child.getAbsolutePath());
//				addRecursive(filePaths, child);
//			} else {
//				filePaths.add(child.getAbsolutePath());
//			}
//		}
//
//		System.out.println("search: " + sw.get());
//		int images = 0;
//		for(String imgPath : filePaths) {
//			if(imgPath.endsWith(".jpg")) {
//				File f = new File(imgPath);
//				String fileName = f.getAbsolutePath().split("\\.")[0] + "_R"+(int)X+"x"+(int)Y+".jpg";
//				BufferedImage img = ImageIO.read(f);
//				Image resultingImage = img.getScaledInstance(X, Y, Image.SCALE_SMOOTH);
//				BufferedImage out = new BufferedImage(X, Y, img.getType());
//				out.getGraphics().drawImage(resultingImage, 0, 0, null);
//				ImageIO.write(out, "jpg", new File(fileName));
//				images++;
//			}
//		}
//
//		System.out.println("create (" + images + "): " + sw.get());
//
//		for(String imgPath : filePaths) {
//			File f = new File(imgPath);
//			f.delete();
//		}
//
//		System.out.println("total (" + images + "): " + sw.get());
//	}
//
//	private static void addRecursive(List<String> target, File dir) {
//		for(File child : dir.listFiles()) {
//			if(child.isDirectory()) {
//				addRecursive(target, child);
//				System.out.println(child.getAbsolutePath());
//			} else {
//				target.add(child.getAbsolutePath());
//			}
//		}
//	}

	public static void createRAWImageData() throws IOException {
		PlantNetType d11 = PlantNetType.D11;
		Stopwatch sw;
		Stopwatch swTest = new Stopwatch().start();
		Map<Integer , Map<Integer, String>> testRotationToIndexOnRotationToFileNameMap = new HashMap<>();
		Map<Integer , Map<Integer, String>> trainingRotationToIndexOnRotationToFileNameMap = new HashMap<>();
		for(int r=0; r<d11.TEST_ROTATION_COUNT; r++) {
			sw = new Stopwatch().start();
			testRotationToIndexOnRotationToFileNameMap.put(r, createSingleRaw(r, "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test", d11.TEST_ROTATION_COUNT, d11.RAW_DATA_PATH_TEST));
			System.out.println(String.format("<createSingleRaw> TEST %d of %d, done within %1.3f Sec.", r, d11.TEST_ROTATION_COUNT, sw.get()/1000.0));
		}
		System.out.println(String.format("Done Test on %1.3f Sec.", swTest.get()/1000.0));
		Stopwatch swTrain = new Stopwatch().start();
		for(int r = 0; r<d11.TRAINING_ROTATION_COUNT; r++) {
			sw = new Stopwatch().start();
			trainingRotationToIndexOnRotationToFileNameMap.put(r, createSingleRaw(r, "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train", d11.TRAINING_ROTATION_COUNT, d11.RAW_DATA_PATH_TRAINING));
			System.out.println(String.format("<createSingleRaw> TRAINING %d of %d, done within %1.3f Sec.", r, d11.TRAINING_ROTATION_COUNT, sw.get()/1000.0));
		}
		System.out.println(String.format("Done Train on %1.3f Sec.", swTrain.get()/1000.0));

		System.out.println("Saving Maps");
		File indexForTest = new File( "E:\\PlantNet_Dataset\\testRotationToIndexOnRotationToFileNameMap.raw");
		FileOutputStream fos = new FileOutputStream(indexForTest);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(testRotationToIndexOnRotationToFileNameMap);
		oos.close();

		File indexForTraining = new File( "E:\\PlantNet_Dataset\\trainingRotationToIndexOnRotationToFileNameMap.raw");
		fos = new FileOutputStream(indexForTraining);
		oos = new ObjectOutputStream(fos);
		oos.writeObject(trainingRotationToIndexOnRotationToFileNameMap);
		oos.close();
	}

	private static Map<Integer, String> createSingleRaw(int rotation, String path, int maxPartitionCount, String rawPath) throws IOException {
		int X = 224;
		int Y = 224;
		Map<Integer, String> result = new HashMap<>();

		File root = new File(path);
		List<String> imgPaths = new ArrayList<>();
		addImagesRecursive(imgPaths, root, 0, rotation, maxPartitionCount);

		int imageCount = imgPaths.size();
		System.out.println("ImageCount: " + imageCount);

		float[][] images = new float[imageCount][X*Y*3];
		String[] fileNames = new String[imageCount];
		List<Integer> indexList = new ArrayList<>();
		int index = 0;
//		Stopwatch sw = new Stopwatch().start();
		for(String imgPath : imgPaths) {
			String[] split = imgPath.split("\\\\");
			fileNames[index] = split[split.length-1].replace(".jpg", "");

			BufferedImage orgImg = ImageIO.read(new File(imgPath));
			Image resultingImage = orgImg.getScaledInstance(X, Y, Image.SCALE_SMOOTH);
			BufferedImage out = new BufferedImage(X, Y, orgImg.getType());
			out.getGraphics().drawImage(resultingImage, 0, 0, null);
			Raster img = out.getData();

			int imgIndex = 0;
			int[] p = new int[3]; // TODO
			float[] R = new float[X * Y];
			float[] G = new float[X * Y];
			float[] B = new float[X * Y];
			int i = 0;
			for (int x = 0; x<X; x++) {
				for (int y = 0; y<Y; y++) {
					img.getPixel(x, y, p);
					R[i] = ((p[0] / 255.0f) * 2.0f - 1.0f);
					G[i] = ((p[1] / 255.0f) * 2.0f - 1.0f);
					B[i] = ((p[2] / 255.0f) * 2.0f - 1.0f);
//					images[index][imgIndex++] = ((p[0] / 255.0f) * 2.0f - 1.0f);
//					images[index][imgIndex++] = ((p[1] / 255.0f) * 2.0f - 1.0f);
//					images[index][imgIndex++] = ((p[2] / 255.0f) * 2.0f - 1.0f);
					i++;
				}
			}
			for(int r=0; r<R.length; r++) {
				images[index][imgIndex++] = R[r];
			}
			for(int g=0; g<G.length; g++) {
				images[index][imgIndex++] = G[g];
			}
			for(int b=0; b<B.length; b++) {
				images[index][imgIndex++] = B[b];
			}

			indexList.add(index++);
		}

		Collections.shuffle(indexList, new Random(9875732));
		float[][] imagesShuffled = new float[imageCount][X*Y*3];
		int j = 0;
		for(Integer i : indexList) {
			imagesShuffled[j] = images[i];
			result.put(j, fileNames[i]);
			j++;
		}

		File rawFile = new File(rawPath + "\\raw_" + rotation + ".raw");
		FileOutputStream fos = new FileOutputStream(rawFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(imagesShuffled);
		oos.close();

		return result;
	}

	private static int addImagesRecursive(List<String> target, File node, int current, int r, int R) {
		for(File child : node.listFiles()) {
			if(child.isDirectory()) {
				current = addImagesRecursive(target, child, current, r, R);
			}else {
				if(child.getAbsolutePath().endsWith(".jpg")) {
					if(current % R == r) {
						target.add(child.getAbsolutePath());
					}
					current++;
				}
			}
		}
		return current;
	}

	//-------------------------------------------------------------------------------------------------------------------

	public static void createD11ClassRAW() throws Exception {
		PlantNetType d11 = PlantNetType.D11;

		File metaFile = new File("E:\\PlantNet_Dataset\\testRotationToIndexOnRotationToFileNameMap.raw");
		FileInputStream fis = new FileInputStream(metaFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object metaObj = ois.readObject();
		Map<Integer, Map<Integer, String>> meta = (Map<Integer, Map<Integer, String>>) metaObj;
		Map<String, Integer> classes = new HashMap<>();
		List<String> classLabels = new ArrayList<>();
		addD11ClassesRecursive(classes, classLabels, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test"), "", 0);
		System.out.println("CLASS_COUNT:\t" + classLabels.size());
		Map<String, String> imageNamesToPathMap = new HashMap<>();
		addD11ImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d11.TEST_ROTATION_SIZE][classes.size()];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()][pathToD11ClassIndex(imageNamesToPathMap.get(entry.getValue()), classes)] = 1.0f;
			}
			File classFile = new File(String.format("%s\\classes_%d.raw", d11.RAW_CLASSES_PATH_TEST, rotationEntry.getKey()));
			FileOutputStream fos = new FileOutputStream(classFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(classValues);
			oos.close();
		}
		File labelFile = new File(String.format("%s\\labelList.raw", d11.RAW_CLASSES_PATH_TEST));
		FileOutputStream fos = new FileOutputStream(labelFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(classLabels);
		oos.close();

		System.out.println("test done");

		metaFile = new File("E:\\PlantNet_Dataset\\trainingRotationToIndexOnRotationToFileNameMap.raw");
		fis = new FileInputStream(metaFile);
		ois = new ObjectInputStream(fis);
		metaObj = ois.readObject();
		meta = (Map<Integer, Map<Integer, String>>) metaObj;
//		Map<String, Integer> classes = new HashMap<>();
//		addD11ClassesRecursive(classes, root, "", 0);
		imageNamesToPathMap = new HashMap<>();
		addD11ImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d11.TRAINING_ROTATION_SIZE][classes.size()];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()][pathToD11ClassIndex(imageNamesToPathMap.get(entry.getValue()), classes)] = 1.0f;
			}
			File classFile = new File(String.format("%s\\classes_%d.raw", d11.RAW_CLASSES_PATH_TRAINING, rotationEntry.getKey()));
			fos = new FileOutputStream(classFile);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(classValues);
			oos.close();
		}
		labelFile = new File(String.format("%s\\labelList.raw", d11.RAW_CLASSES_PATH_TRAINING));
		fos = new FileOutputStream(labelFile);
		oos = new ObjectOutputStream(fos);
		oos.writeObject(classLabels);
		oos.close();

		System.out.println("training done");
	}

	private static int addD11ClassesRecursive(Map<String, Integer> target, List<String> nameList, File node, String root, int index) {
		for(File child : node.listFiles()) {
			if(child.isDirectory()) {
				if(root.isEmpty()) {
					index = addD11ClassesRecursive(target, nameList, child, child.getName(), index);
				}else {
					target.put(root + "_" + child.getName(), index++);
					nameList.add(root + "_" + child.getName());
				}
			}else {
				// do nothing.
			}
		}
		return index;
	}

	private static void addD11ImagesRecursive(Map<String, String> target, File node) {
		for(File child : node.listFiles()) {
			if(child.isDirectory()) {
				addD11ImagesRecursive(target, child);
			}else {
				target.put(child.getName().replace(".jpg", ""), child.getAbsolutePath());
			}
		}
	}

	private static int pathToD11ClassIndex(String path, Map<String, Integer> classes) {
		String[] split = path.split("\\\\");
		int l = split.length;
		return classes.get(String.format("%s_%s", split[l-3], split[l-2]));
	}

	//-------------------------------------------------------------------------------------------------------------------

	public static void mergeValidationIntoTestD11() throws IOException {
		String testPath = "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test";
		String validationPath = "E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_val";

		Stopwatch sw = new Stopwatch();
		for(File gen : new File(validationPath).listFiles()) {
			for(File spec : gen.listFiles()) {
				String testGenSpec = String.format("%s\\%s\\%s\\", testPath, gen.getName(), spec.getName());
				for(File valImg : spec.listFiles()) {
					File testImg = new File(String.format("%sval_%s", testGenSpec, valImg.getName()));
					Files.copy(valImg.toPath(), testImg.toPath());
				}
			}
		}
		System.out.println("Done in " + sw.get(1000) + " Sec.");
	}

	public static void findDoubledNamesInMetaData() throws IOException {
		File namesJson = new File("E:\\PlantNet_Dataset\\plantnet_300K\\plantnet300K_species_names.json");
		BufferedReader br = new BufferedReader(new FileReader(namesJson));
		String json = br.readLine();
		String[] pairsUnsplit = json.replace("{", "").replace("}", "").split(",");
		Set<String> set = new HashSet<>();
		List<String> dupplicats = new ArrayList<>();
		for(String pairUnsplit : pairsUnsplit) {
			String[] pair = pairUnsplit.replace("\"", "").split(":");
			String name = pair[1].trim().split("_")[0];
			set.add(name);
		}
		System.out.println(set.size());
	}

	public static void crawlD11Log() throws IOException {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\AlexNetD11.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		int imageCount = 0;
		String train = "count,top1Acc,top5Acc,macroAVG\n";
		String test = "count,top1Acc,top5Acc,macroAVG\n";
		String line;
		crawl: while((line=reader.readLine())!=null) {
			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TOTAL TIME")) {
				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TRAINING | TopKAccuracyEvaluator | Top-1-Accuracy:")) {
				train += imageCount + "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TRAINING | TopKAccuracyEvaluator | Top-5-Accuracy:")) {
				train += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TRAINING | MacroAVGTop1AccuracyEvaluator")) {
				train += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".") + "\n";
				continue crawl;
			}
			if(line.contains("TEST | TopKAccuracyEvaluator | Top-1-Accuracy:")) {
				test += imageCount + "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TEST | TopKAccuracyEvaluator | Top-5-Accuracy:")) {
				test += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TEST | MacroAVGTop1AccuracyEvaluator")) {
				test += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".") + "\n";
				continue crawl;
			}
		}

		System.out.println("TRAIN");
		System.out.println(train);
		System.out.println("\nTEST");
		System.out.println(test);
	}

	//-------------------------------------------------------------------------------------------------------------------

	public static void createD12ClassRAW() throws Exception {
		PlantNetType d12 = PlantNetType.D12;

		File metaFile = new File("E:\\PlantNet_Dataset\\testRotationToIndexOnRotationToFileNameMap.raw");
		FileInputStream fis = new FileInputStream(metaFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		Object metaObj = ois.readObject();
		Map<Integer, Map<Integer, String>> meta = (Map<Integer, Map<Integer, String>>) metaObj;
		Map<String, Integer> classes = new HashMap<>();
		List<String> classLabels = new ArrayList<>();
		addD12ClassesRecursive(classes, classLabels, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test"), "", 0);
		System.out.println("CLASS_COUNT:\t" + classLabels.size());
		Map<String, String> imageNamesToPathMap = new HashMap<>();
		addD11ImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d12.TEST_ROTATION_SIZE][classes.size()];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()][pathToD12ClassIndex(imageNamesToPathMap.get(entry.getValue()), classes)] = 1.0f;
			}
			File classFile = new File(String.format("%s\\classes_%d.raw", d12.RAW_CLASSES_PATH_TEST, rotationEntry.getKey()));
			FileOutputStream fos = new FileOutputStream(classFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(classValues);
			oos.close();
		}
		File labelFile = new File(String.format("%s\\labelList.raw", d12.RAW_CLASSES_PATH_TEST));
		FileOutputStream fos = new FileOutputStream(labelFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(classLabels);
		oos.close();

		System.out.println("test done");

		metaFile = new File("E:\\PlantNet_Dataset\\trainingRotationToIndexOnRotationToFileNameMap.raw");
		fis = new FileInputStream(metaFile);
		ois = new ObjectInputStream(fis);
		metaObj = ois.readObject();
		meta = (Map<Integer, Map<Integer, String>>) metaObj;
//		Map<String, Integer> classes = new HashMap<>();
//		addD11ClassesRecursive(classes, root, "", 0);
		imageNamesToPathMap = new HashMap<>();
		addD11ImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d12.TRAINING_ROTATION_SIZE][classes.size()];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()][pathToD12ClassIndex(imageNamesToPathMap.get(entry.getValue()), classes)] = 1.0f;
			}
			File classFile = new File(String.format("%s\\classes_%d.raw", d12.RAW_CLASSES_PATH_TRAINING, rotationEntry.getKey()));
			fos = new FileOutputStream(classFile);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(classValues);
			oos.close();
		}
		labelFile = new File(String.format("%s\\labelList.raw", d12.RAW_CLASSES_PATH_TRAINING));
		fos = new FileOutputStream(labelFile);
		oos = new ObjectOutputStream(fos);
		oos.writeObject(classLabels);
		oos.close();

		System.out.println("training done\n");

		for(String label : classLabels) {
			System.out.println(label);
		}
		System.out.println(classLabels.size() + " - " + classes.size());
	}

	private static int addD12ClassesRecursive(Map<String, Integer> target, List<String> nameList, File node, String root, int index) {
		for(File child : node.listFiles()) {
			if(child.isDirectory()) {
				if(root.isEmpty()) {
					index = addD12ClassesRecursive(target, nameList, child, child.getName(), index);
				}else {
					if(!target.containsKey(root)) {
						target.put(root, index++);
						nameList.add(root);
					}
				}
			}else {
				// do nothing.
			}
		}
		return index;
	}

	private static int pathToD12ClassIndex(String path, Map<String, Integer> classes) {
		String[] split = path.split("\\\\");
		int l = split.length;
		return classes.get(split[l-3]);
	}

	public static void crawlD12Log() throws IOException {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\AlexNetD12.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		int imageCount = 0;
		String train = "count,top1Acc,top5Acc,macroAVG\n";
		String test = "count,top1Acc,top5Acc,macroAVG\n";
		String line;
		crawl: while((line=reader.readLine())!=null) {
			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TOTAL TIME")) {
				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TRAINING | TopKAccuracyEvaluator | Top-1-Accuracy:")) {
				train += imageCount + "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TRAINING | TopKAccuracyEvaluator | Top-5-Accuracy:")) {
				train += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TRAINING | MacroAVGTop1AccuracyEvaluator")) {
				train += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".") + "\n";
				continue crawl;
			}
			if(line.contains("TEST | TopKAccuracyEvaluator | Top-1-Accuracy:")) {
				test += imageCount + "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TEST | TopKAccuracyEvaluator | Top-5-Accuracy:")) {
				test += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".");
				continue crawl;
			}
			if(line.contains("TEST | MacroAVGTop1AccuracyEvaluator")) {
				test += "," + line.split(":")[3].trim().replace("%", "").replace(",", ".") + "\n";
				continue crawl;
			}
		}

		System.out.println("TRAIN");
		System.out.println(train);
		System.out.println("\nTEST");
		System.out.println(test);
	}

	//-------------------------------------------------------------------------------------------------------------------

	public static void removeImagesFromDirRec() {
		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_origin");
		removeImagesRecursive(root);
	}

	private static void removeImagesRecursive(File root) {
		for(File child : root.listFiles()) {
			if(child.isDirectory()) {
				removeImagesRecursive(child);
			}else {
				child.delete();
			}
		}
	}

	public static void createFlattenTaxoTree() {
		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_origin");
		Set<String> classSet = new HashSet<>();
		Map<String, String> ffDirs = new HashMap<>();
		fillDepthToFileNameListMap(classSet, ffDirs, root);
		Set<String> helper = new HashSet<>();
		for(Map.Entry<String, String> ffEntry : ffDirs.entrySet()) {
			for(String classPath : classSet) {
				helper.add(classPath.replace(ffEntry.getKey(), ffEntry.getValue()));
			}
			classSet = new HashSet<>(helper);
			helper.clear();
		}
		for (String classPath : classSet) {
			helper.add(classPath.replace("taxoTree_origin", "taxoTree_flatten"));
		}
		for (String classPath : helper) {
			new File(classPath).mkdirs();
		}
	}

	private static void fillDepthToFileNameListMap(Set<String> result, Map<String, String> ffDirs, File root) {
		File[] childs = root.listFiles();
		if(childs.length == 0) {
					result.add(root.getAbsolutePath());
		} else if(childs.length == 1) {
			ffDirs.put(
					String.format("%s\\%s", root.getName(), childs[0].getName()),
					String.format("%s_%s", root.getName(), childs[0].getName()));
			fillDepthToFileNameListMap(result, ffDirs, childs[0]); // removes FF-Classes
		} else {
			for(File child : childs) {
				fillDepthToFileNameListMap(result, ffDirs, child); // removes FF-Classes
			}
		}
	}

	public static void analyseD2X() {
//		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_flatten\\Tracheophytes");
//		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_origin");
		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		Map<Integer, List<String>> classMapByLevel = new HashMap<>();
		Map<Integer, Set<String>> extended = new HashMap<>();
		Map<Integer, List<String>> abs = new HashMap<>();
		fillClassMapPerLevel(classMapByLevel, extended, abs, root, 0);
		for(Map.Entry<Integer, List<String>> entry : classMapByLevel.entrySet()) {
			System.out.println(entry.getKey() + "\t-->\t" + entry.getValue().size());
		}
		System.out.println("");
		for(Map.Entry<Integer, Set<String>> entry : extended.entrySet()) {
			System.out.println(entry.getKey() + "\t-->\t" + entry.getValue().size());
		}

		for(Map.Entry<Integer, Set<String>> entry : extended.entrySet()) {
			for(Map.Entry<Integer, Set<String>> control : extended.entrySet()) {
				if(!entry.getKey().equals(control.getKey())) {
					for(String eString : entry.getValue()) {
						if(control.getValue().contains(eString)) {
							System.out.println(">> " + eString + "\t(" + control.getKey() + ")");
						}
					}
				}
			}
		}

		System.out.println("\n");

//		for(String path : abs.get(5)) {
//			System.out.println(path);
//		}
	}

	private static void fillClassMapPerLevel(Map<Integer, List<String>> result, Map<Integer, Set<String>> extended, Map<Integer, List<String>> abs, File root, int depth) {
		File[] children = root.listFiles();
		if(children.length == 0) {
			String absPath = root.getAbsolutePath();
			result.computeIfAbsent(depth, key -> new ArrayList<>())
					.add(pathToSubPath(absPath, depth));

			abs.computeIfAbsent(5, key -> new ArrayList<>())
					.add(pathToSubPath(absPath, 5));

			extended.computeIfAbsent(0, key -> new HashSet<>())
					.add(pathToParticle(absPath, 0));
			extended.computeIfAbsent(1, key -> new HashSet<>())
					.add(pathToParticle(absPath, 1));
			extended.computeIfAbsent(2, key -> new HashSet<>())
					.add(pathToParticle(absPath, 2));
			extended.computeIfAbsent(3, key -> new HashSet<>())
					.add(pathToParticle(absPath, 3));
			extended.computeIfAbsent(4, key -> new HashSet<>())
					.add(pathToParticle(absPath, 4));
			extended.computeIfAbsent(5, key -> new HashSet<>())
					.add(pathToParticle(absPath, 5));
		}else {
			for(File child : children) {
				fillClassMapPerLevel(result, extended, abs, child, depth+1);
			}
		}
	}

	private static String pathToSubPath(String path, int depth) {
		String[] splitted = path.split("\\\\");
		String result = "";
		for(int i=splitted.length-depth; i<splitted.length; i++) {
			result += "\\" + splitted[i];
		}
		return result;
	}

	private static String pathToParticle(String path, int depth) {
		String[] splitted = path.split("\\\\");
		return splitted[splitted.length-depth-1];
	}

	public static void normalizeD2X() {
		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		normalizeDirDepth(root);
	}

	private static void normalizeDirDepth(File root){
		int maxDepth = -1;
		File[] childs = root.listFiles();
		int[] depths = new int[childs.length];
		for(int i=0; i<childs.length; i++) {
			depths[i] = getDepths(childs[i]);
			if(depths[i] > maxDepth) {
				maxDepth = depths[i];
			}
		}
		boolean didRebase = false;
		for(int i=0; i<childs.length; i++) {
			if(depths[i] < maxDepth) {
				rebase(childs[i].getAbsolutePath(), String.format("%s\\node\\%s", childs[i].getParentFile().getAbsolutePath(), childs[i].getName()));
				didRebase = true;
			}
		}
		if(didRebase) {
			normalizeDirDepth(root);
		}else {
			for(File child : root.listFiles()) {
				normalizeDirDepth(child);
			}
		}
	}

	private static int getDepths(File root) {
		File[] childs = root.listFiles();
		if(childs.length == 0) {
			return 1;
		} else {
			int maxDepth = 0;
			for(File child : childs) {
				int depth = getDepths(child);
				if(depth > maxDepth) {
					maxDepth = depth;
				}
			}
			return 1 + maxDepth;
		}
	}

	private static void rebase(String oldPath, String newPath) {
		List<String> dirs = new ArrayList<>();
		File root = new File(oldPath);
		listAllDirsRec(dirs, root);
		List<String> rebasedPaths = new ArrayList<>();
		for(String dir : dirs) {
			rebasedPaths.add(dir.replace(oldPath, newPath));
		}
		deleteRec(root);
		for(String path : rebasedPaths) {
			new File(path).mkdirs();
		}
	}

	private static void listAllDirsRec(List<String> result, File root) {
		File[] childs = root.listFiles();
		if(childs.length == 0) {
			result.add(root.getAbsolutePath());
		}else {
			for(File child : childs) {
				listAllDirsRec(result, child);
			}
		}
	}

	private static void deleteRec(File root) {
		for(File child : root.listFiles()) {
			deleteRec(child);
		}
		root.delete();
	}

	public static void createAppendixFromNormalized() throws IOException {
		File namesJson = new File("E:\\PlantNet_Dataset\\plantnet_300K\\plantnet300K_species_names.json");
		Map<String, List<String>> json = new HashMap<>();
		BufferedReader br = new BufferedReader(new FileReader(namesJson));
		String jsonString = br.readLine();
		String[] pairsUnsplit = jsonString.replace("{", "").replace("}", "").split(",");
		for(String pairUnsplit : pairsUnsplit) {
			String[] splitt = pairUnsplit.split(": ");
			json.computeIfAbsent(splitt[1]
							.replace("_x_", "_")
							.replace(".", "")
							.replace("_", "___")
							.replace("\"", "")
					, key -> new ArrayList<>())
					.add(splitt[0]
							.replace("\"", ""));
		}

		Map<Integer, Integer> nodeCountMap = new HashMap<>();

//		for(String key : json.keySet()) {
//			System.out.println(">> " + key + "\t\t" + json.get(key).size());
//		}

		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		String[] table = new String[]{"\\begin{tabular}{|c|c|c|c|c|c|}\n\\hline\n"};
		appendAppendix(new int[]{0}, table, new String[]{"A\\A\\A\\A\\A\\A\\A\\A\\A\\A\\A\\A\\A\\A\\A"}, json, nodeCountMap, root);
		table[0] += "\\end{tabular}";
		System.out.println(table[0]);

		System.out.println("\nnodeCountMap");
		for(Map.Entry<Integer, Integer> entry : nodeCountMap.entrySet()) {
			System.out.println(entry.getKey() + " -> " + entry.getValue());
		}
	}

	private static void appendAppendix(int[] line, String result[], String[] lastLine, Map<String, List<String>> json,
												  Map<Integer, Integer> nodeCountMap, File root) {
		File[] childs = root.listFiles();
		if(childs.length == 0) {
			String[] last = pathToSubPath(lastLine[0], 6).split("\\\\");
			String[] current = pathToSubPath(root.getAbsolutePath(), 6).split("\\\\");
			result[0] += line[0]++ + ". & ";
			int difFound = 100;
			for(int i=1; i<6; i++) {
				if(!last[i].equals(current[i]) || i>= difFound) {
					result[0] += current[i].replace("_", "\\newline$\\_$");
					difFound = i;
					nodeCountMap.replace(i, nodeCountMap.computeIfAbsent(i, key -> 0) + 1);

					//-----
				}
				result[0] += " & ";
			}
			result[0] += current[6].replace("_", "\\newline$\\_$") + " & ";
			nodeCountMap.replace(6, nodeCountMap.computeIfAbsent(6, key -> 0) + 1);

			//-----
			List<String> ids = json.get(createQualifiedNameFromFlatten(root.getAbsolutePath()));
			for(int i=0; i<ids.size(); i++) {
				result[0] += ids.get(i);
				if(i+1 < ids.size()) {
					result[0] += " \\newline";
				}
			}
			result[0] += " \\\\ \\hline \n";
			lastLine[0] = pathToSubPath(root.getAbsolutePath(), 6);
		}else {
			for(File child : childs) {
				appendAppendix(line, result, lastLine, json, nodeCountMap, child);
			}
		}
	}

	private static String createQualifiedNameFromFlatten(String path) {
		String[] splitted = path.replace("_", "\\").split("\\\\");
//		System.out.println("QUALIFIED\t" + String.format("%s___%s", splitted[splitted.length-2], splitted[splitted.length-1]));
		return String.format("%s___%s", splitted[splitted.length-2], splitted[splitted.length-1]);
	}

	private static String createQualifiedNameFromPath(String path) {
		String[] splitted = path.replace("val_", "").replace("_", "\\").split("\\\\");
//		System.out.println("QUALIFIED\t" + String.format("%s___%s", splitted[splitted.length-2], splitted[splitted.length-1]));
		return String.format("%s___%s", splitted[splitted.length-3], splitted[splitted.length-2]);
	}

	public static void createD2XHistogram() {
		File rootNorm = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		File rootD1 = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train");

		Map<String, Integer> qualifiedNameToImageCountMap = new HashMap<>();
		for(File dir : rootD1.listFiles()) {
			for(File spec : dir.listFiles()) {
				qualifiedNameToImageCountMap.put(String.format("%s___%s", dir.getName(), spec.getName()), spec.listFiles().length);
			}
		}

		Map<Integer, List<String>> depthToPathMap = new HashMap<>();
		addPathsToMap(depthToPathMap, 0, rootNorm);

		for(int i=5; i<=10; i++) {
			System.out.println("---" + i + "---");
			Map<Integer, List<String>> classToQualifiedNamesListMap = new HashMap<>();
			List<String> paths = depthToPathMap.get(i);
			for(int c=0; c<paths.size(); c++) {
				List<String> qNames = new ArrayList<>();
				collectQualifiedNamesForPath(qNames, new File(paths.get(c)));
				classToQualifiedNamesListMap.put(c, qNames);
			}
			System.out.println("classes: " + classToQualifiedNamesListMap.keySet().size());

			Map<Integer, Integer> classToImageCountMap = new HashMap<>();
			for(Map.Entry<Integer, List<String>> entry : classToQualifiedNamesListMap.entrySet()) {
				int count = 0;
				for(String path : entry.getValue()) {
					count += qualifiedNameToImageCountMap.get(path);
				}
				classToImageCountMap.put(entry.getKey(), count);
			}

			List<Integer> counts = new ArrayList<>(classToImageCountMap.values());
			Collections.sort(counts);
			int size = counts.size();
			for(int c=0; c<size; c++) {
				System.out.print("(" + c + "," + (counts.get(size-c-1)/100000.0) + ") ");
			}

//			for(Map.Entry<Integer, Integer> entry : classToImageCountMap.entrySet()) {
//				System.out.println(entry.getKey() + "\t" + entry.getValue());
//			}

			System.out.println("");
		}
	}

	private static void addPathsToMap(Map<Integer, List<String>> result, int depth, File root) {
		for(File child : root.listFiles()) {
			result.computeIfAbsent(depth, key -> new ArrayList<>())
					.add(child.getAbsolutePath());
			addPathsToMap(result, depth+1, child);
		}
	}

	private static void collectQualifiedNamesForPath(List<String> result, File root) {
		File[] childs = root.listFiles();
		if(childs.length==0) {
			result.add(createQualifiedNameFromFlatten(root.getAbsolutePath()));
		}else {
			for(File child : childs) {
				collectQualifiedNamesForPath(result, child);
			}
		}
	}

	private static Object readObjectFormFile(String path) throws Exception {
		File metaFile = new File(path);
		FileInputStream fis = new FileInputStream(metaFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		return ois.readObject();
	}

	private static void writeObjectToFile(String path, Object obj) throws Exception {
		File classFile = new File(path);
		FileOutputStream fos = new FileOutputStream(classFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(obj);
		oos.close();
	}

	public static void createD2XClassRAWs() throws Exception {
		Stopwatch sw = new Stopwatch().start();
		createD2XClassRAWs(PlantNetType.D26);
		System.out.println("D26 done: " + sw.get());
		createD2XClassRAWs(PlantNetType.D25);
		System.out.println("D25 done: " + sw.get());
		createD2XClassRAWs(PlantNetType.D24);
		System.out.println("D24 done: " + sw.get());
		createD2XClassRAWs(PlantNetType.D23);
		System.out.println("D23 done: " + sw.get());
		createD2XClassRAWs(PlantNetType.D22);
		System.out.println("D22 done: " + sw.get());
	}

	private static void createD2XClassRAWs(PlantNetType d2x) throws Exception {
		File rootNorm = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		Map<Integer, List<String>> depthToPathMap = new HashMap<>();
		addPathsToMap(depthToPathMap, 0, rootNorm);
		Map<Integer, Map<Integer, List<String>>> combinedToClassToQualifiedNamesListMap = new HashMap<>();
		for(int i=5; i<=10; i++) {
			Map<Integer, List<String>> classToQualifiedNamesListMap = new HashMap<>();
			List<String> paths = depthToPathMap.get(i);
			for(int c=0; c<paths.size(); c++) {
				List<String> qNames = new ArrayList<>();
				collectQualifiedNamesForPath(qNames, new File(paths.get(c)));
				classToQualifiedNamesListMap.put(c, qNames);
			}
			combinedToClassToQualifiedNamesListMap.put(i-5, new HashMap<>());
			for(Map.Entry<Integer, List<String>> entry : classToQualifiedNamesListMap.entrySet()) {
				combinedToClassToQualifiedNamesListMap.get(i-5).put(entry.getKey(), entry.getValue());
			}
		}
		System.out.println("<--> " + combinedToClassToQualifiedNamesListMap.keySet().size());
		for(Map.Entry<Integer, Map<Integer, List<String>>> combEntry : combinedToClassToQualifiedNamesListMap.entrySet()) {
			System.out.println(combEntry.getKey() + " --> " + combEntry.getValue().keySet().size());
		}
		// -----

		Map<Integer, Map<Integer, String>> meta = (Map<Integer, Map<Integer, String>>) readObjectFormFile("E:\\PlantNet_Dataset\\testRotationToIndexOnRotationToFileNameMap.raw");
		Map<String, String> imageNamesToPathMap = new HashMap<>();
		addD2XImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_test"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d2x.TEST_ROTATION_SIZE][d2x.CLASS_SIZE];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()] = createD2XGroundTruthVector(entry.getValue(), d2x, imageNamesToPathMap, combinedToClassToQualifiedNamesListMap);
//				count(classValues[entry.getKey()]);
			}
			writeObjectToFile(String.format("%s\\classes_%d.raw", d2x.RAW_CLASSES_PATH_TEST, rotationEntry.getKey()), classValues);
		}
		System.out.println("test done");

		meta = (Map<Integer, Map<Integer, String>>) readObjectFormFile("E:\\PlantNet_Dataset\\trainingRotationToIndexOnRotationToFileNameMap.raw");
		imageNamesToPathMap = new HashMap<>();
		addD2XImagesRecursive(imageNamesToPathMap, new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train"));
		for(Map.Entry<Integer, Map<Integer, String>> rotationEntry : meta.entrySet()) {
			float[][] classValues = new float[d2x.TRAINING_ROTATION_SIZE][d2x.CLASS_SIZE];
			for(Map.Entry<Integer, String> entry : rotationEntry.getValue().entrySet()) {
				classValues[entry.getKey()] = createD2XGroundTruthVector(entry.getValue(), d2x, imageNamesToPathMap, combinedToClassToQualifiedNamesListMap);
			}
			writeObjectToFile(String.format("%s\\classes_%d.raw", d2x.RAW_CLASSES_PATH_TRAINING, rotationEntry.getKey()), classValues);
		}
		System.out.println("training done");
	}

	private static void count(float[] vec) {
		int sum = 0;
		for(int i=0; i<vec.length; i++) {
			sum += vec[i];
		}
		System.out.println("--> " + sum);
	}

	private static float[] createD2XGroundTruthVector(String fileName, PlantNetType d2x, Map<String, String> imageNamesToPathMap,
																	  Map<Integer, Map<Integer, List<String>>> combinedToClassToQualifiedNamesListMap) {
		float[] result = new float[d2x.CLASS_SIZE];
		int[] combClassSep = d2x.COMB_CLASS_SEP;
		int offSet = 0;
		String qualifiedName = imageNamesToPathMap.get(fileName);
		for(int i=0; i<combClassSep.length; i++) {
			int helper = i;
			switch (d2x) {
				case D22: helper = i + 4; break;
				case D23: helper = i + 3; break;
				case D24: helper = i + 2; break;
				case D25: helper = i + 1; break;
			}
			result[offSet + findQualifiedNameIndex(combinedToClassToQualifiedNamesListMap.get(helper), qualifiedName)] = 1.0f;
			offSet += combClassSep[i];
		}
		int helper = combClassSep.length;
		switch (d2x) {
			case D22: helper = combClassSep.length + 4; break;
			case D23: helper = combClassSep.length + 3; break;
			case D24: helper = combClassSep.length + 2; break;
			case D25: helper = combClassSep.length + 1; break;
		}
		result[offSet + findQualifiedNameIndex(combinedToClassToQualifiedNamesListMap.get(helper), qualifiedName)] = 1.0f;
		return result;
	}

	private static int findQualifiedNameIndex(Map<Integer, List<String>> map, String qualifiedName) {
		int index = -1;
		for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
			for(String qName : entry.getValue()) {
				if(qName.equals(qualifiedName)) {
					if(index != -1) {
						System.out.println("ERROR doubled qName: " + qName);
					}
					index = entry.getKey();
				}
			}
		}
		if(index == -1) {
			System.out.println("ERROR qName not found: " + qualifiedName);
		}
		return index;
	}

	private static void addD2XImagesRecursive(Map<String, String> target, File node) {
		for(File child : node.listFiles()) {
			if(child.isDirectory()) {
				addD2XImagesRecursive(target, child);
			}else {
				target.put(child.getName().replace(".jpg", ""), createQualifiedNameFromPath(child.getAbsolutePath()));
			}
		}
	}

	public static void createSunburst() {
		File rootNorm = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		File rootD1 = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxo\\images_train");

		Map<String, Integer> qualifiedNameToImageCountMap = new HashMap<>();
		for(File dir : rootD1.listFiles()) {
			for(File spec : dir.listFiles()) {
				qualifiedNameToImageCountMap.put(String.format("%s___%s", dir.getName(), spec.getName()), spec.listFiles().length);
			}
		}

		Map<Integer, List<String>> depthToPathMap = new HashMap<>();
		addPathsToMap(depthToPathMap, 0, rootNorm);

		Map<Integer, String> helperMap = new TreeMap<>();

		for(int i=5; i<=10; i++) {
//			System.out.println("---" + i + "---");
			Map<Integer, List<String>> classToQualifiedNamesListMap = new HashMap<>();
			List<String> paths = depthToPathMap.get(i);

			Map<Integer, String> classToClassNameMap = new HashMap<>();

			for(int c=0; c<paths.size(); c++) {
				List<String> qNames = new ArrayList<>();
				collectQualifiedNamesForPath(qNames, new File(paths.get(c)));
				classToQualifiedNamesListMap.put(c, qNames);
				classToClassNameMap.put(c, paths.get(c).split("\\\\")[9]);
			}
//			System.out.println("classes: " + classToQualifiedNamesListMap.keySet().size());

			Map<Integer, Integer> classToImageCountMap = new HashMap<>();
			for(Map.Entry<Integer, List<String>> entry : classToQualifiedNamesListMap.entrySet()) {
				int count = 0;
				for(String path : entry.getValue()) {
					count += qualifiedNameToImageCountMap.get(path);
//					count++;
				}
				classToImageCountMap.put(entry.getKey(), count);
			}
			double start = 0.0;
			double end;
			double sum = 0.0;
			for(Integer count : classToImageCountMap.values()) {
				sum += count;
			}

			int nodeCount = 0;

			for(Map.Entry<Integer, Integer> entry : classToImageCountMap.entrySet()) {
				end = start + 360.0 * (entry.getValue() / sum);
				if(i==5) {
					String name = classToClassNameMap.get(entry.getKey());
					name = name.equals("node") ? "node " + nodeCount++ : name;
					name = name.replace("_", "$\\_$");
					helperMap.put(entry.getKey(), name);
					System.out.println(String.format("\t\\annulus*{%d}{%1.3f}{%1.3f}[%s]", i-2, start, end, entry.getKey()).replace(",", "."));
				}else {
					System.out.println(String.format("\t\\annulus{%d}{%1.3f}{%1.3f}", i-2, start, end).replace(",", "."));
				}
				start = end;
			}

			System.out.println("");
		}
		for(Map.Entry<Integer, String> entry : helperMap.entrySet()) {
			System.out.println(entry.getKey() + " & " + entry.getValue() + " \\\\ \\hline");
		}
	}

	private static String crop(String input, int length) {
		if(input.length() > length) {
			return input.substring(0, length) + ".";
		}else {
			return input;
		}
	}

	public static void crawlD26Log() throws Exception {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\AlexNetD26.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		int imageCount = 0;
		String train = "count,top1AccE6,top1AccE5,top1AccE4,top1AccE3,top1AccE2,top1AccE1,macroAVGE6,macroAVGE5,macroAVGE4,macroAVGE3,macroAVGE2,macroAVGE1\n";
		String test = "count,top1AccE6,top1AccE5,top1AccE4,top1AccE3,top1AccE2,top1AccE1,macroAVGE6,macroAVGE5,macroAVGE4,macroAVGE3,macroAVGE2,macroAVGE1\n";
		String line;
		crawl: while((line=reader.readLine())!=null) {
			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TOTAL TIME")) {
				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TRAINING") && !line.contains("spend evaluating")) {
				train += imageCount + ",";
				train += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<11; i++) {
					train += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 10) {
						train += ",";
					}
				}
				train += "\n";
				for(int i=0; i<6; i++) {
					reader.readLine();
				}
				continue crawl;
			}
			if(line.contains("TEST") && !line.contains("spend evaluating")) {
				test += imageCount + ",";
				test += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<11; i++) {
					test += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 10) {
						test += ",";
					}
				}
				test += "\n";
				for(int i=0; i<6; i++) {
					reader.readLine();
				}
				continue crawl;
			}
		}

		System.out.println("TRAIN");
		System.out.println(train);
		System.out.println("\nTEST");
		System.out.println(test);
	}

	public static void crawlMALogs() throws Exception {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\AlexNetD23_MA.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		String result = "count,E3,E2,E1\n";
		int combs = 3;
//		int imageCount = 0;
		String line;
		crawl: while((line=reader.readLine())!=null) {
//			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
//				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
//				continue crawl;
//			}
//			if(line.contains("TOTAL TIME")) {
//				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
//				continue crawl;
//			}
			if(line.contains("MacroAVG-Top-1_Accuracy")) {
				for (int i = 0; i < combs; i++) {
					String[] splitted = line.replace("\t", ":").replace(" ", ":").split(":");//12 19
					if(i==0) {
						result += splitted[13] + ",";
					}
					result += splitted[24].replace(",", ".").replace("%", "");
					if(i+1 != combs) {
						result += ",";
						line = reader.readLine();
					}
				}
				result += "\n";
			}
		}
		System.out.println(result);
	}

	public static void addOne() throws Exception {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\LATEX\\data\\D25MATest.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		double add = 0.007874015748031496; // 0.002369668246445498 // test: 0.007874015748031496
		String line = reader.readLine();
		String result = "";
		crawl: while((line=reader.readLine())!=null) {
			String[] splitted = line.split(",");
			result += (add + Double.parseDouble(splitted[0]));
			for(int i=1; i<splitted.length; i++) {
				result += "," + splitted[i];
			}
			result += "\n";
		}
		System.out.println(result);
	}

	public static void createD26CondFFHelper() throws Exception {
		File root = new File("E:\\PlantNet_Dataset\\plantnet_300K_taxoTree\\taxoTree_normalized");
		Map<Integer, Map<Integer, int[]>> levelToClassToStartEndArrayMap = new HashMap<>();
		crawlD26CondFFHelperLevel(levelToClassToStartEndArrayMap, root, 11);
		System.out.println(levelToClassToStartEndArrayMap.keySet());
		for(Map.Entry<Integer, Map<Integer, int[]>> levelEntry : levelToClassToStartEndArrayMap.entrySet()) {
			System.out.println(levelEntry.getKey());
			for(Map.Entry<Integer, int[]> classEntry : levelEntry.getValue().entrySet()) {
				System.out.println(classEntry.getKey() + " ->\t" + classEntry.getValue()[0] + "\t\t" + classEntry.getValue()[1]);
			}
		}
		writeObjectToFile("E:\\PlantNet_Dataset\\CondFF_levelToClassToStartEndArrayMap.map", levelToClassToStartEndArrayMap);
	}

	private static void crawlD26CondFFHelperLevel(Map<Integer, Map<Integer, int[]>> result, File root, int level) {
		if(level >= 2) {
			File[] childs = root.listFiles();
			result.computeIfAbsent(level, key -> new HashMap<>());
			int start;
			for(File child : childs) {
				int latestClass = result.get(level).size();
				if(latestClass == 0) {
					start = 0;
				} else{
					start = result.get(level).get(latestClass - 1)[1];
				}
				int length = child.listFiles().length;
				result.get(level).put(latestClass, new int[]{start, start+length});
				crawlD26CondFFHelperLevel(result, child, level - 1);
			}
		}
	}

	public static void crawlD36Log() throws Exception {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\V7_OptLFNet.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		int imageCount = 0;
		String train = "count,top1AccE6,top1AccE5,top1AccE4,top1AccE3,top1AccE2,top1AccE1\n";
		String test = "count,top1AccE6,top1AccE5,top1AccE4,top1AccE3,top1AccE2,top1AccE1\n";
		String line;
		crawl: while((line=reader.readLine())!=null) {
			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TOTAL TIME")) {
				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TRAINING") && !line.contains("spend evaluating")) {
				train += imageCount + ",";
				train += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<5; i++) {
					train += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 4) {
						train += ",";
					}
				}
				train += "\n";
//				for(int i=0; i<6; i++) {
//					reader.readLine();
//				}
				continue crawl;
			}
			if(line.contains("TEST") && !line.contains("spend evaluating")) {
				test += imageCount + ",";
				test += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<5; i++) {
					test += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 4) {
						test += ",";
					}
				}
				test += "\n";
//				for(int i=0; i<6; i++) {
//					reader.readLine();
//				}
				continue crawl;
			}
		}

		System.out.println("TRAIN");
		System.out.println(train);
		System.out.println("\nTEST");
		System.out.println(test);
	}

	public static void crawlD36Log_3() throws Exception {
		File logFile = new File("E:\\Informatik Visual_Computing\\Semester XI\\Masterarbeit\\src\\main\\resources\\log\\BCNN_BaseC_224_Single_E1.txt");
		FileReader fileReader = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fileReader);
		int imageCount = 0;
		String train = "count,top1AccE3,top1AccE2,top1AccE1\n";
		String test = "count,top1AccE3,top1AccE2,top1AccE1\n";
		String line;
		crawl: while((line=reader.readLine())!=null) {
			if(line.contains("EPOCH END") && !line.contains("- 0 -")) {
				imageCount += 5 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TOTAL TIME")) {
				imageCount += 2 * PlantNetType.D11.TRAINING_ROTATION_SIZE;
				continue crawl;
			}
			if(line.contains("TRAINING") && !line.contains("spend evaluating")) {
				train += imageCount + ",";
				train += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<2; i++) {
					train += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 1) {
						train += ",";
					}
				}
				train += "\n";
//				for(int i=0; i<6; i++) {
//					reader.readLine();
//				}
				continue crawl;
			}
			if(line.contains("TEST") && !line.contains("spend evaluating")) {
				test += imageCount + ",";
				test += line.split(":")[4].trim().replace("%", "").replace(",", ".") + ",";
				for(int i=0; i<2; i++) {
					test += reader.readLine().split(":")[4].trim().replace("%", "").replace(",", ".");
					if(i != 1) {
						test += ",";
					}
				}
				test += "\n";
//				for(int i=0; i<6; i++) {
//					reader.readLine();
//				}
				continue crawl;
			}
		}

		System.out.println("TRAIN");
		System.out.println(train);
		System.out.println("\nTEST");
		System.out.println(test);
	}

}
