package util;

public class Stopwatch {

	private long startTime;
	private long lastTime;
	private boolean stopped = false;
	private long backhand;

	public Stopwatch() {
		// do nothing
	}

	public Stopwatch start() {
		long current = System.currentTimeMillis();
		this.startTime = current;
		this.lastTime = current;
		return this;
	}

	public long get() {
		if(stopped) {
			return backhand;
		}else {
			long now = System.currentTimeMillis();
			long dif = System.currentTimeMillis() - lastTime;
			lastTime = now;
			return dif;
		}
	}

	public double get(double divide) {
		return ((double) get()) / divide;
	}

	public void stop() {
		backhand = get();
		stopped = true;
	}
}
